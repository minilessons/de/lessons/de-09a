/**
 * Created by Komediruzecki on 5.12.2016..
 */

function questionInitialize() {
    userData.answers = ["", "", "", ""];
    userData.answered = [false, false, false, false];
    userData.correct = ["ans1-2", "ans2-4", "ans3-2", "ans4-5"];
}
function questionRenderView(view, infoParams) {
    if (view === "start") {
        var res = {};
        res.options = ['cont'];
        res.questionState = JSON.stringify(
            {
                answers: userData.answers,
                answered: userData.answered,
                correct: userData.correct
            }
        );
        res.view = {action: "page:6-1"};
        return res;
    }

    if (view === "done") {
        return {
            options: ['cont'],
            questionState: JSON.stringify({}),
            view: {action: "page:6-2"},
        };
    }

    if (view === "no-good") {
        // Vars are first and second question populated by process function
        // If value is 0, question was answered good. Otherwise it was wrong.
        //var vars = {firstQuestion: 0, secondQuestion: 0};
        var vars = {};
        vars.wrongAnswers = infoParams.listOfWrongs;
        vars.answered = infoParams.answered;
        return {
            options: ['ret'],
            questionState: JSON.stringify({}),
            view: {action: "page:6-3"},
            variables: vars
        };
    }
    return null;
}

function questionProcessKey(view, key, sentData) {
    // Info consists of a list which have numbers of questions wrong.
    // For example, info.listOfWrongs[0] have value 1 if first is wrong.
    if (view === "start") {
        var answered = sentData.answered;
        var listOfWrongs = [0, 0, 0, 0];
        userData.answers = sentData.answers;
        var isGood = userData.answers.length !== 0;
        for (var i = 0, l = userData.answered.length; i < l; i++) {
            if (answered[i] === false) {
                isGood = false;
            }
            if (userData.answers[i] != userData.correct[i]) {
                listOfWrongs[i] = i + 1;
                isGood = false;
            }
        }
        if (isGood) {
            return {
                action: "view:done",
                completed: true,
            };
        } else {
            return {
                action: "view:no-good",
                info: {
                    listOfWrongs: listOfWrongs,
                    answered: answered
                }
            };
        }
    } else if (view === "done") {
        if (!questionCompleted) {
            return {
                action: "view:start"
            };
        } else {
            return {
                action: "next"
            };
        }
    } else if (view === "no-good") {
        return {
            action: "view:start"
        };
    } else {
        return {
            action: "fail"
        };
    }
}
