/**
 * Created by Komediruzecki on 20.11.2016..
 */


/**
 * Defines LineHandler template which uses different set of colors.
 * @Note Only for learning purposes...
 */
class LineHandler extends BoxHandler {
    constructor(x, y, w, h, lineWidth, lineLength, inputLineDistance, color) {
        super(x, y, w, h, "black");
        this.lineLength = lineLength;
        this.inputLineDistance = inputLineDistance;
        this.lineWidth = lineWidth;
        this.colorIn1 = color[0];
        this.colorIn2 = color[1];
        this.colorOut = color[2];
    }

    get getColorIn1() {
        return this.colorIn1;
    }

    get getColorIn2() {
        return this.colorIn2;
    }

    get getColorOut() {
        return this.colorOut;
    }
}

scopeFunctionSR();
function scopeFunctionSR() {

    let canvas = document.getElementById("SR");
    let context = canvas.getContext("2d");

    let mouseHots4 = {
        hots: [],
        min: 1.5,
        max: 2.5
    };

    let stateSR = {
        simName: "Simbol bistabila ostvarenog NI sklopovima",
        S: 1,
        R: 1,
        Q: 1,
        nQ: 0,
        userInput: [1, 1],
        hots: []
    };

    let scaleFactor = 2;
    saveClickHots(mouseHots4, 0, 0, canvas.width, canvas.height, 0);

    /**
     * Initialises whole simulation. Draws sketch and adds event listeners.
     */
    function init() {
        drawSketch(canvas, context);
        drawSketch(canvas, context);
        eventListeners(canvas);
    }

    /**
     * Draws NANDSC in given context and their connecting lines.
     * @param canvas canvas to draw sketch
     * @param context context to draw sketch
     */
    function drawSketch(canvas, context) {
        context.clearRect(0, 0, canvas.width, canvas.height);
        writeSimulationName(canvas, context, stateSR.simName);
        setBackgroundFrame(context, canvas.width, canvas.height);
        clearStateHots(stateSR);

        // Changing colors
        let q = (stateSR.Q === 1) ? "red" : "black";
        let nQ = (stateSR.nQ === 1) ? "red" : "black";
        let X = (stateSR.userInput[0] === 1) ? "red" : "black";
        let Y = (stateSR.userInput[1] === 1) ? "red" : "black";

        let lineColorOut1 = stateSR.Q === 0 ? "black" : "red";
        let lineColorOut2 = stateSR.nQ === 0 ? "black" : "red";
        let lineColorIn1 = stateSR.S === 0 ? "black" : "red";
        let lineColorIn2 = stateSR.R === 0 ? "black" : "red";
        let colors = [lineColorIn1, lineColorIn2, lineColorOut1, lineColorOut2];
        let colorsSR = {boxColor: "#CB3A34", lineColor: colors, cBoxColor: "#746dc5"};

        // Description: boxXOffset, boxYOffset, boxWidth, boxHeight, lineWidth,
        // lineLength, inputLineDistance, clickBoxWidth, clickBoxHeight, colors...
        let simSR = new Constants(scaleFactor, 75, 75, 40, 20, 15, 2, 30, Math.PI + 2, Math.PI, 20, 20);
        let boxLineWidth = 4;
        let xOff = (canvas.width - simSR.boxWidth) / 2;
        let yOff = (canvas.height - simSR.boxHeight) / 2;
        let complementList = [0, 0, 0];
        let optsSR = setOptions(xOff, yOff, simSR.boxWidth, simSR.boxHeight,
            boxLineWidth, simSR.lineLength, simSR.inputLineDistance, simSR.clickBoxWidth, simSR.clickBoxHeight, complementList, colorsSR);

        let boxSR = new BoxHandler(optsSR.boxXOff, optsSR.boxYOff,
            optsSR.boxWidth, optsSR.boxHeight, optsSR.boxColor);

        let states = [0, 1];
        drawBox(context, boxSR);
        //drawLines(context, linesSR, simSR);

        // Draw Click Boxes and Out boxes
        let inputLinesYOff = (simSR.boxHeight - simSR.inputLineDistance) / 2;
        let boxIn1 = {
            leftVertexX: optsSR.boxXOff - simSR.inputLineLength -
            simSR.clickBoxWidth,
            leftVertexY: optsSR.boxYOff + inputLinesYOff - simSR.clickBoxHeight / 2,
            clickBoxWidth: simSR.clickBoxWidth,
            clickBoxHeight: simSR.clickBoxHeight,
            color: simSR.standardLineColor,
            fillStyle: simSR.commonFillStyle
        };
        drawClickBox(context, boxIn1, stateSR, states[0]);

        let boxIn2 = {
            leftVertexX: boxIn1.leftVertexX,
            leftVertexY: boxIn1.leftVertexY + simSR.inputLineDistance,
            clickBoxWidth: simSR.clickBoxWidth,
            clickBoxHeight: simSR.clickBoxHeight,
            color: simSR.standardLineColor,
            fillStyle: simSR.commonFillStyle
        };
        drawClickBox(context, boxIn2, stateSR, states[1]);


        let boxOut1 = {
            leftVertexX: optsSR.boxXOff + optsSR.boxWidth + simSR.inputLineLength,
            leftVertexY: boxIn1.leftVertexY,
            boxWidth: simSR.clickBoxWidth,
            boxHeight: simSR.clickBoxHeight,
            color: simSR.standardLineColor,
            fillStyle: simSR.commonFillStyle
        };
        drawTextBoxOnly(context, boxOut1, stateSR.Q.toString());

        let boxOut2 = {
            leftVertexX: boxOut1.leftVertexX,
            leftVertexY: boxOut1.leftVertexY + simSR.inputLineDistance,
            boxWidth: simSR.clickBoxWidth,
            boxHeight: simSR.clickBoxHeight,
            color: simSR.standardLineColor,
            fillStyle: simSR.commonFillStyle
        };
        drawTextBoxOnly(context, boxOut2, stateSR.nQ.toString());

        let lineIn1 = {};
        lineIn1.startX = boxIn1.leftVertexX + simSR.inputLineLength + simSR.clickBoxWidth;
        lineIn1.startY = boxIn1.leftVertexY + simSR.clickBoxHeight / 2;
        lineIn1.endX = lineIn1.startX - simSR.inputLineLength;
        lineIn1.endY = lineIn1.startY;
        lineIn1.color = X;

        let lineIn2 = {};
        lineIn2.startX = lineIn1.startX;
        lineIn2.startY = lineIn1.startY + simSR.inputLineDistance;
        lineIn2.endX = lineIn2.startX - simSR.inputLineLength;
        lineIn2.endY = lineIn2.startY;
        lineIn2.color = Y;

        let lineOut1 = {};
        lineOut1.startX = boxOut1.leftVertexX - simSR.inputLineLength;
        lineOut1.startY = boxOut1.leftVertexY + simSR.clickBoxHeight / 2;
        lineOut1.endX = lineOut1.startX + simSR.inputLineLength;
        lineOut1.endY = lineOut1.startY;
        lineOut1.color = q;

        let lineOut2 = {};
        lineOut2.startX = lineOut1.startX;
        lineOut2.startY = lineOut1.startY + simSR.inputLineDistance;
        lineOut2.endX = lineOut2.startX + simSR.inputLineLength;
        lineOut2.endY = lineOut2.startY;
        lineOut2.color = nQ;

        drawConnectLine(context, lineIn1, simSR.connectLinesWidth);
        drawConnectLine(context, lineIn2, simSR.connectLinesWidth);
        drawConnectLine(context, lineOut1, simSR.connectLinesWidth);
        drawConnectLine(context, lineOut2, simSR.connectLinesWidth);

        let textIn1 = {
            leftVertexX: lineIn1.startX,
            leftVertexY: boxIn1.leftVertexY,
            boxWidth: simSR.textBoxWidth,
            boxHeight: simSR.textBoxHeight,
            color: simSR.standardLineColor,
            fillStyle: simSR.standardLineColor,
            strokeStyle: simSR.standardLineColor,
            lineWidth: simSR.textLineWidth,
            textFont: simSR.varTextSize + "px Courier"
        };

        let textIn2 = {
            leftVertexX: textIn1.leftVertexX,
            leftVertexY: boxIn2.leftVertexY,
            boxWidth: simSR.textBoxWidth,
            boxHeight: simSR.textBoxHeight,
            fillStyle: simSR.standardLineColor,
            strokeStyle: simSR.standardLineColor,
            lineWidth: simSR.textLineWidth,
            color: simSR.standardLineColor,
            textFont: simSR.varTextSize + "px Courier"
        };

        let textOut1 = {
            leftVertexX: lineOut1.startX - simSR.clickBoxWidth,
            leftVertexY: boxOut1.leftVertexY,
            boxWidth: simSR.clickBoxWidth,
            boxHeight: simSR.clickBoxHeight,
            fillStyle: simSR.standardLineColor,
            strokeStyle: simSR.standardLineColor,
            lineWidth: simSR.textLineWidth,
            color: simSR.standardLineColor,
            textFont: simSR.varTextSize + "px Courier"
        };

        let textOut2 = {
            leftVertexX: textOut1.leftVertexX,
            leftVertexY: boxOut2.leftVertexY,
            boxWidth: simSR.clickBoxWidth,
            boxHeight: simSR.clickBoxHeight,
            fillStyle: simSR.standardLineColor,
            strokeStyle: simSR.standardLineColor,
            lineWidth: simSR.textLineWidth,
            color: simSR.standardLineColor,
            textFont: simSR.varTextSize + "px Courier"
        };
        let overLineLength = simSR.overLineLength;
        drawOverlineText2(context, textIn1, "S", overLineLength, scaleFactor);
        drawOverlineText2(context, textIn2, "R", overLineLength, scaleFactor);
        drawText(context, textOut1, "Q");
        drawOverlineText2(context, textOut2, "Q", overLineLength, scaleFactor);
    }

    /**
     * This function checks for any mousedown action and activates circuit flow accordingly.
     * This is callback function for event <code>mousedown</code> listener of clickable box.
     * @param event event for this action
     */
    function clickLineEvent(event) {
        let rect = canvas.getBoundingClientRect();
        let pos = {
            x: event.clientX - rect.left,
            y: event.clientY - rect.top
        };
        // Check if mouse is in area which we consider..
        for (let i = 0, l = stateSR.hots.length; i < l; i++) {
            let h = stateSR.hots[i];
            if (pos.x >= h.x && pos.x < h.x + h.w &&
                pos.y >= h.y && pos.y < h.y + h.h) {
                stateSR.Q = 1 - stateSR.Q;
                stateSR.nQ = 1 - stateSR.nQ;
                if (h.row === 0) {
                    // Set was pressed
                    stateSR.Q = 1;
                    stateSR.nQ = 0;
                } else if (h.row === 1) {
                    // Reset was pressed
                    stateSR.nQ = 1;
                    stateSR.Q = 0;
                }
                let u = stateSR.userInput[h.row];
                if (u === 1) {
                    stateSR.userInput[h.row] = 0;
                }
                drawSketch(canvas, context);
                break;
            }
        }
    }

    function mouseUpEvent() {
        // Check if mouse is in area which we consider..
        for (let i = 0, l = stateSR.hots.length; i < l; i++) {
            let h = stateSR.hots[i];
            stateSR.userInput[h.row] = 1;

        }
        drawSketch(canvas, context);
    }


    /**
     * Adds mouse down and mouse up listeners to simulation.
     * If mouse is pressed inverters will change state. After releasing the mouse button,
     * inverters hold their state.
     */
    function eventListeners(canvas) {
        canvas.addEventListener('mousedown', clickLineEvent);
        canvas.addEventListener('mouseup', mouseUpEvent);
        canvas.addEventListener("mousewheel", MouseWheelEvent1, false);
        // Firefox
        canvas.addEventListener("DOMMouseScroll", MouseWheelEvent1, false);
    }

    function MouseWheelEvent1(e) {
        // Firefox prevent default...
        e.preventDefault();
        mouseZoomer(e, canvas, mouseHots4, scaleFactor, setScaleFactor);
    }

    function setScaleFactor(newScaleFactor) {
        scaleFactor = newScaleFactor;
        drawSketch(canvas, context);
    }

    init();

}
