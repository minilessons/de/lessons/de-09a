scopeFunction4a();

function scopeFunction4a() {

    let canvas = document.getElementById("replacedWithNAND");
    let context = canvas.getContext("2d");

    let stateNANDS = {
        simName: "Bistabil ostvaren NI sklopovima\n(oba ulaza spojena zajedno)",
        Q: 1,
        nQ: 0,
        userInput: [0, 0],
        hots: [],
        animationOffset: 0,
        animationPattern: [20, 12]
    };

    let mouseHots1 = {
        hots: [],
        min: 1.4,
        max: 2
    };

    let scaleFactor = 2;
    let sim1Const = new Constants(scaleFactor);
    saveClickHots(mouseHots1, 0, 0, canvas.width, canvas.height, 0);

    /**
     * Draws inverters in given context and their connecting lines.
     * @param canvas canvas to draw sketch
     * @param context context to draw sketch
     */
    function drawSketch(canvas, context) {
        context.clearRect(0, 0, canvas.width, canvas.height);
        writeSimulationName(canvas, context, stateNANDS.simName);
        setBackgroundFrame(context, canvas.width, canvas.height);
        clearStateHots(stateNANDS);

        // Set some useful variables
        let connectLineLength = 60 * scaleFactor;

        /* Sketch Globals */
        let sketchLengthSim1 = (2 * (sim1Const.NANDBoxWidth) + 2 *
        sim1Const.IEEEComplementDiameter + 4 * sim1Const.inputLineLength
        + connectLineLength);

        /* Starting point for first NAND, important for fixing center on canvas */
        let canvasCenter = (canvas.width - sketchLengthSim1 -
            2 * sim1Const.outLineLength) / 2;
        let xOff = canvasCenter + sim1Const.outLineLength + sim1Const.inputLineLength;
        let yOff = ((canvas.height - sim1Const.NANDBoxHeight / 2) / 2 );

        let inputLinesYOff = (sim1Const.NANDBoxHeight - sim1Const.inputLineDistance) / 2;
        // Changing colors
        let q = (stateNANDS.Q == 1)
            ? sim1Const.redLineActiveColor : sim1Const.passiveStateColor;
        let nQ = (stateNANDS.nQ == 1)
            ? sim1Const.redLineActiveColor : sim1Const.passiveStateColor;

        let NAND_IEC = [
            {
                startX: xOff,
                startY: yOff,
                boxWidth: sim1Const.NANDBoxWidth,
                boxHeight: sim1Const.NANDBoxHeight,
                color: sim1Const.frameBlacked,
                fillStyle: "white",
                textFont: sim1Const.IECFontSize + "px Courier",
                complementList: [0, 0, 1],
                lineColorsIO: [nQ, nQ, q],
                state: stateNANDS,
                clickHotsIO: [true, true, true]
            }
        ];


        let connectNANDS = {};
        connectNANDS.startX = xOff + sim1Const.NANDBoxWidth +
            sim1Const.IEEEComplementDiameter + sim1Const.inputLineLength;
        connectNANDS.startY = yOff + sim1Const.NANDBoxHeight / 2;
        connectNANDS.endX = connectNANDS.startX + connectLineLength;
        connectNANDS.endY = connectNANDS.startY;
        connectNANDS.color = sim1Const.passiveStateColor;

        let NAND_IEC2 =
            {
                startX: connectNANDS.endX + sim1Const.inputLineLength,
                startY: yOff,
                boxWidth: sim1Const.NANDBoxWidth,
                boxHeight: sim1Const.NANDBoxHeight,
                color: sim1Const.frameBlacked,
                fillStyle: "white",
                textFont: sim1Const.IECFontSize + "px Courier",
                complementList: [0, 0, 1],
                lineColorsIO: [q, q, nQ],
                state: stateNANDS,
                clickHotsIO: [true, true, true]
            };

        NAND_IEC = NAND_IEC.concat(NAND_IEC2);
        setGradient(context, NAND_IEC);
        let connectInputs = [
            {
                startX: xOff - sim1Const.inputLineLength,
                startY: yOff + inputLinesYOff,
                endX: xOff - sim1Const.inputLineLength,
                endY: yOff + sim1Const.NANDBoxHeight - inputLinesYOff,
                color: sim1Const.passiveStateColor
            },
            {
                startX: NAND_IEC[1].startX - sim1Const.inputLineLength,
                startY: yOff + inputLinesYOff,
                endX: NAND_IEC[1].startX - sim1Const.inputLineLength,
                endY: yOff + sim1Const.NANDBoxHeight - inputLinesYOff,
                color: sim1Const.passiveStateColor
            }
        ];

        drawConnectLine(context, connectNANDS, sim1Const.connectLinesWidth);
        if (q === sim1Const.redLineActiveColor) {
            activeStyle(context, stateNANDS);
            connectNANDS.color = sim1Const.currentColor;
            drawConnectLine(context, connectNANDS, sim1Const.currentLineWidth);
        }
        passiveStyle(context);

        let clickHotsOffset = 10;
        saveConnectLineClickHots(context, stateNANDS, connectNANDS, clickHotsOffset);
        let logicGates = new LogicGates();
        drawRectangularBox(context, NAND_IEC[0], logicGates.ANDSIGN, sim1Const);
        drawRectangularBox(context, NAND_IEC[1], logicGates.ANDSIGN, sim1Const);

        drawConnectLine(context, connectInputs[0], sim1Const.connectLinesWidth);
        drawConnectLine(context, connectInputs[1], sim1Const.connectLinesWidth);
        saveConnectLineClickHots(context, stateNANDS,
            connectInputs[0], clickHotsOffset, 1);
        saveConnectLineClickHots(context, stateNANDS,
            connectInputs[1], clickHotsOffset, 1);

        // Draw returning line
        let returnLine = {
            startX: NAND_IEC[1].startX + sim1Const.NANDBoxWidth +
            sim1Const.IEEEComplementDiameter + sim1Const.inputLineLength,
            startY: NAND_IEC[1].startY + sim1Const.NANDBoxHeight / 2,
            endAtY: NAND_IEC[1].startY + sim1Const.NANDBoxHeight / 2,
            sketchLength: sketchLengthSim1,
            color: sim1Const.passiveStateColor,
            saveClickHots: true,
            state: stateNANDS
        };

        drawReturnConnector(context, returnLine, sim1Const);
        if (nQ === sim1Const.redLineActiveColor) {
            activeStyle(context, stateNANDS);
            returnLine.color = sim1Const.currentColor;
            drawReturnConnector(context, returnLine, sim1Const,
                sim1Const.currentLineWidth);
        }
        passiveStyle(context);

        let withOverLine = true;
        let shift = 20;
        genericDrawOutText(context, NAND_IEC[0], "Q =", " " + stateNANDS.Q,
            sim1Const, 0, shift);
        genericDrawOutText(context, NAND_IEC[1], "Q =", " " + stateNANDS.nQ,
            sim1Const, 0, shift, withOverLine);

        let connectDot1 =
            {
                centerX: NAND_IEC[0].startX - sim1Const.inputLineLength,
                centerY: NAND_IEC[0].startY + sim1Const.NANDBoxHeight / 2,
                radius: sim1Const.IEEEConnectDotRadius,
                startAngle: toRadians(sim1Const.zeroAngle),
                endAngle: toRadians(sim1Const.fullCircle),
                rotation: false,
                color: sim1Const.passiveStateColor,
                fillStyle: sim1Const.passiveStateColor,
                lineWidth: sim1Const.connectDotLineWidth
            };

        let connectDot2 =
            {
                centerX: NAND_IEC[1].startX - sim1Const.inputLineLength,
                centerY: NAND_IEC[1].startY + sim1Const.NANDBoxHeight / 2,
                radius: sim1Const.IEEEConnectDotRadius,
                startAngle: toRadians(sim1Const.zeroAngle),
                endAngle: toRadians(sim1Const.fullCircle),
                rotation: false,
                color: sim1Const.passiveStateColor,
                fillStyle: sim1Const.passiveStateColor,
                lineWidth: sim1Const.connectDotLineWidth
            };

        drawCircle(context, connectDot1);
        drawCircle(context, connectDot2);
    }

    /**
     * This function checks for any mousedown action and activates circuit flow accordingly.
     * This is callback function for event <code>mousedown</code> listener of clickable box.
     * @param event event for this action
     */
    function clickLineEvent(event) {
        let rect = canvas.getBoundingClientRect();
        let pos = {
            x: event.clientX - rect.left,
            y: event.clientY - rect.top
        };
        // Check if mouse is in area which we consider..
        for (let i = 0, l = stateNANDS.hots.length; i < l; i++) {
            let h = stateNANDS.hots[i];
            if (pos.x >= h.x && pos.x < h.x + h.w &&
                pos.y >= h.y && pos.y < h.y + h.h) {
                stateNANDS.Q = 1 - stateNANDS.Q;
                stateNANDS.nQ = 1 - stateNANDS.nQ;
                drawSketch(canvas, context);
                break;
            }
        }
    }

    function mouseUpEvent() {
        // Do nothing...
    }

    /**
     * Adds mouse down and mouse up listeners to simulation.
     * If mouse is pressed inverters will change state. After releasing the mouse button,
     * inverters hold their state.
     */
    function eventListeners(canvas) {
        canvas.addEventListener('mousedown', clickLineEvent);
        canvas.addEventListener('mouseup', mouseUpEvent);
        canvas.addEventListener("mousewheel", MouseWheelEvent1, false);
        // Firefox
        canvas.addEventListener("DOMMouseScroll", MouseWheelEvent1, false);
    }

    function MouseWheelEvent1(e) {
        // Firefox prevent default...
        e.preventDefault();
        mouseZoomer(e, canvas, mouseHots1, scaleFactor, setScaleFactor);
    }

    function setScaleFactor(newScaleFactor) {
        scaleFactor = newScaleFactor;
        sim1Const = new Constants(scaleFactor);
        drawSketch(canvas, context);
    }

    function startSimulation() {
        stateNANDS.animationOffset++;
        if (stateNANDS.animationOffset > 2000) {
            stateNANDS.animationOffset = 0;
        }
        drawSketch(canvas, context);
        setTimeout(startSimulation, 20);
    }

    /**
     * Initialises whole simulation. Draws sketch and adds event listeners.
     */
    function init() {
        drawSketch(canvas, context);
        drawSketch(canvas, context);
        eventListeners(canvas);
        startSimulation();
    }

    init();
}
