function scopeFunctionInv() {

    let canvas = document.getElementById("inverters");
    let context = canvas.getContext("2d");

    let zoomInvHots = {
        hots: [],
        min: 1.5,
        max: 2.5
    };

    let stateInv = {
        simName: "Bistabil ostvaren invertorima",
        signalOut: 1,
        signalBetween: 0,
        userInput: [0, 0],
        hots: [],
        animationOffset: 0,
        animationPattern: [20, 12]
    };

    let scaleFactor = 2;
    let invSim = new Constants(scaleFactor);
    saveClickHots(zoomInvHots, 0, 0, canvas.width, canvas.height, 0);


    /**
     * Draws inverters in given context and their connecting lines.
     * @param canvas canvas to draw sketch
     * @param context context to draw sketch
     */
    function drawSketch(canvas, context) {
        context.clearRect(0, 0, canvas.width, canvas.height);
        writeSimulationName(canvas, context, stateInv.simName);
        clearStateHots(stateInv);

        let returnLineColor = (stateInv.signalOut == 1) ?
            invSim.redLineActiveColor : invSim.passiveStateColor;
        let connectLineColor = (stateInv.signalBetween == 1) ?
            invSim.redLineActiveColor : invSim.passiveStateColor;

        setBackgroundFrame(context, canvas.width, canvas.height);

        /* Radius is for complement circles  */
        let scaleHeight = 1 / scaleFactor;
        let connectLineLength = 80 * scaleFactor;
        let baseLineInverterLength = 30 * scaleFactor;
        let height = (baseLineInverterLength * scaleHeight) * scaleFactor;

        /* Inverter Globals */
        let inverterOff = 10 * scaleFactor;


        /* Sketch Globals */
        let sketchLengthInv = (2 * height + 4 * invSim.IEEEComplementRadius +
        connectLineLength);

        /* Starting point for first inverter */
        let xOff = ((canvas.width - 2 * invSim.IEEEComplementDiameter -
        connectLineLength - 2 * height) / 2);
        let yOff = ((canvas.height - baseLineInverterLength) / 2 );

        let inverter1 = {
            startX: xOff,
            startY: yOff,
            baseLength: baseLineInverterLength,
            h: height,
            color: "black",
            lineWidth: invSim.connectLinesWidth
        };

        // Draw triangle for inverter 1
        drawTriangle(context, inverter1);

        // Draw Circle representing complement for inverter 1
        let circle = {
            centerX: (xOff + height + invSim.IEEEComplementRadius),
            centerY: (yOff + (baseLineInverterLength / 2)),
            radius: invSim.IEEEComplementRadius,
            startAngle: toRadians(invSim.zeroAngle),
            endAngle: toRadians(invSim.fullCircle),
            fillStyle: "white",
            color: "black",
            lineWidth: invSim.complementLineWidth
        };

        // Draw complement circle on x, y position
        drawCircle(context, circle);
        // Draw connecting line

        let lineConnect = {
            startX: (circle.centerX + circle.radius),
            startY: circle.centerY,
            endX: (circle.centerX + circle.radius + connectLineLength),
            endY: circle.centerY,
            color: invSim.passiveStateColor
        };

        drawConnectLine(context, lineConnect, invSim.connectLinesWidth);
        if (connectLineColor === invSim.redLineActiveColor) {
            activeStyle(context, stateInv);
            lineConnect.color = invSim.currentColor;
            drawConnectLine(context, lineConnect, invSim.currentLineWidth);
        }
        passiveStyle(context);

        let clickHotsOffset = 10;
        saveConnectLineClickHots(context, stateInv, lineConnect, clickHotsOffset);

        // Draw triangle for inverter 2
        let inverter2 = {
            startX: lineConnect.endX,
            startY: yOff,
            baseLength: baseLineInverterLength,
            h: height,
            color: "black",
            lineWidth: invSim.connectLinesWidth
        };
        drawTriangle(context, inverter2);

        // Draw Circle representing complement for inverter2
        circle = {
            centerX: (inverter2.startX + inverter2.h + invSim.IEEEComplementRadius),
            centerY: (yOff + (baseLineInverterLength / 2)),
            radius: invSim.IEEEComplementRadius,
            startAngle: toRadians(invSim.zeroAngle),
            endAngle: toRadians(invSim.fullCircle),
            fillStyle: "white",
            color: "black",
            lineWidth: invSim.complementLineWidth
        };
        // Draw complement on position defined in circle
        drawCircle(context, circle);

        // Draw returning line
        let returnLine = {
            startX: (circle.centerX + circle.radius),
            startY: circle.centerY,
            endAtY: circle.centerY,
            color: invSim.passiveStateColor,
            sketchLength: sketchLengthInv,
            saveClickHots: true,
            state: stateInv
        };

        drawReturnConnector(context, returnLine, invSim, invSim.connectLinesWidth);
        if (returnLineColor === invSim.redLineActiveColor) {
            activeStyle(context, stateInv);
            returnLine.color = invSim.currentColor;
            drawReturnConnector(context, returnLine, invSim,
                invSim.currentLineWidth);
        }
        passiveStyle(context);

        let clickBoxOutLine = {
            leftVertexX: (xOff - invSim.outLineLength / 2 - inverterOff),
            leftVertexY: (yOff + baseLineInverterLength),
            boxWidth: invSim.clickBoxWidth,
            boxHeight: invSim.clickBoxHeight,
            color: "black",
            fillStyle: "teal",
            state: stateInv,
            clickId: 0,
            textFont: "20px Courier"
        };

        let clickBoxBetweenLine = {
            leftVertexX: lineConnect.startX + connectLineLength / 2 - inverterOff,
            leftVertexY: clickBoxOutLine.leftVertexY,
            boxWidth: invSim.clickBoxWidth,
            boxHeight: invSim.clickBoxHeight,
            color: "black",
            fillStyle: "green",
            state: stateInv,
            clickId: 1,
            textFont: "20px Courier"
        };
        clickBoxBetweenLine.textVar = clickBoxBetweenLine.state.signalBetween;
        clickBoxOutLine.textVar = clickBoxOutLine.state.signalOut;

        drawTextBox(context, clickBoxBetweenLine, clickBoxBetweenLine.textVar, 1, invSim);
        drawTextBox(context, clickBoxOutLine, clickBoxOutLine.textVar, 1, invSim);

        let inv1Text = {
            leftVertexX: xOff,
            leftVertexY: yOff + baseLineInverterLength,
            boxWidth: invSim.clickBoxWidth * 3,
            boxHeight: invSim.clickBoxHeight * 2,
            color: "black",
            fillStyle: "white",
            textFont: "20px Courier"
        };

        let inv2Text = {
            leftVertexX: inverter2.startX,
            leftVertexY: yOff + baseLineInverterLength,
            boxWidth: invSim.clickBoxWidth * 3,
            boxHeight: invSim.clickBoxHeight * 2,
            color: "black",
            fillStyle: "white",
            textFont: "20px Courier"
        };
        drawText(context, inv1Text, "invertor 1");
        drawText(context, inv2Text, "invertor 2");
    }

    function startSimulation() {
        stateInv.animationOffset++;
        if (stateInv.animationOffset > 2000) {
            stateInv.animationOffset = 0;
        }
        drawSketch(canvas, context);
        setTimeout(startSimulation, 20);
    }

    /**
     * Initialises whole simulation. Draws sketch and adds event listeners.
     */
    function init() {
        drawSketch(canvas, context);
        eventListeners(canvas);
        startSimulation();
    }

    /**
     * Adds mouse down and mouse up listeners to simulation.
     * If mouse is pressed inverters will change state. After releasing the mouse button,
     * inverters hold their state.
     */
    function eventListeners(canvas) {
        canvas.addEventListener('mousedown', clickLineEvent);
        canvas.addEventListener('mouseup', mouseUpEvent);
        canvas.addEventListener("mousewheel", MouseWheelEvent1, false);
        // Firefox
        canvas.addEventListener("DOMMouseScroll", MouseWheelEvent1, false);
    }

    function MouseWheelEvent1(e) {
        // Firefox prevent default...
        e.preventDefault();
        mouseZoomer(e, canvas, zoomInvHots, scaleFactor, setScaleFactor);
    }

    function setScaleFactor(newScaleFactor) {
        scaleFactor = newScaleFactor;
        invSim = new Constants(scaleFactor);
        drawSketch(canvas, context);
    }


    /**
     * This function checks for any mousedown action and activates circuit flow accordingly.
     * This is callback function for event <code>mousedown</code> listener of clickable box.
     * @param event event for this action
     */
    function clickLineEvent(event) {
        let rect = canvas.getBoundingClientRect();
        let pos = {
            x: event.clientX - rect.left,
            y: event.clientY - rect.top
        };
        // Check if mouse is in area which we consider..
        for (let i = 0, l = stateInv.hots.length; i < l; i++) {
            let h = stateInv.hots[i];
            if (pos.x >= h.x && pos.x < h.x + h.w &&
                pos.y >= h.y && pos.y < h.y + h.h) {
                stateInv.signalOut = 1 - stateInv.signalOut;
                stateInv.signalBetween = 1 - stateInv.signalBetween;
                drawSketch(canvas, context);
                break;
            }
        }
    }

    function mouseUpEvent(event) {
        // Do nothing
    }

    init();

}
scopeFunctionInv();