/**
 * Created by Komediruzecki on 27.11.2016..
 */

let canvasRaisingEdge = document.getElementById("RaisingEdge");
let contextRaisingEdge = canvasRaisingEdge.getContext("2d");

let canvasFallingEdge = document.getElementById("FallingEdge");
let contextFallingEdge = canvasFallingEdge.getContext("2d");

/* Global constants for this simulation */
let scaleFactor = 1;
let constSR = new Constants(scaleFactor, 50, 50, 20, 20, 40);

let stateRaisingEdge = {
    simName: "Primjer 1",
    cpEdge: 0,
    cp: 0,
    curOut: 0,
    userInput: [0, 0],
    hots: []
};

let stateFallingEdge = {
    simName: "Primjer 2",
    cpEdge: 0,
    cp: 0,
    curOut: 0,
    userInput: [0, 0],
    hots: []
};


/**
 * Defines LineHandler template which uses different set of colors.
 * @Note Only for learning purposes...
 */
class LineHandler extends BoxHandler {
    constructor(x, y, w, h, lineWidth, lineLength, inputLineDistance, color) {
        super(x, y, w, h, "black");
        this.lineWidth = lineWidth;
        this.lineLength = lineLength;
        this.inputLineDistance = inputLineDistance;
        this.colorIn1 = color[0];
        this.colorIn2 = color[1];
        this.colorOut = color[2];
        this.colorCp = color[3]
    }

    get getColorIn1() {
        return this.colorIn1;
    }

    get getColorIn2() {
        return this.colorIn2;
    }

    get getColorOut() {
        return this.colorOut;
    }

    get getColorCp() {
        return this.colorCp;
    }
}

function animateSim() {
    setInterval(animateRaisingCP, 2000);
    setInterval(animateFallingCP, 2000);
}
/**
 * This function initialises main draw functions and theirs events.
 */
function init() {
    drawSR(canvasRaisingEdge, contextRaisingEdge, stateRaisingEdge, constSR);
    drawSR(canvasFallingEdge, contextFallingEdge, stateFallingEdge, constSR);
    eventListeners();

}

/**
 * This function connects events which are used for simulating boxes behaviour.
 */
function eventListeners() {
    canvasRaisingEdge.addEventListener('mousedown', clickBoxEventRaisingEdge);
    canvasFallingEdge.addEventListener('mousedown', clickBoxEventFallingEdge);

    canvasRaisingEdge.addEventListener('mouseup', setDefaultRaisingEdge);
    canvasFallingEdge.addEventListener('mouseup', setDefaultFallingEdge);

}

/**
 * This function resets state of first box inputs and outputs.
 * This is callback function for event <code>mouseup</code> listener of first box.
 */
function setDefaultRaisingEdge() {
    for (let i = 0, l = stateRaisingEdge.hots.length; i < l; i++) {
        stateRaisingEdge.userInput[i] = 0;
    }
    drawSR(canvasRaisingEdge, contextRaisingEdge, stateRaisingEdge, constSR);
}


/**
 * This function resets state of first box inputs and outputs.
 * This is callback function for event <code>mouseup</code> listener of first box.
 */
function setDefaultFallingEdge() {
    for (let i = 0, l = stateFallingEdge.hots.length; i < l; i++) {
        stateFallingEdge.userInput[i] = 0;
    }
    drawSR(canvasFallingEdge, contextFallingEdge, stateFallingEdge, constSR);
}


/**
 * This function checks for any mousedown action and activates circuit flow accordingly.
 * This is callback function for event <code>mousedown</code> listener of second box.
 */
function clickBoxEventRaisingEdge(event) {
    let rect = canvasRaisingEdge.getBoundingClientRect();
    let pos = {
        x: event.clientX - rect.left,
        y: event.clientY - rect.top
    };
    // Check if mouse is in area which we consider..
    for (let i = 0, l = stateRaisingEdge.hots.length; i < l; i++) {
        let h = stateRaisingEdge.hots[i];
        if (pos.x >= h.x && pos.x < h.x + h.w &&
            pos.y >= h.y && pos.y < h.y + h.h) {

            setInputState(stateRaisingEdge.userInput, h.row);
            drawSR(canvasRaisingEdge, contextRaisingEdge, stateRaisingEdge, constSR);
            break;
        }
    }
}

/**
 * This function checks for any mousedown action and activates circuit flow accordingly.
 * This is callback function for event <code>mousedown</code> listener of second box.
 */
function clickBoxEventFallingEdge(event) {
    let rect = canvasFallingEdge.getBoundingClientRect();
    let pos = {
        x: event.clientX - rect.left,
        y: event.clientY - rect.top
    };
    // Check if mouse is in area which we consider..
    for (let i = 0, l = stateFallingEdge.hots.length; i < l; i++) {
        let h = stateFallingEdge.hots[i];
        if (pos.x >= h.x && pos.x < h.x + h.w &&
            pos.y >= h.y && pos.y < h.y + h.h) {
            setInputState(stateFallingEdge.userInput, h.row);
            drawSR(canvasFallingEdge, contextFallingEdge, stateFallingEdge, constSR);
            break;
        }
    }
}

/**
 * Animates flip flop which has raising edge behaviour.
 */
function animateRaisingCP() {
    stateRaisingEdge.cp = 1 - stateRaisingEdge.cp;
    if (stateRaisingEdge.cp === 1) {
        stateRaisingEdge.cpEdge = 1;
    } else {
        stateRaisingEdge.cpEdge = 0;
    }
    if (stateRaisingEdge.cpEdge === 1 && stateRaisingEdge.userInput[0] == 1) {
        stateRaisingEdge.curOut = 1;
    }
    if (stateRaisingEdge.cpEdge === 1 && stateRaisingEdge.userInput[1] == 1) {
        stateRaisingEdge.curOut = 0;
    }
    stateRaisingEdge.cpEdge = 0;
    drawSR(canvasRaisingEdge, contextRaisingEdge, stateRaisingEdge, constSR);
}

/**
 * Animates flip flop which has raising edge behaviour.
 */
function animateFallingCP() {
    stateFallingEdge.cp = 1 - stateFallingEdge.cp;
    if (stateFallingEdge.cp === 0) {
        stateFallingEdge.cpEdge = 1;
    } else {
        stateFallingEdge.cpEdge = 0;
    }
    if (stateFallingEdge.cpEdge === 1 && stateFallingEdge.userInput[0] == 1) {
        stateFallingEdge.curOut = 1;
    }
    if (stateFallingEdge.cpEdge === 1 && stateFallingEdge.userInput[1] == 1) {
        stateFallingEdge.curOut = 0;
    }
    stateFallingEdge.cpEdge = 0;
    drawSR(canvasFallingEdge, contextFallingEdge, stateFallingEdge, constSR);
}


init();
init();
animateSim();