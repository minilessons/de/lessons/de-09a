scopeFunctionC6();

function scopeFunctionC6() {

    let canvas = document.getElementById("NANDC6");
    let context = canvas.getContext("2d");

    let mouseHots6 = {
        hots: [],
        min: 1.3,
        max: 2
    };

    let stateNANDC6 = {
        simName: "Bistabil SR ostvaren NI sklopovima\n(CP djeluje na visoku razinu)",
        S: 0,
        R: 0,
        nS: 1,
        nR: 1,
        CP: 1,
        Q: 1,
        nQ: 0,
        userInput: [0, 0, 0],
        hots: [],
        animationOffset: 0,
        animationPattern: [20, 12]
    };

    let scaleFactor = 2;
    let sim6Const = new Constants(scaleFactor);
    saveClickHots(mouseHots6, 0, 0, canvas.width, canvas.height, 0);

    /**
     * This function checks for any mousedown action and activates circuit flow accordingly.
     * This is callback function for event <code>mousedown</code> listener of clickable box.
     * @param event event for this action
     */
    function clickLineEvent(event) {
        let rect = canvas.getBoundingClientRect();
        let pos = {
            x: event.clientX - rect.left,
            y: event.clientY - rect.top
        };
        // Check if mouse is in area which we consider..
        for (let i = 0, l = stateNANDC6.hots.length; i < l; i++) {
            let h = stateNANDC6.hots[i];
            if (pos.x >= h.x && pos.x < h.x + h.w &&
                pos.y >= h.y && pos.y < h.y + h.h) {
                if (h.row === 0 && stateNANDC6.userInput[2] === 1) {
                    // Set was pressed
                    stateNANDC6.Q = 1;
                    stateNANDC6.nQ = 0;
                } else if (h.row === 1 && stateNANDC6.userInput[2] === 1) {
                    // Reset was pressed
                    stateNANDC6.nQ = 1;
                    stateNANDC6.Q = 0;
                }
                let u = stateNANDC6.userInput[h.row];
                stateNANDC6.userInput[h.row] = (u === 0) ? 1 : 0;
                stateNANDC6.nS = (stateNANDC6.userInput[0] === 1 &&
                stateNANDC6.userInput[2] === 1) ? 0 : 1;
                stateNANDC6.nR = (stateNANDC6.userInput[1] === 1 &&
                stateNANDC6.userInput[2] === 1) ? 0 : 1;

                drawSketch(canvas, context);
                break;
            }
        }
    }

    function mouseUpEvent() {
        for (let i = 0, l = stateNANDC6.hots.length; i < l; i++) {
            let h = stateNANDC6.hots[i];
            if (h.row != 2) {
                stateNANDC6.userInput[h.row] = 0;
            }
        }
        stateNANDC6.nS = (stateNANDC6.userInput[0] === 1 &&
        stateNANDC6.userInput[2] === 1) ? 0 : 1;
        stateNANDC6.nR = (stateNANDC6.userInput[1] === 1 &&
        stateNANDC6.userInput[2] === 1) ? 0 : 1;
        drawSketch(canvas, context);
    }

    function setScaleFactor(newScaleFactor) {
        scaleFactor = newScaleFactor;
        sim6Const = new Constants(scaleFactor);
        drawSketch(canvas, context);
    }

    function MouseWheelEvent(e) {
        // Firefox prevent default...
        e.preventDefault();
        mouseZoomer(e, canvas, mouseHots6, scaleFactor, setScaleFactor);
    }

    /**
     * Adds mouse down and mouse up listeners to simulation.
     * If mouse is pressed inverters will change state. After releasing the mouse button,
     * inverters hold their state.
     */
    function eventListeners(canvas) {
        canvas.addEventListener('mousedown', clickLineEvent);
        canvas.addEventListener('mouseup', mouseUpEvent);
        canvas.addEventListener("mousewheel", MouseWheelEvent, false);
        // Firefox
        canvas.addEventListener("DOMMouseScroll", MouseWheelEvent, false);
    }

    function startSimulation() {
        stateNANDC6.animationOffset++;
        if (stateNANDC6.animationOffset > 2000) {
            stateNANDC6.animationOffset = 0;
        }
        drawSketch(canvas, context);
        setTimeout(startSimulation, 20);
    }

    /**
     * Initialises whole simulation. Draws sketch and adds event listeners.
     */
    function init() {
        drawSketch(canvas, context);
        drawSketch(canvas, context);
        eventListeners(canvas);
        startSimulation();
    }

    /**
     * Draws NANDSC in given context and their connecting lines.
     * @param canvas canvas to draw sketch
     * @param context context to draw sketch
     */
    function drawSketch(canvas, context) {
        context.clearRect(0, 0, canvas.width, canvas.height);
        writeSimulationName(canvas, context, stateNANDC6.simName);
        setBackgroundFrame(context, canvas.width, canvas.height);
        clearStateHots(stateNANDC6);

        /* Starting point for first NAND, important for fixing center on canvas */
        let canvasCenter = canvas.width / 2;
        let xOff = canvasCenter + sim6Const.inputLineLength;
        let yOff = ((canvas.height - 2 * sim6Const.universalBoxHeight
        - sim6Const.verticalUpLength) / 2 );

        // Changing colors
        let q = (stateNANDC6.Q === 1)
            ? sim6Const.redLineActiveColor : sim6Const.passiveStateColor;
        let nQ = (stateNANDC6.nQ === 1)
            ? sim6Const.redLineActiveColor : sim6Const.passiveStateColor;
        let S = (stateNANDC6.userInput[0] === 1)
            ? sim6Const.redLineActiveColor : sim6Const.passiveStateColor;
        let R = (stateNANDC6.userInput[1] === 1) ? sim6Const.redLineActiveColor : sim6Const.passiveStateColor;

        let nS = (stateNANDC6.nS === 1)
            ? sim6Const.redLineActiveColor : sim6Const.passiveStateColor;
        let nR = (stateNANDC6.nR === 1)
            ? sim6Const.redLineActiveColor : sim6Const.passiveStateColor;
        let CP = (stateNANDC6.userInput[2] === 1)
            ? sim6Const.redLineActiveColor : sim6Const.passiveStateColor;

        let NANDIEC = [
            {
                startX: xOff,
                startY: yOff,
                boxWidth: sim6Const.universalBoxWidth,
                boxHeight: sim6Const.universalBoxHeight,
                color: sim6Const.frameBlacked,
                fillStyle: "white",
                textFont: sim6Const.IECFontSize + "px Courier",
                complementList: [0, 0, 1],
                lineColorsIO: [nS, nQ, q],
                state: stateNANDC6
            }
        ];

        let logicGates = new LogicGates();
        let inputLinesYOff = (sim6Const.universalBoxHeight -
            sim6Const.inputLineDistance) / 2;

        // Draw return line1
        let downYOff = 50;
        let downLine = {};

        downLine.startX = NANDIEC[0].startX + sim6Const.universalBoxWidth +
            sim6Const.IEEEComplementDiameter + sim6Const.inputLineLength;
        downLine.startY = NANDIEC[0].startY + sim6Const.universalBoxHeight / 2;
        downLine.endX = downLine.startX;
        downLine.endY = downLine.startY + downYOff;
        downLine.color = sim6Const.passiveStateColor;

        let NANDYOff = 60;
        let retLineOff = 45;
        let retLine1 = {
            startX: downLine.endX,
            startY: downLine.endY,
            endX: NANDIEC[0].startX - sim6Const.inputLineLength,
            endY: NANDIEC[0].startY + sim6Const.universalBoxHeight + retLineOff,
            color: sim6Const.passiveStateColor
        };

        drawConnectLine(context, downLine, sim6Const.connectLinesWidth);
        if (q === sim6Const.redLineActiveColor) {
            activeStyle(context, stateNANDC6);
            downLine.color = sim6Const.currentColor;
            drawConnectLine(context, downLine, sim6Const.currentLineWidth);
        }
        passiveStyle(context);

        drawConnectLine(context, retLine1, sim6Const.connectLinesWidth);
        if (q === sim6Const.redLineActiveColor) {
            activeStyle(context, stateNANDC6);
            retLine1.color = sim6Const.currentColor;
            drawConnectLine(context, retLine1, sim6Const.currentLineWidth);
        }
        passiveStyle(context);


        let NANDIEC2 = {
            startX: NANDIEC[0].startX,
            startY: NANDIEC[0].startY + sim6Const.universalBoxHeight + NANDYOff,
            boxWidth: sim6Const.universalBoxWidth,
            boxHeight: sim6Const.universalBoxHeight,
            color: sim6Const.frameBlacked,
            fillStyle: "white",
            textFont: sim6Const.IECFontSize + "px Courier",
            complementList: [0, 0, 1],
            lineColorsIO: [q, nR, nQ],
            state: stateNANDC6
        };
        NANDIEC = NANDIEC.concat(NANDIEC2);
        let retLineConnect = {
            startX: retLine1.endX,
            startY: retLine1.endY,
            endX: retLine1.endX,
            endY: NANDIEC[1].startY + inputLinesYOff,
            color: sim6Const.passiveStateColor
        };

        drawConnectLine(context, retLineConnect, sim6Const.connectLinesWidth);
        if (q === sim6Const.redLineActiveColor) {
            activeStyle(context, stateNANDC6);
            retLineConnect.color = sim6Const.currentColor;
            drawConnectLine(context, retLineConnect, sim6Const.currentLineWidth);
        }
        passiveStyle(context);

        let upLine = {};
        let upYOff = downYOff;
        upLine.startX = NANDIEC[1].startX + sim6Const.universalBoxWidth +
            sim6Const.IEEEComplementDiameter + sim6Const.inputLineLength;
        upLine.startY = NANDIEC[1].startY + sim6Const.universalBoxHeight / 2;
        upLine.endX = upLine.startX;
        upLine.endY = upLine.startY - upYOff;
        upLine.color = sim6Const.passiveStateColor;

        let retLine2 = {
            startX: upLine.endX,
            startY: upLine.endY,
            endX: NANDIEC[0].startX - sim6Const.inputLineLength,
            endY: NANDIEC[1].startY - retLineOff,
            color: sim6Const.passiveStateColor
        };

        let retLineConnect2 = {
            startX: retLine2.endX,
            startY: retLine2.endY,
            endX: retLine2.endX,
            endY: NANDIEC[0].startY + sim6Const.universalBoxHeight - inputLinesYOff,
            color: sim6Const.passiveStateColor
        };

        drawConnectLine(context, upLine, sim6Const.connectLinesWidth);
        if (nQ === sim6Const.redLineActiveColor) {
            activeStyle(context, stateNANDC6);
            upLine.color = sim6Const.currentColor;
            drawConnectLine(context, upLine, sim6Const.currentLineWidth);
        }
        passiveStyle(context);

        drawConnectLine(context, retLine2, sim6Const.connectLinesWidth);
        if (nQ === sim6Const.redLineActiveColor) {
            activeStyle(context, stateNANDC6);
            retLine2.color = sim6Const.currentColor;
            drawConnectLine(context, retLine2, sim6Const.currentLineWidth);
        }
        passiveStyle(context);

        drawConnectLine(context, retLineConnect2, sim6Const.connectLinesWidth);
        if (nQ === sim6Const.redLineActiveColor) {
            activeStyle(context, stateNANDC6);
            retLineConnect2.color = sim6Const.currentColor;
            drawConnectLine(context, retLineConnect2, sim6Const.currentLineWidth);
        }
        passiveStyle(context);

        // Draw Another NANDS
        let NANDIEC3 = {
            startX: NANDIEC[0].startX - 2 * sim6Const.inputLineLength -
            sim6Const.universalBoxWidth - sim6Const.IEEEComplementDiameter,
            startY: NANDIEC[0].startY + inputLinesYOff - sim6Const.universalBoxHeight / 2,
            boxWidth: sim6Const.universalBoxWidth,
            boxHeight: sim6Const.universalBoxHeight,
            color: sim6Const.frameBlacked,
            fillStyle: "white",
            textFont: sim6Const.IECFontSize + "px Courier",
            complementList: [0, 0, 1],
            lineColorsIO: [S, CP, nS],
            state: stateNANDC6
        };

        let NANDIEC4 = {
            startX: NANDIEC[1].startX - 2 * sim6Const.inputLineLength -
            sim6Const.universalBoxWidth - sim6Const.IEEEComplementDiameter,
            startY: NANDIEC[1].startY + sim6Const.universalBoxWidth - inputLinesYOff -
            sim6Const.universalBoxWidth / 2,
            boxWidth: sim6Const.universalBoxWidth,
            boxHeight: sim6Const.universalBoxHeight,
            color: sim6Const.frameBlacked,
            fillStyle: "white",
            textFont: sim6Const.IECFontSize + "px Courier",
            complementList: [0, 0, 1],
            lineColorsIO: [CP, R, nR],
            state: stateNANDC6
        };

        NANDIEC = NANDIEC.concat(NANDIEC3, NANDIEC4);
        setGradient(context, NANDIEC);
        drawRectangularBox(context, NANDIEC[1], logicGates.ANDSIGN, sim6Const);
        drawRectangularBox(context, NANDIEC[0], logicGates.ANDSIGN, sim6Const);
        drawRectangularBox(context, NANDIEC[2], logicGates.ANDSIGN, sim6Const);
        drawRectangularBox(context, NANDIEC[3], logicGates.ANDSIGN, sim6Const);


        let clickBoxX = {
            leftVertexX: NANDIEC[2].startX - sim6Const.inputLineLength -
            sim6Const.constBoxWidth,
            leftVertexY: NANDIEC[2].startY + inputLinesYOff -
            sim6Const.constBoxHeight / 2,
            boxWidth: sim6Const.clickBoxWidth,
            boxHeight: sim6Const.clickBoxHeight,
            state: stateNANDC6,
            clickId: 0,
            color: "black",
            fillStyle: sim6Const.commonFillStyle,
            textFont: "20px Courier",
            lineWidth: sim6Const.constBoxLineWidth
        };

        // Draws text boxes, and Q and nQ text
        drawTextBox(context, clickBoxX,
            stateNANDC6.userInput[clickBoxX.clickId], 1, sim6Const);

        let clickBoxY = {
            leftVertexX: NANDIEC[3].startX - sim6Const.inputLineLength -
            sim6Const.constBoxWidth,
            leftVertexY: NANDIEC[3].startY + inputLinesYOff -
            sim6Const.constBoxHeight / 2 + sim6Const.inputLineDistance,
            boxWidth: sim6Const.clickBoxWidth,
            boxHeight: sim6Const.clickBoxHeight,
            state: stateNANDC6,
            clickId: 1,
            color: "black",
            fillStyle: sim6Const.commonFillStyle,
            textFont: "20px Courier",
            lineWidth: sim6Const.constBoxLineWidth
        };

        drawTextBox(context, clickBoxY,
            stateNANDC6.userInput[clickBoxY.clickId], 1, sim6Const);
        let underLinePosition = 1;
        let overLine = true;
        let shift = -sim6Const.inputLineLength + 20;
        genericDrawOutText(context, NANDIEC[0], "Q =", " " + stateNANDC6.Q, sim6Const, 0, shift);
        genericDrawOutText(context, NANDIEC[1], "Q =", " " + stateNANDC6.nQ,
            sim6Const, underLinePosition, shift, overLine);

        let inputS =
            {
                startX: NANDIEC[2].startX - sim6Const.inputLineLength -
                sim6Const.clickBoxWidth,
                startY: NANDIEC[2].startY + inputLinesYOff,
                color: sim6Const.passiveStateColor
            };
        inputS.endX = inputS.startX - 2 * sim6Const.inputLineLength;
        inputS.endY = inputS.startY;

        let inputR =
            {
                startX: NANDIEC[3].startX - sim6Const.inputLineLength -
                sim6Const.clickBoxWidth,
                startY: NANDIEC[3].startY + inputLinesYOff +
                sim6Const.inputLineDistance,
                color: sim6Const.passiveStateColor
            };
        inputR.endX = inputR.startX - 2 * sim6Const.inputLineLength;
        inputR.endY = inputR.startY;

        let halfOfCPLine = ((NANDIEC[3].startY + inputLinesYOff) - (NANDIEC[2].startY + sim6Const.universalBoxHeight - inputLinesYOff)) / 2;
        // Make two lines for nice current flow
        let inputCPUp = {
            startX: NANDIEC[2].startX - sim6Const.inputLineLength,
            startY: NANDIEC[2].startY + sim6Const.universalBoxHeight - inputLinesYOff + halfOfCPLine,
            color: sim6Const.passiveStateColor
        };

        inputCPUp.endX = inputCPUp.startX;
        inputCPUp.endY = inputCPUp.startY - halfOfCPLine;
        let inputCPDown =
            {
                startX: NANDIEC[2].startX - sim6Const.inputLineLength,
                startY: inputCPUp.startY,
                color: sim6Const.passiveStateColor
            };
        inputCPDown.endX = inputCPDown.startX;
        inputCPDown.endY = inputCPDown.startY + halfOfCPLine;

        drawConnectLine(context, inputS, sim6Const.connectLinesWidth);
        if (S === sim6Const.redLineActiveColor) {
            activeStyle(context, stateNANDC6);
            inputS.color = sim6Const.currentColor;
            drawConnectLine(context, inputS, sim6Const.currentLineWidth);
        }
        passiveStyle(context);


        drawConnectLine(context, inputR, sim6Const.connectLinesWidth);
        if (R === sim6Const.redLineActiveColor) {
            activeStyle(context, stateNANDC6);
            inputR.color = sim6Const.currentColor;
            drawConnectLine(context, inputR, sim6Const.currentLineWidth);
        }
        passiveStyle(context);

        drawConnectLine(context, inputCPUp, sim6Const.connectLinesWidth);
        if (CP === sim6Const.redLineActiveColor) {
            activeStyle(context, stateNANDC6);
            inputCPUp.color = sim6Const.currentColor;
            drawConnectLine(context, inputCPUp, sim6Const.currentLineWidth);
        }
        passiveStyle(context);

        drawConnectLine(context, inputCPDown, sim6Const.connectLinesWidth);
        if (CP === sim6Const.redLineActiveColor) {
            activeStyle(context, stateNANDC6);
            inputCPDown.color = sim6Const.currentColor;
            drawConnectLine(context, inputCPDown, sim6Const.currentLineWidth);
        }
        passiveStyle(context);

        /* drawConnectLine(context, inputS, sim6Const.connectLinesWidth);
         drawConnectLine(context, inputR, sim6Const.connectLinesWidth);
         drawConnectLine(context, inputCP, sim6Const.connectLinesWidth);*/

        let clickBoxCP = {
            leftVertexX: inputCPUp.startX - sim6Const.inputLineLength -
            sim6Const.clickBoxWidth,
            leftVertexY: inputCPUp.startY - sim6Const.clickBoxHeight / 2,
            boxWidth: sim6Const.clickBoxWidth,
            boxHeight: sim6Const.clickBoxHeight,
            state: stateNANDC6,
            clickId: 2,
            color: "black",
            fillStyle: sim6Const.commonFillStyle,
            textFont: "20px Courier",
            lineWidth: sim6Const.constBoxLineWidth
        };

        drawTextBox(context, clickBoxCP,
            stateNANDC6.userInput[clickBoxCP.clickId], 1, sim6Const);

        let inputLineCP = {
            startX: clickBoxCP.leftVertexX,
            startY: clickBoxCP.leftVertexY + sim6Const.clickBoxHeight / 2,
            color: sim6Const.passiveStateColor
        };
        inputLineCP.endX = inputLineCP.startX - sim6Const.inputLineLength;
        inputLineCP.endY = inputLineCP.startY;

        let inputCPOut = {
            startX: inputCPUp.startX,
            startY: inputCPUp.startY,
            color: sim6Const.passiveStateColor
        };

        inputCPOut.endX = inputCPOut.startX - sim6Const.inputLineLength;
        inputCPOut.endY = inputCPOut.startY;

        drawConnectLine(context, inputLineCP, sim6Const.connectLinesWidth);
        if (CP === sim6Const.redLineActiveColor) {
            activeStyle(context, stateNANDC6);
            inputLineCP.color = sim6Const.currentColor;
            drawConnectLine(context, inputLineCP, sim6Const.currentLineWidth);
        }
        passiveStyle(context);


        drawConnectLine(context, inputCPOut, sim6Const.connectLinesWidth);
        if (CP === sim6Const.redLineActiveColor) {
            activeStyle(context, stateNANDC6);
            inputCPOut.color = sim6Const.currentColor;
            drawConnectLine(context, inputCPOut, sim6Const.currentLineWidth);
        }
        passiveStyle(context);

        let centerWithClickBox = 5;
        let textS = {
            leftVertexX: inputS.endX - sim6Const.textBoxWidth,
            leftVertexY: clickBoxX.leftVertexY - centerWithClickBox,
            boxWidth: sim6Const.textBoxWidth,
            boxHeight: sim6Const.textBoxHeight,
            color: "black",
            fillStyle: "white",
            textFont: sim6Const.varTextSize + "px Courier",
            lineWidth: sim6Const.connectLinesWidth
        };

        let textR = {
            leftVertexX: inputR.endX - sim6Const.textBoxWidth,
            leftVertexY: clickBoxY.leftVertexY - centerWithClickBox,
            boxWidth: sim6Const.textBoxWidth,
            boxHeight: sim6Const.textBoxHeight,
            color: "black",
            fillStyle: "white",
            textFont: sim6Const.varTextSize + "px Courier",
            lineWidth: sim6Const.connectLinesWidth
        };

        let letterDistance = sim6Const.textBoxWidth / 2;
        let textCP = {
            leftVertexX: inputLineCP.endX - sim6Const.clickBoxWidth - letterDistance,
            leftVertexY: clickBoxCP.leftVertexY - centerWithClickBox,
            boxWidth: sim6Const.textBoxWidth,
            boxHeight: sim6Const.textBoxHeight,
            color: "black",
            fillStyle: "white",
            textFont: sim6Const.varTextSize + "px Courier",
            lineWidth: sim6Const.connectLinesWidth
        };
        drawText(context, textS, "S");
        drawText(context, textR, "R");
        drawText(context, textCP, "CP");

        // Draw Connect Dots
        let connectDotCP =
            {
                centerX: inputCPUp.startX,
                centerY: inputCPUp.startY,
                radius: sim6Const.IEEEConnectDotRadius,
                startAngle: toRadians(sim6Const.zeroAngle),
                endAngle: toRadians(sim6Const.fullCircle),
                rotation: false,
                color: sim6Const.passiveStateColor,
                fillStyle: sim6Const.passiveStateColor,
                lineWidth: sim6Const.connectDotLineWidth
            };

        let connectDotQ =
            {
                centerX: downLine.startX,
                centerY: downLine.startY,
                radius: sim6Const.IEEEConnectDotRadius,
                startAngle: toRadians(sim6Const.zeroAngle),
                endAngle: toRadians(sim6Const.fullCircle),
                rotation: false,
                color: sim6Const.passiveStateColor,
                fillStyle: sim6Const.passiveStateColor,
                lineWidth: sim6Const.connectDotLineWidth
            };

        let connectDotNQ =
            {
                centerX: upLine.startX,
                centerY: upLine.startY,
                radius: sim6Const.IEEEConnectDotRadius,
                startAngle: toRadians(sim6Const.zeroAngle),
                endAngle: toRadians(sim6Const.fullCircle),
                rotation: false,
                color: sim6Const.passiveStateColor,
                fillStyle: sim6Const.passiveStateColor,
                lineWidth: sim6Const.connectDotLineWidth
            };
        drawCircle(context, connectDotCP);
        drawCircle(context, connectDotQ);
        drawCircle(context, connectDotNQ);

        let outQLine = {
            startX: connectDotQ.centerX + connectDotQ.radius,
            startY: connectDotQ.centerY,
            color: sim6Const.passiveStateColor
        };
        outQLine.endX = outQLine.startX + sim6Const.inputLineLength;
        outQLine.endY = outQLine.startY;

        let outNQLine = {
            startX: connectDotNQ.centerX + connectDotNQ.radius,
            startY: connectDotNQ.centerY,
            color: sim6Const.passiveStateColor
        };
        outNQLine.endX = outNQLine.startX + sim6Const.inputLineLength;
        outNQLine.endY = outNQLine.startY;

        drawConnectLine(context, outQLine, sim6Const.connectLinesWidth);
        if (q === sim6Const.redLineActiveColor) {
            activeStyle(context, stateNANDC6);
            outQLine.color = sim6Const.currentColor;
            drawConnectLine(context, outQLine, sim6Const.currentLineWidth);
        }
        passiveStyle(context);

        drawConnectLine(context, outNQLine, sim6Const.connectLinesWidth);
        if (nQ === sim6Const.redLineActiveColor) {
            activeStyle(context, stateNANDC6);
            outNQLine.color = sim6Const.currentColor;
            drawConnectLine(context, outNQLine, sim6Const.currentLineWidth);
        }
        passiveStyle(context);
    }

    init();
}
