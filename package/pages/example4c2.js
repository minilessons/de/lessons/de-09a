// Problem with setting mouse up outside of canvas.. 0, 0 displayed...
// Each drawing function clears own space
// Created by Komediruzecki on 24.11.2016..
scopeFunctionC2();

function scopeFunctionC2() {

    let canvas = document.getElementById("NORC2");
    let context = canvas.getContext("2d");

    let mouseHots5 = {
        hots: [],
        min: 1.5,
        max: 2
    };

    let stateNORC2 = {
        simName: "Bistabil ostvaren NILI sklopovima\n" +
        "(tablica istinitosti i dijagram promjene stanja)",
        currentRow: 5,
        nextState: 1,
        X: 0,
        Y: 0,
        Q: 1,
        nQ: 0,
        QnNext: 1,
        userInput: [0, 0],
        hots: [],
        animationOffset: 0,
        animationPattern: [20, 12]
    };
    stateNORC2.elems = [
        [0, 0, 0, 0], [0, 0, 1, 0],
        [0, 1, 0, 1], [0, 1, 1, "X"],
        [1, 0, 0, 1], [1, 0, 1, 0],
        [1, 1, 0, 1], [1, 1, 1, "X"]];


    let scaleFactor = 2;
    let sim5Const = new Constants(scaleFactor);
    saveClickHots(mouseHots5, 0, 0, canvas.width, canvas.height, 0);

    /**
     * This function checks for any mousedown action and activates circuit flow accordingly.
     * This is callback function for event <code>mousedown</code> listener of clickable box.
     * @param event event for this action
     */
    function clickLineEvent(event) {
        let rect = canvas.getBoundingClientRect();
        let pos = {
            x: event.clientX - rect.left,
            y: event.clientY - rect.top
        };
        // Check if mouse is in area which we consider..
        for (let i = 0, l = stateNORC2.hots.length; i < l; i++) {
            let h = stateNORC2.hots[i];
            if (pos.x >= h.x && pos.x < h.x + h.w &&
                pos.y >= h.y && pos.y < h.y + h.h) {

                if (stateNORC2.userInput[0] === 1 && stateNORC2.userInput[1] === 1) {
                    mouseUpEvent();
                }

                let u = stateNORC2.userInput[h.row];
                stateNORC2.userInput[h.row] = (u === 0) ? 1 : 0;
                setCurrentState(stateNORC2);
                reInit();

                if (h.row === 1) {
                    // Set was pressed
                    stateNORC2.Q = (stateNORC2.userInput[0] === 1) ? 0 : 1;
                    stateNORC2.nQ = 0;
                } else if (h.row === 0) {
                    // Reset was pressed
                    stateNORC2.nQ = (stateNORC2.userInput[1] === 1) ? 0 : 1;
                    stateNORC2.Q = 0;
                }
                // Check if mouse is in area which we consider..
                setTimeout(function () {
                    setNextState(stateNORC2);
                    reInit();
                }, 3000);
                break;
            }
        }
    }

    function setNextState(state) {
        // One more skips the HEADER row in table, which is at row 0.
        let currentState = [state.Q, state.userInput[1], state.userInput[0]];
        state.currentRow = binaryToDecimal(currentState) + 1;
        state.nextState = state.elems[state.currentRow - 1][3];
    }

    function setCurrentState(state) {
        // One more skips the HEADER row in table, which is at row 0.
        let currentState = [state.Q, state.userInput[1], state.userInput[0]];
        state.currentRow = binaryToDecimal(currentState) + 1;
        state.QnNext = state.elems[state.currentRow - 1][3];
    }

    function mouseUpEvent() {
        for (let i = 0, l = stateNORC2.hots.length; i < l; i++) {
            let h = stateNORC2.hots[i];
            stateNORC2.userInput[h.row] = 0;

        }
        setCurrentState(stateNORC2);
        reInit();
    }

    function setScaleFactor(newScaleFactor) {
        scaleFactor = newScaleFactor;
        sim5Const = new Constants(scaleFactor);
        reInit();
    }


    function MouseWheelEvent(e) {
        // Firefox prevent default...
        e.preventDefault();
        mouseZoomer(e, canvas, mouseHots5, scaleFactor, setScaleFactor);
    }

    /**
     * Adds mouse down and mouse up listeners to simulation.
     * If mouse is pressed inverters will change state. After releasing the mouse button,
     * inverters hold their state.
     */
    function eventListeners(canvas) {
        canvas.addEventListener('mousedown', clickLineEvent);
        canvas.addEventListener('mouseup', mouseUpEvent);
        canvas.addEventListener("mousewheel", MouseWheelEvent, false);
        // Firefox
        canvas.addEventListener("DOMMouseScroll", MouseWheelEvent, false);
    }

    function getXOff(canvas, constants) {
        let sketchLengthSim3 = constants.clickBoxWidth +
            constants.inputLineLength + constants.universalBoxWidth
            + constants.IEEEComplementDiameter +
            constants.inputLineLength;
        let canvasCenter = (canvas.width - sketchLengthSim3 -
            2 * constants.outLineLength) / 2;
        return (canvasCenter + constants.outLineLength +
        constants.inputLineLength);
    }

    function getYOff(canvas, constants) {
        return ((canvas.height - 2 * constants.universalBoxHeight -
        constants.verticalUpLength) / 2);
    }

    function getXClear(xOff, textElements, constants) {
        return (xOff + constants.universalBoxWidth +
        constants.IEEEComplementDiameter + 2 * constants.inputLineLength +
        ((textElements - 1.43) * constants.textBoxWidth));
    }

    function getYClear(yOff, constants) {
        return (yOff + 2 * constants.universalBoxHeight + 95);
    }

    function drawDiagram(context) {
        /* Starting point for first NOR, important for fixing center on canvas */
        let stayInZero;
        let stayInOne;
        let set, reset;

        if (stateNORC2.Q === 0 && stateNORC2.QnNext === 0) {
            stayInZero = "red";
        } else {
            stayInZero = "black";
        }
        if (stateNORC2.Q === 0 && stateNORC2.QnNext === 1) {
            set = "red";
        } else {
            set = "black";
        }
        if (stateNORC2.Q === 1 && stateNORC2.QnNext === 0) {
            reset = "red";
        } else {
            reset = "black";
        }
        if (stateNORC2.Q === 1 && stateNORC2.QnNext === 1) {
            stayInOne = "red";
        } else {
            stayInOne = "black";
        }

        let stateZero = (stateNORC2.Q === 1)
            ? sim5Const.passiveStateColor : sim5Const.activeStateColor;
        let stateOne = (stateNORC2.Q === 0)
            ? sim5Const.passiveStateColor : sim5Const.activeStateColor;


        let xOff = 140;
        let yOff = 425;
        let state0 = {
            centerX: xOff,
            centerY: yOff,
            radius: sim5Const.IEEEArcRadius,
            startAngle: toRadians(0),
            endAngle: toRadians(360),
            rotation: false,
            color: stateZero,
            fillStyle: "white",
            lineWidth: sim5Const.connectLinesWidth + 2
        };

        let circleDistance = 250;
        let state1 = {
            centerX: xOff + circleDistance,
            centerY: yOff,
            radius: sim5Const.IEEEArcRadius,
            startAngle: toRadians(0),
            endAngle: toRadians(360),
            rotation: false,
            color: stateOne,
            fillStyle: "white",
            lineWidth: sim5Const.connectLinesWidth + 2
        };
        drawCircle(context, state0);
        drawCircle(context, state1);

        // Draw Text for circle states
        let textS0 = {
            leftVertexX: state0.centerX - state0.radius / 2,
            leftVertexY: state0.centerY - state0.radius / 2,
            boxWidth: sim5Const.textBoxWidth,
            boxHeight: sim5Const.textBoxHeight,
            textFont: sim5Const.varTextSize + "px Courier",
            color: sim5Const.standardLineColor,
            lineWidth: sim5Const.textLineWidth
        };
        let textS1 = {
            leftVertexX: state1.centerX - state0.radius / 2,
            leftVertexY: state1.centerY - state0.radius / 2,
            boxWidth: sim5Const.textBoxWidth,
            boxHeight: sim5Const.textBoxHeight,
            textFont: sim5Const.varTextSize + "px Courier",
            color: sim5Const.standardLineColor,
            lineWidth: sim5Const.textLineWidth
        };


        drawText(context, textS0, "0");
        drawText(context, textS1, "1");


        // Draw connect lines
        let yLineOff = sim5Const.IEEEArcRadius - Math.PI - 2;
        let distX = Math.sqrt(Math.pow(sim5Const.IEEEArcRadius, 2) -
            Math.pow(yLineOff, 2));

        let lineSet = {
            startX: xOff + distX,
            startY: yOff - yLineOff,
            controlPointX: xOff + (circleDistance - 2 * distX) / 2,
            controlPointY: yOff - sim5Const.IEEEArcRadius * 2,
            endPointX: xOff - distX + circleDistance - 5,
            endPointY: yOff - yLineOff,
            color: set,
            lineWidth: sim5Const.connectLinesWidth

        };

        let lineReset = {
            startX: xOff + distX,
            startY: yOff + yLineOff,
            controlPointX: xOff + (circleDistance - 2 * distX) / 2,
            controlPointY: yOff + sim5Const.IEEEArcRadius * 2,
            endPointX: xOff - distX + circleDistance,
            endPointY: yOff + yLineOff,
            color: reset,
            lineWidth: sim5Const.connectLinesWidth
        };

        let yCenterOff = 45;
        let lineStayZero = {
            startX: xOff - distX,
            startY: yOff - yLineOff,
            controlPointX: xOff - 3 * sim5Const.IEEEArcRadius,
            controlPointY: yOff - yCenterOff,
            endPointX: xOff - sim5Const.IEEEArcRadius,
            endPointY: yOff,
            color: stayInZero,
            lineWidth: sim5Const.connectLinesWidth
        };


        let lineStayOne = {
            startX: xOff + circleDistance + distX,
            startY: yOff - yLineOff,
            controlPointX: xOff + circleDistance + 3 * sim5Const.IEEEArcRadius,
            controlPointY: yOff - yCenterOff,
            endPointX: xOff + circleDistance + sim5Const.IEEEArcRadius,
            endPointY: yOff,
            color: stayInOne,
            lineWidth: sim5Const.connectLinesWidth
        };

        let textYOff = 20;
        let textForLineSet = {
            leftVertexX: lineSet.controlPointX,
            leftVertexY: lineSet.controlPointY - textYOff,
            boxWidth: sim5Const.textBoxWidth,
            boxHeight: sim5Const.textBoxHeight,
            textFont: sim5Const.stateDiagramFont,
            color: "black",
            fillStyle: sim5Const.passiveStateColor,
            lineWidth: sim5Const.textLineWidth

        };

        let textYOffUp = 55;
        let textForLineReset = {
            leftVertexX: lineReset.controlPointX,
            leftVertexY: lineReset.controlPointY - textYOffUp,
            boxWidth: sim5Const.textBoxWidth,
            boxHeight: sim5Const.textBoxHeight,
            textFont: sim5Const.stateDiagramFont,
            color: "black",
            fillStyle: sim5Const.passiveStateColor,
            lineWidth: sim5Const.textLineWidth

        };

        let smallInc = 17 * sim5Const.scaleFactor;
        let twoLineOff = 10;
        let textForLineStay0 = {
            leftVertexX: lineStayZero.controlPointX + smallInc,
            leftVertexY: lineStayZero.controlPointY - smallInc - twoLineOff,
            boxWidth: sim5Const.textBoxWidth,
            boxHeight: sim5Const.textBoxHeight,
            textFont: sim5Const.stateDiagramFont,
            color: "black",
            fillStyle: sim5Const.passiveStateColor,
            lineWidth: sim5Const.textLineWidth

        };

        let textForLineStay1 = {
            leftVertexX: lineStayOne.controlPointX - 2 * smallInc,
            leftVertexY: lineStayOne.controlPointY - smallInc - twoLineOff,
            boxWidth: sim5Const.textBoxWidth,
            boxHeight: sim5Const.textBoxHeight,
            textFont: sim5Const.stateDiagramFont,
            color: "black",
            fillStyle: sim5Const.passiveStateColor,
            lineWidth: sim5Const.textLineWidth

        };

        let lineHeight = 20;
        drawCurvedConnectLine(context, lineSet);
        drawCurvedConnectLine(context, lineReset);
        drawCurvedConnectLine(context, lineStayZero);
        drawCurvedConnectLine(context, lineStayOne);
        drawArrow(context, lineSet, 15, 1);
        drawArrow(context, lineReset, 15, 0);
        drawArrow(context, lineStayZero, 15, 1);
        drawArrow(context, lineStayOne, 15, 1);

        // Draw text on lines
        writeMultilineText2(context, textForLineSet, lineHeight, "(1, 0)");
        writeMultilineText2(context, textForLineReset, lineHeight, "(0, 1)");
        writeMultilineText2(context, textForLineStay0, lineHeight, "(0, 0)\n(0, 1)");
        writeMultilineText2(context, textForLineStay1, lineHeight, "(0, 0)\n(1, 0)");
    }

    /**
     * Draws NANDSC in given context and their connecting lines.
     * @param canvas canvas to draw sketch
     * @param context context to draw sketch
     */
    function drawSketch(canvas, context) {
        clearStateHots(stateNORC2);

        /* Starting point for first NOR, important for fixing center on canvas */
        let tableOff = 130;
        let horizontalOff = 70;
        let textElements = 5;
        let xOff = getXOff(canvas, sim5Const) - tableOff;
        let yOff = getYOff(canvas, sim5Const) - horizontalOff;
        let xClear = getXClear(xOff, textElements, sim5Const);
        let yClear = getYClear(yOff, sim5Const);
        context.clearRect(1, 1, xClear, yClear);

        writeSimulationName(canvas, context, stateNORC2.simName);
        setBackgroundFrame(context, canvas.width, canvas.height);
        // Changing colors
        let q = (stateNORC2.Q === 1) ? "red" : "black";
        let nQ = (stateNORC2.nQ === 1) ? "red" : "black";
        let X = (stateNORC2.userInput[0] === 1) ? "red" : "black";
        let Y = (stateNORC2.userInput[1] === 1) ? "red" : "black";

        let NORIEC = [
            {
                startX: xOff,
                startY: yOff,
                boxWidth: sim5Const.universalBoxWidth,
                boxHeight: sim5Const.universalBoxHeight,
                color: sim5Const.frameBlacked,
                fillStyle: "white",
                textFont: (sim5Const.IECFontSize - 5) + "px Courier",
                complementList: [0, 0, 1],
                lineColorsIO: [X, nQ, q],
                state: stateNORC2
            }
        ];
        let inputLinesYOff = (sim5Const.universalBoxHeight -
            sim5Const.inputLineDistance) / 2;

        // Draw return line1
        let downYOff = 60;
        let downLine = {};

        downLine.startX = NORIEC[0].startX + sim5Const.universalBoxWidth +
            sim5Const.IEEEComplementDiameter + sim5Const.inputLineLength;
        downLine.startY = NORIEC[0].startY + sim5Const.universalBoxHeight / 2;
        downLine.endX = downLine.startX;
        downLine.endY = downLine.startY + downYOff;
        downLine.color = sim5Const.passiveStateColor;

        let NORYOff = 90;
        let retLineOff = 75;
        let retLine1 = {
            startX: downLine.endX,
            startY: downLine.endY,
            endX: NORIEC[0].startX - sim5Const.inputLineLength,
            endY: NORIEC[0].startY + sim5Const.universalBoxHeight + retLineOff,
            color: sim5Const.passiveStateColor
        };

        drawConnectLine(context, downLine, sim5Const.connectLinesWidth);
        if (q === sim5Const.redLineActiveColor) {
            activeStyle(context, stateNORC2);
            downLine.color = sim5Const.currentColor;
            drawConnectLine(context, downLine, sim5Const.currentLineWidth);
        }
        passiveStyle(context);

        drawConnectLine(context, retLine1, sim5Const.connectLinesWidth);
        if (q === sim5Const.redLineActiveColor) {
            activeStyle(context, stateNORC2);
            retLine1.color = sim5Const.currentColor;
            drawConnectLine(context, retLine1, sim5Const.currentLineWidth);
        }
        passiveStyle(context);

        let NORIEC2 = {
            startX: NORIEC[0].startX,
            startY: NORIEC[0].startY + sim5Const.universalBoxHeight + NORYOff,
            boxWidth: sim5Const.universalBoxWidth,
            boxHeight: sim5Const.universalBoxHeight,
            color: sim5Const.frameBlacked,
            fillStyle: "white",
            textFont: (sim5Const.IECFontSize - 5) + "px Courier",
            complementList: [0, 0, 1],
            lineColorsIO: [q, Y, nQ],
            state: stateNORC2
        };

        let logicGates = new LogicGates();
        NORIEC = NORIEC.concat(NORIEC2);
        setGradient2(context, NORIEC);
        drawRectangularBox(context, NORIEC[0], logicGates.ORSIGN + "1", sim5Const);
        drawRectangularBox(context, NORIEC[1], logicGates.ORSIGN + "1", sim5Const);

        let retLineConnect = {
            startX: retLine1.endX,
            startY: retLine1.endY,
            endX: retLine1.endX,
            endY: NORIEC[1].startY + inputLinesYOff,
            color: sim5Const.passiveStateColor
        };

        drawConnectLine(context, retLineConnect, sim5Const.connectLinesWidth);
        if (q === sim5Const.redLineActiveColor) {
            activeStyle(context, stateNORC2);
            retLineConnect.color = sim5Const.currentColor;
            drawConnectLine(context, retLineConnect, sim5Const.currentLineWidth);
        }
        passiveStyle(context);

        let upLine = {};
        let upYOff = downYOff;
        upLine.startX = NORIEC[1].startX + sim5Const.universalBoxWidth +
            sim5Const.IEEEComplementDiameter + sim5Const.inputLineLength;
        upLine.startY = NORIEC[1].startY + sim5Const.universalBoxHeight / 2;
        upLine.endX = upLine.startX;
        upLine.endY = upLine.startY - upYOff;
        upLine.color = sim5Const.passiveStateColor;

        let retLine2 = {
            startX: upLine.endX,
            startY: upLine.endY,
            endX: NORIEC[0].startX - sim5Const.inputLineLength,
            endY: NORIEC[1].startY - retLineOff,
            color: sim5Const.passiveStateColor
        };

        let retLineConnect2 = {
            startX: retLine2.endX,
            startY: retLine2.endY,
            endX: retLine2.endX,
            endY: NORIEC[0].startY + sim5Const.universalBoxHeight - inputLinesYOff,
            color: sim5Const.passiveStateColor
        };

        drawConnectLine(context, upLine, sim5Const.connectLinesWidth);
        if (nQ === sim5Const.redLineActiveColor) {
            activeStyle(context, stateNORC2);
            upLine.color = sim5Const.currentColor;
            drawConnectLine(context, upLine, sim5Const.currentLineWidth);
        }
        passiveStyle(context);

        drawConnectLine(context, retLine2, sim5Const.connectLinesWidth);
        if (nQ === sim5Const.redLineActiveColor) {
            activeStyle(context, stateNORC2);
            retLine2.color = sim5Const.currentColor;
            drawConnectLine(context, retLine2, sim5Const.currentLineWidth);
        }
        passiveStyle(context);

        drawConnectLine(context, retLineConnect2, sim5Const.connectLinesWidth);
        if (nQ === sim5Const.redLineActiveColor) {
            activeStyle(context, stateNORC2);
            retLineConnect2.color = sim5Const.currentColor;
            drawConnectLine(context, retLineConnect2, sim5Const.currentLineWidth);
        }
        passiveStyle(context);

        let clickBoxX = {
            leftVertexX: xOff - sim5Const.inputLineLength - sim5Const.clickBoxWidth,
            leftVertexY: yOff + inputLinesYOff - sim5Const.clickBoxHeight / 2,
            boxWidth: sim5Const.clickBoxWidth,
            boxHeight: sim5Const.clickBoxHeight,
            state: stateNORC2,
            clickId: 0,
            color: "black",
            fillStyle: sim5Const.commonFillStyle,
            textFont: "20px Courier",
            lineWidth: sim5Const.constBoxLineWidth
        };

        // Draws text boxes, and Q and nQ text
        drawTextBox(context, clickBoxX,
            stateNORC2.userInput[clickBoxX.clickId], 1, sim5Const);

        let clickBoxY = {
            leftVertexX: NORIEC[1].startX - sim5Const.inputLineLength -
            sim5Const.clickBoxWidth,
            leftVertexY: NORIEC[1].startY + inputLinesYOff -
            sim5Const.clickBoxHeight / 2 + sim5Const.inputLineDistance,
            boxWidth: sim5Const.clickBoxWidth,
            boxHeight: sim5Const.clickBoxHeight,
            state: stateNORC2,
            clickId: 1,
            color: "black",
            fillStyle: sim5Const.commonFillStyle,
            textFont: "20px Courier",
            lineWidth: sim5Const.constBoxLineWidth
        };

        drawTextBox(context, clickBoxY,
            stateNORC2.userInput[clickBoxY.clickId], 1, sim5Const);
        let underLinePosition = 1;
        let overLine = true;
        let shift = -sim5Const.inputLineLength + (10 * sim5Const.scaleFactor);
        let uni = new usedUnicode();
        let shiftY = (NORIEC[0].boxHeight / 2) - (sim5Const.textBoxHeight / 2) +
            (4 * sim5Const.scaleFactor);
        let shiftOverLineY = -(4 * sim5Const.scaleFactor) -
            (sim5Const.textBoxHeight / 2);


        genericDrawOutText(context, NORIEC[0], "Q" + uni.subN + " =", " " + stateNORC2.Q, sim5Const, 0, shift, false, shiftY);
        genericDrawOutText(context, NORIEC[1], "Q" + uni.subN + " =", " " + stateNORC2.nQ, sim5Const, underLinePosition, shift, overLine, shiftOverLineY);

        let inputLineLengths = 50;
        let inputS =
            {
                startX: NORIEC[0].startX - sim5Const.inputLineLength -
                sim5Const.clickBoxWidth,
                startY: NORIEC[0].startY + inputLinesYOff,
                color: sim5Const.passiveStateColor
            };
        inputS.endX = inputS.startX - inputLineLengths;
        inputS.endY = inputS.startY;

        let inputR =
            {
                startX: NORIEC[1].startX - sim5Const.inputLineLength -
                sim5Const.clickBoxWidth,
                startY: NORIEC[1].startY + inputLinesYOff +
                sim5Const.inputLineDistance,
                color: sim5Const.passiveStateColor
            };
        inputR.endX = inputR.startX - inputLineLengths;
        inputR.endY = inputR.startY;

        drawConnectLine(context, inputS, sim5Const.connectLinesWidth);
        if (X === sim5Const.redLineActiveColor) {
            activeStyle(context, stateNORC2);
            inputS.color = sim5Const.currentColor;
            drawConnectLine(context, inputS, sim5Const.currentLineWidth);
        }
        passiveStyle(context);


        drawConnectLine(context, inputR, sim5Const.connectLinesWidth);
        if (Y === sim5Const.redLineActiveColor) {
            activeStyle(context, stateNORC2);
            inputR.color = sim5Const.currentColor;
            drawConnectLine(context, inputR, sim5Const.currentLineWidth);
        }
        passiveStyle(context);

        let centerWithClickBox = 5;


        let textX = {
            leftVertexX: inputR.endX - sim5Const.textBoxWidth,
            leftVertexY: clickBoxY.leftVertexY - centerWithClickBox,
            boxWidth: sim5Const.textBoxWidth,
            boxHeight: sim5Const.textBoxHeight,
            color: "black",
            fillStyle: "white",
            textFont: sim5Const.varTextSize + "px Courier",
            lineWidth: sim5Const.connectLinesWidth
        };

        let textY = {
            leftVertexX: inputS.endX - sim5Const.textBoxWidth,
            leftVertexY: clickBoxX.leftVertexY - centerWithClickBox,
            boxWidth: sim5Const.textBoxWidth,
            boxHeight: sim5Const.textBoxHeight,
            color: "black",
            fillStyle: "white",
            textFont: sim5Const.varTextSize + "px Courier",
            lineWidth: sim5Const.connectLinesWidth
        };
        drawText(context, textX, "S");
        drawText(context, textY, "R");

        let connectDotQ =
            {
                centerX: downLine.startX,
                centerY: downLine.startY,
                radius: sim5Const.IEEEConnectDotRadius,
                startAngle: toRadians(sim5Const.zeroAngle),
                endAngle: toRadians(sim5Const.fullCircle),
                rotation: false,
                color: sim5Const.passiveStateColor,
                fillStyle: sim5Const.passiveStateColor,
                lineWidth: sim5Const.connectDotLineWidth
            };

        let connectDotNQ =
            {
                centerX: upLine.startX,
                centerY: upLine.startY,
                radius: sim5Const.IEEEConnectDotRadius,
                startAngle: toRadians(sim5Const.zeroAngle),
                endAngle: toRadians(sim5Const.fullCircle),
                rotation: false,
                color: sim5Const.passiveStateColor,
                fillStyle: sim5Const.passiveStateColor,
                lineWidth: sim5Const.connectDotLineWidth
            };
        drawCircle(context, connectDotQ);
        drawCircle(context, connectDotNQ);


        let outQLine = {
            startX: connectDotQ.centerX + connectDotQ.radius,
            startY: connectDotQ.centerY,
            color: sim5Const.passiveStateColor
        };
        outQLine.endX = outQLine.startX + sim5Const.inputLineLength;
        outQLine.endY = outQLine.startY;

        let outNQLine = {
            startX: connectDotNQ.centerX + connectDotNQ.radius,
            startY: connectDotNQ.centerY,
            color: sim5Const.passiveStateColor
        };
        outNQLine.endX = outNQLine.startX + sim5Const.inputLineLength;
        outNQLine.endY = outNQLine.startY;

        drawConnectLine(context, outQLine, sim5Const.connectLinesWidth);
        if (q === sim5Const.redLineActiveColor) {
            activeStyle(context, stateNORC2);
            outQLine.color = sim5Const.currentColor;
            drawConnectLine(context, outQLine, sim5Const.currentLineWidth);
        }
        passiveStyle(context);

        drawConnectLine(context, outNQLine, sim5Const.connectLinesWidth);
        if (nQ === sim5Const.redLineActiveColor) {
            activeStyle(context, stateNORC2);
            outNQLine.color = sim5Const.currentColor;
            drawConnectLine(context, outNQLine, sim5Const.currentLineWidth);
        }
        passiveStyle(context);
    }

    function drawShape(rh, toff, loff, cw) {
        context.beginPath();
        context.rect(loff, toff, stateNORC2.elems[0].length * cw, rh);
        context.fillStyle = '#cccccc';
        context.fill();

        context.beginPath();
        context.rect(loff, toff + (stateNORC2.currentRow) * rh,
            stateNORC2.elems[0].length * cw, rh);
        context.fillStyle = "#0d9679";
        context.fill();

        context.beginPath();
        context.lineWidth = 1;
        for (let y = 0; y <= stateNORC2.elems.length + 1; y++) {
            context.moveTo(loff, y * rh + toff);
            context.lineTo(loff + stateNORC2.elems[0].length * cw, y * rh + toff);
        }
        for (let x = 0; x <= stateNORC2.elems[0].length; x++) {
            context.moveTo(loff + x * cw, toff);
            context.lineTo(loff + x * cw, (stateNORC2.elems.length + 1) * rh + toff);
        }
        context.strokeStyle = sim5Const.standardLineColor;
        context.stroke();

    }

    function drawTable(context) {
        let rh = 22 * 2;
        let toff = 45;
        let loff = 500;
        let cw = 25 * 2;
        drawShape(rh, toff, loff, cw);

        let uni = new usedUnicode();
        let fontSize = 14 * 2;

        let letters = ["Q" + uni.subN, "S", "R"];
        context.beginPath();
        context.font = fontSize + "px Courier";
        context.strokeStyle = sim5Const.standardLineColor;
        for (let y = 0; y < stateNORC2.elems.length + 1; y++) {
            for (let x = 0; x < stateNORC2.elems[0].length; x++) {
                let txt = y == 0 ? (x < stateNORC2.elems[0].length - 1 ?
                    letters[x] :
                    "Q" + uni.subN + uni.subPlus + uni.sub1) :
                    '' + stateNORC2.elems[y - 1][x];
                if (y > 0 && x == stateNORC2.elems[0].length - 1) {
                    txt = stateNORC2.elems[y - 1][3];
                }
                context.strokeText(txt, loff + (x + 0.5 ) * cw,
                    (y + 0.6) * rh + toff - (rh - fontSize) / 4);
            }
        }
    }

    function reInit() {
        context.clearRect(0, 0, canvas.width, canvas.height);
        drawSketch(canvas, context);
        drawTable(context);
        drawDiagram(context);
    }

    function startSimulation() {
        stateNORC2.animationOffset++;
        if (stateNORC2.animationOffset > 2000) {
            stateNORC2.animationOffset = 0;
        }
        drawSketch(canvas, context);
        setTimeout(startSimulation, 20);
    }

    /**
     * Initialises whole simulation. Draws sketch and adds event listeners.
     */
    function init() {
        drawSketch(canvas, context);
        drawSketch(canvas, context);
        drawTable(context);
        drawDiagram(context);
        eventListeners(canvas);
        startSimulation();
    }

    init();
}
