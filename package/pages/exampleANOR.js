/**
 * Created by Komediruzecki on 2.12.2016..
 */
scopeFunction5a();

function scopeFunction5a() {

    let canvas = document.getElementById("NOR_A");
    let context = canvas.getContext("2d");

    let stateNORS = {
        simName: "Bistabil ostvaren NILI sklopovima\n(oba ulaza spojena zajedno)",
        Q: 1,
        nQ: 0,
        userInput: [0, 0],
        hots: [],
        animationOffset: 0,
        animationPattern: [20, 12]
    };

    let zoomHotsNORA = {
        hots: [],
        min: 1.4,
        max: 2
    };

    let scaleFactor = 2;
    let sim5AConst = new Constants(scaleFactor);
    saveClickHots(zoomHotsNORA, 0, 0, canvas.width, canvas.height, 0);

    /**
     * Draws inverters in given context and their connecting lines.
     * @param canvas canvas to draw sketch
     * @param context context to draw sketch
     */
    function drawSketch(canvas, context) {
        context.clearRect(0, 0, canvas.width, canvas.height);
        clearStateHots(stateNORS);
        writeSimulationName(canvas, context, stateNORS.simName);
        setBackgroundFrame(context, canvas.width, canvas.height);

        // Set some useful variables
        let connectLineLength = 60 * scaleFactor;

        /* Sketch Globals */
        let sketchLengthSim1 = (2 * (sim5AConst.NANDBoxWidth) + 2 *
        sim5AConst.IEEEComplementDiameter + 4 *
        sim5AConst.inputLineLength + connectLineLength);

        /* Starting point for first NAND, important for fixing center on canvas */
        let canvasCenter = (canvas.width - sketchLengthSim1 -
            2 * sim5AConst.outLineLength) / 2;
        let xOff = canvasCenter + sim5AConst.outLineLength +
            sim5AConst.inputLineLength;
        let yOff = ((canvas.height - sim5AConst.NANDBoxHeight / 2) / 2 );

        let inputLinesYOff = (sim5AConst.NANDBoxHeight -
            sim5AConst.inputLineDistance) / 2;
        // Changing colors
        let q = (stateNORS.Q == 1)
            ? sim5AConst.redLineActiveColor : sim5AConst.passiveStateColor;
        let nQ = (stateNORS.nQ == 1)
            ? sim5AConst.redLineActiveColor : sim5AConst.passiveStateColor;

        let NAND_IEC = [
            {
                startX: xOff,
                startY: yOff,
                boxWidth: sim5AConst.NANDBoxWidth,
                boxHeight: sim5AConst.NANDBoxHeight,
                color: sim5AConst.frameBlacked,
                fillStyle: "white",
                textFont: sim5AConst.IECFontSize + "px Courier",
                complementList: [0, 0, 1],
                lineColorsIO: [nQ, nQ, q],
                state: stateNORS,
                clickHotsIO: [true, true, true]
            }
        ];

        let connectNORS = {};
        connectNORS.startX = xOff + sim5AConst.NANDBoxWidth +
            sim5AConst.IEEEComplementDiameter + sim5AConst.inputLineLength;
        connectNORS.startY = yOff + sim5AConst.NANDBoxHeight / 2;
        connectNORS.endX = connectNORS.startX + connectLineLength;
        connectNORS.endY = connectNORS.startY;
        connectNORS.color = q;

        let NAND_IEC2 =
            {
                startX: connectNORS.endX + sim5AConst.inputLineLength,
                startY: yOff,
                boxWidth: sim5AConst.NANDBoxWidth,
                boxHeight: sim5AConst.NANDBoxHeight,
                color: sim5AConst.frameBlacked,
                fillStyle: "white",
                textFont: sim5AConst.IECFontSize + "px Courier",
                complementList: [0, 0, 1],
                lineColorsIO: [q, q, nQ],
                state: stateNORS,
                clickHotsIO: [true, true, true]
            };

        NAND_IEC = NAND_IEC.concat(NAND_IEC2);
        setGradient2(context, NAND_IEC);
        let connectInputs = [
            {
                startX: xOff - sim5AConst.inputLineLength,
                startY: yOff + inputLinesYOff,
                endX: xOff - sim5AConst.inputLineLength,
                endY: yOff + sim5AConst.NANDBoxHeight - inputLinesYOff,
                color: nQ
            },
            {
                startX: NAND_IEC[1].startX - sim5AConst.inputLineLength,
                startY: yOff + inputLinesYOff,
                endX: NAND_IEC[1].startX - sim5AConst.inputLineLength,
                endY: yOff + sim5AConst.NANDBoxHeight - inputLinesYOff,
                color: q
            }
        ];

        let clickHotsOffset = sim5AConst.clickHotsOffset;

        drawConnectLine(context, connectNORS, sim5AConst.connectLinesWidth);
        if (q === sim5AConst.redLineActiveColor) {
            activeStyle(context, stateNORS);
            connectNORS.color = sim5AConst.currentColor;
            drawConnectLine(context, connectNORS, sim5AConst.currentLineWidth);
        }
        passiveStyle(context);
        saveConnectLineClickHots(context, stateNORS, connectNORS, clickHotsOffset);

        let logicGates = new LogicGates();
        drawRectangularBox(context, NAND_IEC[0],
            logicGates.ORSIGN + "1", sim5AConst);
        drawRectangularBox(context, NAND_IEC[1],
            logicGates.ORSIGN + "1", sim5AConst);
        drawConnectLine(context, connectInputs[0], sim5AConst.connectLinesWidth);
        drawConnectLine(context, connectInputs[1], sim5AConst.connectLinesWidth);
        saveConnectLineClickHots(context, stateNORS,
            connectInputs[0], clickHotsOffset, 1);
        saveConnectLineClickHots(context, stateNORS,
            connectInputs[1], clickHotsOffset, 1);

        // Draw returning line
        let returnLine = {
            startX: NAND_IEC[1].startX + sim5AConst.NANDBoxWidth +
            sim5AConst.IEEEComplementDiameter + sim5AConst.inputLineLength,
            startY: NAND_IEC[1].startY + sim5AConst.NANDBoxHeight / 2,
            endAtY: NAND_IEC[1].startY + sim5AConst.NANDBoxHeight / 2,
            sketchLength: sketchLengthSim1,
            color: sim5AConst.passiveStateColor,
            saveClickHots: true,
            state: stateNORS
        };

        drawReturnConnector(context, returnLine, sim5AConst);
        if (nQ === sim5AConst.redLineActiveColor) {
            activeStyle(context, stateNORS);
            returnLine.color = sim5AConst.currentColor;
            drawReturnConnector(context, returnLine, sim5AConst,
                sim5AConst.currentLineWidth);
        }
        passiveStyle(context);


        let withOverLine = true;
        let shift = 20;
        genericDrawOutText(context, NAND_IEC[0], "Q =", " " + stateNORS.Q,
            sim5AConst, 0, shift);
        genericDrawOutText(context, NAND_IEC[1], "Q =", " " + stateNORS.nQ,
            sim5AConst, 0, shift, withOverLine);

        let connectDot1 =
            {
                centerX: NAND_IEC[0].startX - sim5AConst.inputLineLength,
                centerY: NAND_IEC[0].startY + sim5AConst.NANDBoxHeight / 2,
                radius: sim5AConst.IEEEConnectDotRadius,
                startAngle: toRadians(sim5AConst.zeroAngle),
                endAngle: toRadians(sim5AConst.fullCircle),
                rotation: false,
                color: sim5AConst.passiveStateColor,
                fillStyle: sim5AConst.passiveStateColor,
                lineWidth: sim5AConst.connectDotLineWidth
            };

        let connectDot2 =
            {
                centerX: NAND_IEC[1].startX - sim5AConst.inputLineLength,
                centerY: NAND_IEC[1].startY + sim5AConst.NANDBoxHeight / 2,
                radius: sim5AConst.IEEEConnectDotRadius,
                startAngle: toRadians(sim5AConst.zeroAngle),
                endAngle: toRadians(sim5AConst.fullCircle),
                rotation: false,
                color: sim5AConst.passiveStateColor,
                fillStyle: sim5AConst.passiveStateColor,
                lineWidth: sim5AConst.connectDotLineWidth
            };

        drawCircle(context, connectDot1);
        drawCircle(context, connectDot2);
    }

    /**
     * This function checks for any mousedown action and activates circuit flow accordingly.
     * This is callback function for event <code>mousedown</code> listener of clickable box.
     * @param event event for this action
     */
    function clickLineEvent(event) {
        let rect = canvas.getBoundingClientRect();
        let pos = {
            x: event.clientX - rect.left,
            y: event.clientY - rect.top
        };
        // Check if mouse is in area which we consider..
        for (let i = 0, l = stateNORS.hots.length; i < l; i++) {
            let h = stateNORS.hots[i];
            if (pos.x >= h.x && pos.x < h.x + h.w &&
                pos.y >= h.y && pos.y < h.y + h.h) {
                stateNORS.Q = 1 - stateNORS.Q;
                stateNORS.nQ = 1 - stateNORS.nQ;
                drawSketch(canvas, context);
                break;
            }
        }
    }

    function mouseUpEvent() {
        // Do nothing...
    }

    /**
     * Adds mouse down and mouse up listeners to simulation.
     * If mouse is pressed inverters will change state. After releasing the mouse button,
     * inverters hold their state.
     */
    function eventListeners(canvas) {
        canvas.addEventListener('mousedown', clickLineEvent);
        canvas.addEventListener('mouseup', mouseUpEvent);
        canvas.addEventListener("mousewheel", MouseWheelEvent1, false);
        // Firefox
        canvas.addEventListener("DOMMouseScroll", MouseWheelEvent1, false);
    }

    function MouseWheelEvent1(e) {
        // Firefox prevent default...
        e.preventDefault();
        mouseZoomer(e, canvas, zoomHotsNORA, scaleFactor, setScaleFactor);
    }

    function setScaleFactor(newScaleFactor) {
        scaleFactor = newScaleFactor;
        sim5AConst = new Constants(scaleFactor);
        drawSketch(canvas, context);
    }

    function startSimulation() {
        stateNORS.animationOffset++;
        if (stateNORS.animationOffset > 2000) {
            stateNORS.animationOffset = 0;
        }
        drawSketch(canvas, context);
        setTimeout(startSimulation, 20);
    }

    /**
     * Initialises whole simulation. Draws sketch and adds event listeners.
     */
    function init() {
        drawSketch(canvas, context);
        drawSketch(canvas, context);
        eventListeners(canvas);
        startSimulation();
    }

    init();
}
