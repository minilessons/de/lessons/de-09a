/**
 * Created by Komediruzecki on 27.11.2016..
 */

let canvasActiveHigh = document.getElementById("ActiveHigh");
let contextActiveHigh = canvasActiveHigh.getContext("2d");

let canvasActiveLow = document.getElementById("ActiveLow");
let contextActiveLow = canvasActiveLow.getContext("2d");

/* Global constants for this simulation */
let scaleFactor = 1;
let constSR = new Constants(scaleFactor, 50, 50, 20, 20, 40);

let stateActiveHigh = {
    simName: "Primjer 1",
    cp: 0,
    curOut: 0,
    userInput: [0, 0],
    hots: []
};
let stateActiveLow = {
    simName: "Primjer 2",
    cp: 0,
    curOut: 0,
    userInput: [0, 0],
    hots: []
};


/**
 * Defines LineHandler template which uses different set of colors.
 * @Note Only for learning purposes...
 */
class LineHandler extends BoxHandler {
    constructor(x, y, w, h, lineWidth, lineLength, inputLineDistance, color) {
        super(x, y, w, h, "black");
        this.lineWidth = lineWidth;
        this.lineLength = lineLength;
        this.inputLineDistance = inputLineDistance;
        this.colorIn1 = color[0];
        this.colorIn2 = color[1];
        this.colorOut = color[2];
        this.colorCp = color[3]
    }

    get getColorIn1() {
        return this.colorIn1;
    }

    get getColorIn2() {
        return this.colorIn2;
    }

    get getColorOut() {
        return this.colorOut;
    }

    get getColorCp() {
        return this.colorCp;
    }
}


function animateSim() {
    setInterval(animateHighCP, 2000);
    setInterval(animateLowCP, 2000);
}
/**
 * This function initialises main draw functions and theirs events.
 */
function init() {
    drawSR(canvasActiveHigh, contextActiveHigh, stateActiveHigh, constSR);
    drawSR(canvasActiveLow, contextActiveLow, stateActiveLow, constSR);
    eventListeners();

}

/**
 * This function connects events which are used for simulating boxes behaviour.
 */
function eventListeners() {
    canvasActiveHigh.addEventListener('mousedown', clickBoxEventActiveHigh);
    canvasActiveLow.addEventListener('mousedown', clickBoxEventActiveLow);

    canvasActiveHigh.addEventListener('mouseup', setDefaultActiveHigh);
    canvasActiveLow.addEventListener('mouseup', setDefaultActiveLow);
}

/**
 * This function resets state of first box inputs and outputs.
 * This is callback function for event <code>mouseup</code> listener of first box.
 */
function setDefaultActiveHigh() {
    for (let i = 0, l = stateActiveHigh.hots.length; i < l; i++) {
        stateActiveHigh.userInput[i] = 0;
    }
    drawSR(canvasActiveHigh, contextActiveHigh, stateActiveHigh, constSR);
}


/**
 * This function resets state of first box inputs and outputs.
 * This is callback function for event <code>mouseup</code> listener of first box.
 */
function setDefaultActiveLow() {
    for (let i = 0, l = stateActiveLow.hots.length; i < l; i++) {
        stateActiveLow.userInput[i] = 0;
    }
    drawSR(canvasActiveLow, contextActiveLow, stateActiveLow, constSR);
}

/**
 * This function checks for any mousedown action and activates circuit flow accordingly.
 * This is callback function for event <code>mousedown</code> listener of second box.
 */
function clickBoxEventActiveHigh(event) {
    let rect = canvasActiveHigh.getBoundingClientRect();
    let pos = {
        x: event.clientX - rect.left,
        y: event.clientY - rect.top
    };
    // Check if mouse is in area which we consider..
    for (let i = 0, l = stateActiveHigh.hots.length; i < l; i++) {
        let h = stateActiveHigh.hots[i];
        if (pos.x >= h.x && pos.x < h.x + h.w &&
            pos.y >= h.y && pos.y < h.y + h.h) {
            let cp = stateActiveHigh.cp;

            // If set is pressed
            if (h.row === 0 && cp === 1) {
                stateActiveHigh.curOut = 1;
            } else if (h.row === 1 && cp === 1) {
                // Reset pressed
                stateActiveHigh.curOut = 0;
            }
            setInputState(stateActiveHigh.userInput, h.row);
            drawSR(canvasActiveHigh, contextActiveHigh, stateActiveHigh, constSR);
            break;
        }
    }
}


/**
 * This function checks for any mousedown action and activates circuit flow accordingly.
 * This is callback function for event <code>mousedown</code> listener of second box.
 */
function clickBoxEventActiveLow(event) {
    let rect = canvasActiveLow.getBoundingClientRect();
    let pos = {
        x: event.clientX - rect.left,
        y: event.clientY - rect.top
    };
    // Check if mouse is in area which we consider..
    for (let i = 0, l = stateActiveLow.hots.length; i < l; i++) {
        let h = stateActiveLow.hots[i];
        if (pos.x >= h.x && pos.x < h.x + h.w &&
            pos.y >= h.y && pos.y < h.y + h.h) {
            let cp = stateActiveLow.cp;

            // If set is pressed
            if (h.row === 0 && cp === 0) {
                stateActiveLow.curOut = 1;
            } else if (h.row === 1 && cp === 0) {
                // Reset pressed
                stateActiveLow.curOut = 0;
            }
            setInputState(stateActiveLow.userInput, h.row);
            drawSR(canvasActiveLow, contextActiveLow, stateActiveLow, constSR);
            break;
        }
    }
}


function animateLowCP() {
    stateActiveLow.cp = 1 - stateActiveLow.cp;
    if (stateActiveLow.userInput[0] === 1) {
        stateActiveLow.curOut = 1;
    }
    if (stateActiveLow.userInput[1] === 1) {
        stateActiveLow.curOut = 0;
    }
    drawSR(canvasActiveLow, contextActiveLow, stateActiveLow, constSR)
}

function animateHighCP() {
    stateActiveHigh.cp = 1 - stateActiveHigh.cp;
    if (stateActiveHigh.userInput[0] === 1) {
        stateActiveHigh.curOut = 1;
    }
    if (stateActiveHigh.userInput[1] === 1) {
        stateActiveHigh.curOut = 0;
    }
    drawSR(canvasActiveHigh, contextActiveHigh, stateActiveHigh, constSR)
}

init();
init();
animateSim();

