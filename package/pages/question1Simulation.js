/**
 * Created by Komediruzecki on 5.12.2016..
 */

/**
 * Created by Komediruzecki on 27.11.2016..
 */

let canvasAsyncSR = document.getElementById("AsyncSR");
let contextAsyncSR = canvasAsyncSR.getContext("2d");

/* Global constants for this simulation */
let scaleFactor = 1;
let constSR = new Constants(scaleFactor, 50, 50, 20, 20, 40);

let stateAsyncSR = {
    simName: "Primjer 1",
    curOut: 0,
    userInput: [0, 0],
    hots: []
};

/**
 * Defines LineHandler template which uses different set of colors.
 * @Note Only for learning purposes...
 */
class LineHandler extends BoxHandler {
    constructor(x, y, w, h, lineWidth, lineLength, inputLineDistance, color) {
        super(x, y, w, h, "black");
        this.lineWidth = lineWidth;
        this.lineLength = lineLength;
        this.inputLineDistance = inputLineDistance;
        this.colorIn1 = color[0];
        this.colorIn2 = color[1];
        this.colorOut = color[2];
        this.colorCp = color[3]
    }

    get getColorIn1() {
        return this.colorIn1;
    }

    get getColorIn2() {
        return this.colorIn2;
    }

    get getColorOut() {
        return this.colorOut;
    }

    get getColorCp() {
        return this.colorCp;
    }
}

/**
 * This function initialises main draw functions and theirs events.
 */
function init() {
    drawSR(canvasAsyncSR, contextAsyncSR, stateAsyncSR, constSR);
    eventListeners();

}

/**
 * This function connects events which are used for simulating boxes behaviour.
 */
function eventListeners() {
    canvasAsyncSR.addEventListener('mousedown', clickBoxEventAsyncSR);
    canvasAsyncSR.addEventListener('mouseup', setDefaultAsyncSR);
}

/**
 * This function resets state of first box inputs and outputs.
 * This is callback function for event <code>mouseup</code> listener of first box.
 */
function setDefaultAsyncSR() {
    for (let i = 0, l = stateAsyncSR.hots.length; i < l; i++) {
        stateAsyncSR.userInput[i] = 0;
    }
    drawSR(canvasAsyncSR, contextAsyncSR, stateAsyncSR, constSR);
}

/**
 * This function checks for any mousedown action and activates circuit flow accordingly.
 * This is callback function for event <code>mousedown</code> listener of second box.
 */
function clickBoxEventAsyncSR(event) {
    let rect = canvasAsyncSR.getBoundingClientRect();
    let pos = {
        x: event.clientX - rect.left,
        y: event.clientY - rect.top
    };
    // Check if mouse is in area which we consider..
    for (let i = 0, l = stateAsyncSR.hots.length; i < l; i++) {
        let h = stateAsyncSR.hots[i];
        if (pos.x >= h.x && pos.x < h.x + h.w &&
            pos.y >= h.y && pos.y < h.y + h.h) {

            // If set is pressed
            if (h.row === 0) {
                stateAsyncSR.curOut = 1;
            } else if (h.row === 1) {
                // Reset pressed
                stateAsyncSR.curOut = 0;
            }
            setInputState(stateAsyncSR.userInput, h.row);
            drawSR(canvasAsyncSR, contextAsyncSR, stateAsyncSR, constSR);
            break;
        }
    }
}
init();
init();