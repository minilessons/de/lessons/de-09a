/**
 * Created by Komediruzecki on 19.11.2016..
 */
scopeFunctionC();

function scopeFunctionC() {

    let canvas = document.getElementById("NANDC");
    let context = canvas.getContext("2d");

    let mouseHots3 = {
        hots: [],
        min: 1.5,
        max: 2
    };

    let stateNANDSC = {
        simName: "Bistabil ostvaren NI sklopovima\n(moguće upravljanje ulazima)",
        X: 1,
        Y: 1,
        Q: 1,
        nQ: 0,
        userInput: [1, 1],
        hots: [],
        animationOffset: 0,
        animationPattern: [20, 12]
    };

    let scaleFactor = 2;
    let sim3Const = new Constants(scaleFactor);
    saveClickHots(mouseHots3, 0, 0, canvas.width, canvas.height, 0);

    /**
     * This function checks for any mousedown action and activates circuit flow accordingly.
     * This is callback function for event <code>mousedown</code> listener of clickable box.
     * @param event event for this action
     */
    function clickLineEvent(event) {
        let rect = canvas.getBoundingClientRect();
        let pos = {
            x: event.clientX - rect.left,
            y: event.clientY - rect.top
        };
        // Check if mouse is in area which we consider..
        for (let i = 0, l = stateNANDSC.hots.length; i < l; i++) {
            let h = stateNANDSC.hots[i];
            if (pos.x >= h.x && pos.x < h.x + h.w &&
                pos.y >= h.y && pos.y < h.y + h.h) {
                if (stateNANDSC.userInput[0] === 0 && stateNANDSC.userInput[1] === 0) {
                    mouseUpEvent();
                }
                if (h.row === 0) {
                    // Set was pressed
                    stateNANDSC.Q = 1;
                    stateNANDSC.nQ = (stateNANDSC.userInput[1] === 0) ? 1 : 0;
                } else if (h.row === 1) {
                    // Reset was pressed
                    stateNANDSC.nQ = 1;
                    stateNANDSC.Q = (stateNANDSC.userInput[0] === 0) ? 1 : 0;
                }
                let u = stateNANDSC.userInput[h.row];
                if (u === 1) {
                    stateNANDSC.userInput[h.row] = 0;
                }
                drawSketch(canvas, context);
                break;
            }
        }
    }

    function mouseUpEvent() {
        // Check if mouse is in area which we consider..
        for (let i = 0, l = stateNANDSC.hots.length; i < l; i++) {
            let h = stateNANDSC.hots[i];
            stateNANDSC.userInput[h.row] = 1;

        }
        drawSketch(canvas, context);
    }

    function setScaleFactor(newScaleFactor) {
        scaleFactor = newScaleFactor;
        sim3Const = new Constants(scaleFactor);
        drawSketch(canvas, context);
    }


    function MouseWheelEvent(e) {
        // Firefox prevent default...
        e.preventDefault();
        mouseZoomer(e, canvas, mouseHots3, scaleFactor, setScaleFactor);
    }

    /**
     * Adds mouse down and mouse up listeners to simulation.
     * If mouse is pressed inverters will change state. After releasing the mouse button,
     * inverters hold their state.
     */
    function eventListeners(canvas) {
        canvas.addEventListener('mousedown', clickLineEvent);
        canvas.addEventListener('mouseup', mouseUpEvent);
        canvas.addEventListener("mousewheel", MouseWheelEvent, false);
        // Firefox
        canvas.addEventListener("DOMMouseScroll", MouseWheelEvent, false);
    }

    function startSimulation() {
        stateNANDSC.animationOffset++;
        if (stateNANDSC.animationOffset > 2000) {
            stateNANDSC.animationOffset = 0;
        }
        drawSketch(canvas, context);
        setTimeout(startSimulation, 20);
    }


    /**
     * Initialises whole simulation. Draws sketch and adds event listeners.
     */
    function init() {
        drawSketch(canvas, context);
        drawSketch(canvas, context);
        eventListeners(canvas);
        startSimulation();
    }

    /**
     * Draws NANDSC in given context and their connecting lines.
     * @param canvas canvas to draw sketch
     * @param context context to draw sketch
     */
    function drawSketch(canvas, context) {
        context.clearRect(0, 0, canvas.width, canvas.height);
        writeSimulationName(canvas, context, stateNANDSC.simName);
        setBackgroundFrame(context, canvas.width, canvas.height);
        clearStateHots(stateNANDSC);

        let sketchLengthSim3 = sim3Const.clickBoxWidth +
            sim3Const.inputLineLength + sim3Const.NANDBoxWidth
            + sim3Const.IEEEComplementDiameter +
            sim3Const.inputLineLength;

        /* Starting point for first NAND, important for fixing center on canvas */
        let canvasCenter = (canvas.width - sketchLengthSim3 -
            2 * sim3Const.outLineLength) / 2;
        let xOff = canvasCenter + sim3Const.outLineLength
            + sim3Const.inputLineLength;
        let yOff = ((canvas.height - 2 * sim3Const.NANDBoxHeight
        - sim3Const.verticalUpLength) / 2 );


        // Changing colors
        let q = (stateNANDSC.Q === 1)
            ? sim3Const.redLineActiveColor : sim3Const.passiveStateColor;
        let nQ = (stateNANDSC.nQ === 1)
            ? sim3Const.redLineActiveColor : sim3Const.passiveStateColor;
        let X = (stateNANDSC.userInput[0] === 1)
            ? sim3Const.redLineActiveColor : sim3Const.passiveStateColor;
        let Y = (stateNANDSC.userInput[1] === 1)
            ? sim3Const.redLineActiveColor : sim3Const.passiveStateColor;

        let NAND_IEC = [
            {
                startX: xOff,
                startY: yOff,
                boxWidth: sim3Const.universalBoxWidth,
                boxHeight: sim3Const.universalBoxHeight,
                color: sim3Const.frameBlacked,
                fillStyle: "white",
                textFont: sim3Const.IECFontSize + "px Courier",
                complementList: [0, 0, 1],
                lineColorsIO: [X, nQ, q],
                state: stateNANDSC
            }
        ];


        let inputLinesYOff = (sim3Const.universalBoxWidth -
            sim3Const.inputLineDistance) / 2;

        // Draw return line1
        let downYOff = 50;
        let downLine = {};

        downLine.startX = NAND_IEC[0].startX + sim3Const.universalBoxWidth +
            sim3Const.IEEEComplementDiameter + sim3Const.inputLineLength;
        downLine.startY = NAND_IEC[0].startY + sim3Const.universalBoxHeight / 2;
        downLine.endX = downLine.startX;
        downLine.endY = downLine.startY + downYOff;
        downLine.color = sim3Const.passiveStateColor;

        let NANDYOff = 60;
        let retLineOff = 45;
        let retLine1 = {
            startX: downLine.endX,
            startY: downLine.endY,
            endX: NAND_IEC[0].startX - sim3Const.inputLineLength,
            endY: NAND_IEC[0].startY + sim3Const.universalBoxHeight + retLineOff,
            color: sim3Const.passiveStateColor
        };
        drawConnectLine(context, downLine, sim3Const.connectLinesWidth);
        if (q === sim3Const.redLineActiveColor) {
            activeStyle(context, stateNANDSC);
            downLine.color = sim3Const.currentColor;
            drawConnectLine(context, downLine, sim3Const.currentLineWidth);
        }
        passiveStyle(context);

        drawConnectLine(context, retLine1, sim3Const.connectLinesWidth);
        if (q === sim3Const.redLineActiveColor) {
            activeStyle(context, stateNANDSC);
            retLine1.color = sim3Const.currentColor;
            drawConnectLine(context, retLine1, sim3Const.currentLineWidth);
        }
        passiveStyle(context);

        let NAND_IEC2 = {
            startX: NAND_IEC[0].startX,
            startY: NAND_IEC[0].startY + sim3Const.universalBoxHeight + NANDYOff,
            boxWidth: sim3Const.universalBoxWidth,
            boxHeight: sim3Const.universalBoxHeight,
            color: sim3Const.frameBlacked,
            fillStyle: "white",
            textFont: sim3Const.IECFontSize + "px Courier",
            complementList: [0, 0, 1],
            lineColorsIO: [q, Y, nQ],
            state: stateNANDSC
        };
        let logicGates = new LogicGates();
        NAND_IEC = NAND_IEC.concat(NAND_IEC2);
        setGradient(context, NAND_IEC);
        drawRectangularBox(context, NAND_IEC[0], logicGates.ANDSIGN, sim3Const);
        drawRectangularBox(context, NAND_IEC[1], logicGates.ANDSIGN, sim3Const);

        let retLineConnect = {
            startX: retLine1.endX,
            startY: retLine1.endY,
            endX: retLine1.endX,
            endY: NAND_IEC[1].startY + inputLinesYOff,
            color: sim3Const.passiveStateColor
        };

        drawConnectLine(context, retLineConnect, sim3Const.connectLinesWidth);
        if (q === sim3Const.redLineActiveColor) {
            activeStyle(context, stateNANDSC);
            retLineConnect.color = sim3Const.currentColor;
            drawConnectLine(context, retLineConnect, sim3Const.currentLineWidth);
        }
        passiveStyle(context);

        let upLine = {};
        let upYOff = downYOff;
        upLine.startX = NAND_IEC[1].startX + sim3Const.universalBoxWidth +
            sim3Const.IEEEComplementDiameter + sim3Const.inputLineLength;
        upLine.startY = NAND_IEC[1].startY + sim3Const.universalBoxHeight / 2;
        upLine.endX = upLine.startX;
        upLine.endY = upLine.startY - upYOff;
        upLine.color = sim3Const.passiveStateColor;

        let retLine2 = {
            startX: upLine.endX,
            startY: upLine.endY,
            endX: NAND_IEC[0].startX - sim3Const.inputLineLength,
            endY: NAND_IEC[1].startY - retLineOff,
            color: sim3Const.passiveStateColor
        };

        let retLineConnect2 = {
            startX: retLine2.endX,
            startY: retLine2.endY,
            endX: retLine2.endX,
            endY: NAND_IEC[0].startY + sim3Const.universalBoxHeight - inputLinesYOff,
            color: sim3Const.passiveStateColor
        };

        drawConnectLine(context, upLine, sim3Const.connectLinesWidth);
        if (nQ === sim3Const.redLineActiveColor) {
            activeStyle(context, stateNANDSC);
            upLine.color = sim3Const.currentColor;
            drawConnectLine(context, upLine, sim3Const.currentLineWidth);
        }
        passiveStyle(context);

        drawConnectLine(context, retLine2, sim3Const.connectLinesWidth);
        if (nQ === sim3Const.redLineActiveColor) {
            activeStyle(context, stateNANDSC);
            retLine2.color = sim3Const.currentColor;
            drawConnectLine(context, retLine2, sim3Const.currentLineWidth);
        }
        passiveStyle(context);

        drawConnectLine(context, retLineConnect2, sim3Const.connectLinesWidth);
        if (nQ === sim3Const.redLineActiveColor) {
            activeStyle(context, stateNANDSC);
            retLineConnect2.color = sim3Const.currentColor;
            drawConnectLine(context, retLineConnect2, sim3Const.currentLineWidth);
        }
        passiveStyle(context);

        let clickBoxX = {
            leftVertexX: xOff - sim3Const.inputLineLength - sim3Const.constBoxWidth,
            leftVertexY: yOff + inputLinesYOff - sim3Const.constBoxHeight / 2,
            boxWidth: sim3Const.constBoxWidth,
            boxHeight: sim3Const.constBoxHeight,
            state: stateNANDSC,
            clickId: 0,
            color: "black",
            fillStyle: sim3Const.commonFillStyle,
            textFont: "20px Courier",
            lineWidth: sim3Const.constBoxLineWidth
        };

        // Draws text boxes, and Q and nQ text
        drawTextBox(context, clickBoxX,
            stateNANDSC.userInput[clickBoxX.clickId], 1, sim3Const);

        let clickBoxY = {
            leftVertexX: NAND_IEC[1].startX - sim3Const.inputLineLength -
            sim3Const.constBoxWidth,
            leftVertexY: NAND_IEC[1].startY + inputLinesYOff -
            sim3Const.constBoxHeight / 2 + sim3Const.inputLineDistance,
            boxWidth: sim3Const.constBoxWidth,
            boxHeight: sim3Const.constBoxHeight,
            state: stateNANDSC,
            clickId: 1,
            color: "black",
            fillStyle: sim3Const.commonFillStyle,
            textFont: "20px Courier",
            lineWidth: sim3Const.constBoxLineWidth
        };

        drawTextBox(context, clickBoxY,
            stateNANDSC.userInput[clickBoxY.clickId], 1, sim3Const);
        let underLinePosition = 1;
        let overLine = true;
        let shift = -sim3Const.inputLineLength + (8 * sim3Const.scaleFactor);
        let shiftY = (NAND_IEC[0].boxHeight / 2) - (sim3Const.textBoxHeight / 2) + (4 * sim3Const.scaleFactor);
        let shiftOverLineY = -(4 * sim3Const.scaleFactor) -
            (sim3Const.textBoxHeight / 2);

        let uni = new usedUnicode();
        genericDrawOutText(context, NAND_IEC[0], "Q" + uni.subN + " =", " " + stateNANDSC.Q, sim3Const, 0, shift, false, shiftY);
        genericDrawOutText(context, NAND_IEC[1], "Q" + uni.subN + " =", " " + stateNANDSC.nQ, sim3Const, underLinePosition, shift, overLine, shiftOverLineY);
        /* genericDrawOutText(context, NAND_IEC[0],
         "Q =", " " + stateNANDSC.Q, sim3Const, 0, shift, false, shiftY);
         genericDrawOutText(context, NAND_IEC[1], "Q =", " " + stateNANDSC.nQ,
         sim3Const, underLinePosition, shift, overLine, shiftOverLineY);*/
        let inputLineLengths = 50;
        let inputX =
            {
                startX: NAND_IEC[0].startX - sim3Const.inputLineLength -
                sim3Const.clickBoxWidth,
                startY: NAND_IEC[0].startY + inputLinesYOff,
                color: sim3Const.passiveStateColor
            };
        inputX.endX = inputX.startX - inputLineLengths;
        inputX.endY = inputX.startY;

        let inputY =
            {
                startX: NAND_IEC[1].startX - sim3Const.inputLineLength -
                sim3Const.clickBoxWidth,
                startY: NAND_IEC[1].startY + inputLinesYOff +
                sim3Const.inputLineDistance,
                color: sim3Const.passiveStateColor
            };
        inputY.endX = inputY.startX - inputLineLengths;
        inputY.endY = inputY.startY;


        drawConnectLine(context, inputX, sim3Const.connectLinesWidth);
        if (X === sim3Const.redLineActiveColor) {
            activeStyle(context, stateNANDSC);
            inputX.color = sim3Const.currentColor;
            drawConnectLine(context, inputX, sim3Const.currentLineWidth);
        }
        passiveStyle(context);


        drawConnectLine(context, inputY, sim3Const.connectLinesWidth);
        if (Y === sim3Const.redLineActiveColor) {
            activeStyle(context, stateNANDSC);
            inputY.color = sim3Const.currentColor;
            drawConnectLine(context, inputY, sim3Const.currentLineWidth);
        }
        passiveStyle(context);


        let textX = {
            leftVertexX: inputX.endX - sim3Const.clickBoxWidth,
            leftVertexY: clickBoxX.leftVertexY,
            boxWidth: sim3Const.clickBoxWidth,
            boxHeight: sim3Const.clickBoxHeight,
            color: "black",
            fillStyle: "white",
            textFont: sim3Const.varTextSize + "px Courier",
            lineWidth: sim3Const.connectLinesWidth
        };

        let textY = {
            leftVertexX: inputY.endX - sim3Const.clickBoxWidth,
            leftVertexY: clickBoxY.leftVertexY,
            boxWidth: sim3Const.clickBoxWidth,
            boxHeight: sim3Const.clickBoxHeight,
            color: "black",
            fillStyle: "white",
            textFont: sim3Const.varTextSize + "px Courier",
            lineWidth: sim3Const.connectLinesWidth
        };
        drawText(context, textX, "X");
        drawText(context, textY, "Y");

        let connectDotQ =
            {
                centerX: downLine.startX,
                centerY: downLine.startY,
                radius: sim3Const.IEEEConnectDotRadius,
                startAngle: toRadians(sim3Const.zeroAngle),
                endAngle: toRadians(sim3Const.fullCircle),
                rotation: false,
                color: sim3Const.passiveStateColor,
                fillStyle: sim3Const.passiveStateColor,
                lineWidth: sim3Const.connectDotLineWidth
            };

        let connectDotNQ =
            {
                centerX: upLine.startX,
                centerY: upLine.startY,
                radius: sim3Const.IEEEConnectDotRadius,
                startAngle: toRadians(sim3Const.zeroAngle),
                endAngle: toRadians(sim3Const.fullCircle),
                rotation: false,
                color: sim3Const.passiveStateColor,
                fillStyle: sim3Const.passiveStateColor,
                lineWidth: sim3Const.connectDotLineWidth
            };
        drawCircle(context, connectDotQ);
        drawCircle(context, connectDotNQ);

        let outQLine = {
            startX: connectDotQ.centerX + connectDotQ.radius,
            startY: connectDotQ.centerY,
            color: sim3Const.passiveStateColor
        };
        outQLine.endX = outQLine.startX + sim3Const.inputLineLength;
        outQLine.endY = outQLine.startY;

        let outNQLine = {
            startX: connectDotNQ.centerX + connectDotNQ.radius,
            startY: connectDotNQ.centerY,
            color: sim3Const.passiveStateColor
        };
        outNQLine.endX = outNQLine.startX + sim3Const.inputLineLength;
        outNQLine.endY = outNQLine.startY;


        drawConnectLine(context, outQLine, sim3Const.connectLinesWidth);
        if (q === sim3Const.redLineActiveColor) {
            activeStyle(context, stateNANDSC);
            outQLine.color = sim3Const.currentColor;
            drawConnectLine(context, outQLine, sim3Const.currentLineWidth);
        }
        passiveStyle(context);

        drawConnectLine(context, outNQLine, sim3Const.connectLinesWidth);
        if (nQ === sim3Const.redLineActiveColor) {
            activeStyle(context, stateNANDSC);
            outNQLine.color = sim3Const.currentColor;
            drawConnectLine(context, outNQLine, sim3Const.currentLineWidth);
        }
        passiveStyle(context);

        //drawConnectLine(context, outQLine, sim3Const.connectLinesWidth);
        //drawConnectLine(context, outNQLine, sim3Const.connectLinesWidth);

    }

    init();
}
