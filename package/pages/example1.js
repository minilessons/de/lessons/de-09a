/* Global constants for this simulation */
let scaleFactor = 1;
let constEx1 = new Constants(scaleFactor, 50, 50, 20, 20, 40);
const boxLineWidth = constEx1.clickBoxLineWidth + 1;

let canvasB1 = document.getElementById("b1Canvas");
let canvasB2 = document.getElementById("b2Canvas");
let contextB1 = canvasB1.getContext("2d");
let contextB2 = canvasB2.getContext("2d");
let canvasD1 = document.getElementById("diagram");
let contextD1 = canvasD1.getContext("2d");

let stateB1 = {
    curOut: 0,
    userInput: [0, 0],
    hots: []
};

let stateB2 = {
    curOut: 0,
    userInput: [0, 0],
    hots: []
};
let stateDiagram = {
    simName: "Dijagram stanja sklopa 2",
    currentRow: 1,
    nextState: 0,
    X: 0,
    Y: 0,
    Q: 0,
    nQ: 0,
    QnNext: 0,
    userInput: [0, 0],
    hots: [],
};
stateDiagram.elems = [
    [0, 0, 0, 0], [0, 0, 1, 0],
    [0, 1, 0, 1], [0, 1, 1, "X"],
    [1, 0, 0, 1], [1, 0, 1, 0],
    [1, 1, 0, 1], [1, 1, 1, "X"]];

let zoomHotsDiagram = {
    hots: [],
    min: 1,
    max: 2
};

saveClickHots(zoomHotsDiagram, 0, 0, canvasD1.width, canvasD1.height, 0);

/**
 * Defines LineHandler template which uses different set of colors.
 * @Note Only for learning purposes...
 */
class LineHandler extends BoxHandler {
    constructor(x, y, w, h, lineWidth, lineLength, inputLineDistance, color) {
        super(x, y, w, h, "black");
        this.lineLength = lineLength;
        this.inputLineDistance = inputLineDistance;
        this.lineWidth = lineWidth;
        this.colorIn1 = color[0];
        this.colorIn2 = color[1];
        this.colorOut = color[2];
    }

    get getColorIn1() {
        return this.colorIn1;
    }

    get getColorIn2() {
        return this.colorIn2;
    }

    get getColorOut() {
        return this.colorOut;
    }
}

/**
 * Defines ClickBoxHandler template which uses specific dimensions.
 * @Note Only for learning purposes...
 */
class ClickBoxHandler extends LineHandler {
    constructor(x, y, w, h, lineWidth, lineLength, inputLineDistance, cWidth, cHeight, color) {
        super(x, y, w, h, lineWidth, lineLength, inputLineDistance, [undefined, undefined, undefined]);
        this.clickBoxWidth = cWidth;
        this.clickBoxHeight = cHeight;
        this.color = color;
    }

    get getColor() {
        return this.color;
    }
}

/**
 * This function resets state of first box inputs and outputs.
 * This is callback function for event <code>mouseup</code> listener of first box.
 */
function setDefaultB1() {
    for (let i = 0, l = stateB1.hots.length; i < l; i++) {
        stateB1.userInput[i] = 0;
    }
    stateB1.curOut = 0;
    drawB1(canvasB1, contextB1);
}

/**
 * This function checks for any mousedown action and activates circuit flow accordingly.
 * This is callback function for event <code>mousedown</code> listener of first box.
 */
function clickBoxEventB1(event) {
    let rect = canvasB1.getBoundingClientRect();
    let pos = {
        x: event.clientX - rect.left,
        y: event.clientY - rect.top
    };

    // Check if mouse is in area which we consider clickable..
    for (let i = 0, l = stateB1.hots.length; i < l; i++) {
        let h = stateB1.hots[i];
        if (pos.x >= h.x && pos.x < h.x + h.w &&
            pos.y >= h.y && pos.y < h.y + h.h) {
            let u = stateB1.userInput[h.row];
            if (u === 0) {
                u = 1;
            } else {
                u = 0;
            }
            stateB1.userInput[h.row] = u;
            stateB1.curOut = 1;
            drawB1(canvasB1, contextB1);
            break;
        }
    }
}
/**
 * This function resets state of second box inputs.
 * This is callback function for event <code>mouseup</code> listener of second box.
 */
function setDefaultB2() {
    for (let i = 0, l = stateB2.hots.length; i < l; i++) {
        stateB2.userInput[i] = 0;
    }
    drawB2(canvasB2, contextB2);
}

/**
 * This function checks for any mousedown action and activates circuit flow accordingly.
 * This is callback function for event <code>mousedown</code> listener of second box.
 */
function clickBoxEventB2(event) {
    let rect = canvasB2.getBoundingClientRect();
    let pos = {
        x: event.clientX - rect.left,
        y: event.clientY - rect.top
    };
    // Check if mouse is in area which we consider..
    for (let i = 0, l = stateB2.hots.length; i < l; i++) {
        let h = stateB2.hots[i];
        if (pos.x >= h.x && pos.x < h.x + h.w &&
            pos.y >= h.y && pos.y < h.y + h.h) {
            let u = stateB2.userInput[h.row];
            if (u === 0) {
                u = 1;
            } else {
                u = 0;
            }
            // If set is pressed
            if (h.row === 0) {
                stateB2.curOut = 1;
            } else if (h.row === 1) {
                // Reset pressed
                stateB2.curOut = 0;
            }
            stateB2.userInput[h.row] = u;
            drawB2(canvasB2, contextB2);
            break;
        }
    }
}

/**
 * This function calculates state of output of first box.
 * @param states list of indexes for userInput
 * @returns {number} value of output with regard to current input values
 */
function calcOutputB1(states) {
    return stateB1.userInput[states[0]] * !stateB1.userInput[states[1]] ||
        !stateB1.userInput[states[0]] * stateB1.userInput[states[1]];
}

/**
 * Function which calculates output for second box.
 * @returns {number} current output state of second box
 */
function calcOutputB2() {
    return stateB2.curOut;
}

function MouseWheelEvent1(e) {
    // Firefox prevent default...
    e.preventDefault();
    mouseZoomer(e, canvasD1, zoomHotsDiagram, scaleFactor, setScaleFactor);
}

function setScaleFactor(newScaleFactor) {
    scaleFactor = newScaleFactor;
    constEx1 = new Constants(scaleFactor, 50, 50, 20, 20, 40);
    drawDiagram(canvasD1, contextD1);
    drawTable(contextD1);
}


/**
 * This function connects events which are used for simulating boxes behaviour.
 */
function eventListeners() {
    canvasB1.addEventListener('mousedown', clickBoxEventB1);
    canvasB2.addEventListener('mousedown', clickBoxEventB2);
    canvasB1.addEventListener('mouseup', setDefaultB1);
    canvasB2.addEventListener('mouseup', setDefaultB2);

    canvasD1.addEventListener("mousewheel", MouseWheelEvent1, false);
    // Firefox
    canvasD1.addEventListener("DOMMouseScroll", MouseWheelEvent1, false);
}

/**
 * Function which draws first box.
 * @param canvas canvas element used to get width and height
 * @param context context in which box is drawn
 */
function drawB1(canvas, context) {
    context.clearRect(0, 0, canvas.width, canvas.height);
    let simName = "Primjer 1 - sklop 1";
    writeSimulationName(canvas, context, simName);
    setBackgroundFrame(context, canvas.width, canvas.height);
    clearStateHots(stateB1);
    let lineColorOut = stateB1.curOut === 0 ? "black" : "red";
    let lineColorIn1 = stateB1.userInput[0] === 0 ? "black" : "red";
    let lineColorIn2 = stateB1.userInput[1] === 0 ? "black" : "red";
    let lineColors = [lineColorIn1, lineColorIn2, lineColorOut];
    let colors = {
        boxColor: "#96B1C4", lineColor: lineColors,
        cBoxColor: "#746dc5"
    };

    // Description: boxXOffset, boxYOffset, boxWidth, boxHeight, lineWidth,
    // lineLength, inputLineDistance, clickBoxWidth, clickBoxHeight, colors...
    let boxWidth = 150;
    let boxHeight = 150;
    let boxXOffset = (canvas.width - boxWidth) / 2;
    let boxYOffset = (canvas.height - boxHeight) / 2;
    let lineLength = 40;
    let inputLineDistance = 70;
    let clickBoxWidth = 35;
    let clickBoxHeight = 35;
    let complementList = [0, 0, 0];
    let opts = setOptions(boxXOffset, boxYOffset, boxWidth, boxHeight,
        boxLineWidth, lineLength, inputLineDistance, clickBoxWidth,
        clickBoxHeight, complementList, colors);

    // Init classes
    let box = new BoxHandler(opts.boxXOff, opts.boxYOff,
        opts.boxWidth, opts.boxHeight, opts.boxColor);
    let lineHandler = new LineHandler(opts.boxXOff, opts.boxYOff,
        opts.boxWidth, opts.boxHeight, opts.lineWidth, opts.lineLength,
        opts.inputLineDistance, opts.lineColor);
    let clickBoxHandler = new ClickBoxHandler(opts.boxXOff, opts.boxYOff,
        opts.boxWidth, opts.boxHeight, opts.lineWidth, opts.lineLength,
        opts.inputLineDistance, opts.clickBoxWidth, opts.clickBoxHeight,
        opts.clickBoxColor);

    // Draw first box: B1
    // States params: first & second, Which state row to set at userInput, (0, 1) in this case
    // third: at which list, first or second.
    let states = [0, 1];
    drawBox(context, box);
    drawLines(context, lineHandler, constEx1);
    drawClickBoxes(context, clickBoxHandler, stateB1, states, calcOutputB1(states));
    drawTextForBox(context, lineHandler, constEx1);

}

/**
 * Function which draws second box.
 * @param canvas canvas element used to get width and height
 * @param context context in which box is drawn
 */
function drawB2(canvas, context) {
    context.clearRect(0, 0, canvas.width, canvas.height);
    let simName = "Primjer 2 - sklop 2";
    writeSimulationName(canvas, context, simName);
    setBackgroundFrame(context, canvas.width, canvas.height);
    clearStateHots(stateB2);

    let lineColorOut = stateB2.curOut === 0 ? "black" : "red";
    let lineColorIn1 = stateB2.userInput[0] === 0 ? "black" : "red";
    let lineColorIn2 = stateB2.userInput[1] === 0 ? "black" : "red";
    let colors = [lineColorIn1, lineColorIn2, lineColorOut];
    let colorsBs = {boxColor: "#CB3A34", lineColor: colors, cBoxColor: "#746dc5"};

    // Description: boxXOffset, boxYOffset, boxWidth, boxHeight, lineWidth,
    // lineLength, inputLineDistance, clickBoxWidth, clickBoxHeight, colors...
    let boxWidth = 140;
    let boxHeight = 140;
    let boxXOffset = (canvas.width - boxWidth) / 2;
    let boxYOffset = (canvas.height - boxHeight) / 2;
    let lineLength = 40;
    let inputLineDistance = 70;
    let clickBoxWidth = 35;
    let clickBoxHeight = 35;
    let complementList = [0, 0, 0];
    let optsBs = setOptions(boxXOffset, boxYOffset, boxWidth, boxHeight,
        boxLineWidth, lineLength, inputLineDistance, clickBoxWidth,
        clickBoxHeight, complementList, colorsBs);

    // Init classes
    let boxBs = new BoxHandler(optsBs.boxXOff, optsBs.boxYOff,
        optsBs.boxWidth, optsBs.boxHeight, optsBs.boxColor);
    let lineHandlerBs = new LineHandler(optsBs.boxXOff, optsBs.boxYOff,
        optsBs.boxWidth, optsBs.boxHeight, optsBs.lineWidth, optsBs.lineLength,
        optsBs.inputLineDistance, optsBs.lineColor);
    let clickBoxHandlerBs = new ClickBoxHandler(optsBs.boxXOff, optsBs.boxYOff,
        optsBs.boxWidth, optsBs.boxHeight, optsBs.lineWidth, optsBs.lineLength,
        optsBs.inputLineDistance,
        optsBs.clickBoxWidth, optsBs.clickBoxHeight, optsBs.clickBoxColor);

    // Draw Second Black Box
    let states = [0, 1];
    drawBox(context, boxBs);
    drawLines(context, lineHandlerBs, constEx1);
    drawClickBoxes(context, clickBoxHandlerBs, stateB2, states, calcOutputB2());
    drawTextForBox(context, clickBoxHandlerBs, constEx1);
}

function drawDiagram(canvas, context) {
    context.clearRect(0, 0, canvas.width, canvas.height);
    writeSimulationName(canvas, context, stateDiagram.simName);
    setBackgroundFrame(context, canvas.width, canvas.height);
    clearStateHots(stateDiagram);

    let circleRadius = constEx1.IEEEArcRadius + 2;
    let diagramDiameter = 2 * circleRadius;
    let circleDistance = 250;
    let alignLeft = 100;
    let xOff = ((canvas.width - circleDistance) / 2) - alignLeft;
    let yOff = (canvas.height - diagramDiameter + circleRadius) / 2;
    drawDiagramGeneric(contextD1, stateDiagram, xOff, yOff, circleDistance, circleRadius, constEx1);
}

function drawTable(context) {
    let letters = ["Y", "A", "B"];
    drawTableGeneric(context, stateDiagram, constEx1, letters);
}

/**
 * This function initialises main draw functions and theirs events.
 */
function init() {
    drawB1(canvasB1, contextB1);
    drawB2(canvasB2, contextB2);
    drawDiagram(canvasD1, contextD1);
    drawTable(contextD1);
    eventListeners();
}

function returnNext(position) {
    let structure = [[0, 0], [0, 1], [1, 0], [1, 1]];
    return structure[position];
}

function mouseUpEvent() {
    for (let i = 0, l = stateDiagram.hots.length; i < l; i++) {
        let h = stateDiagram.hots[i];
        stateDiagram.userInput[h.row] = 0;

    }
    setCurrentStateGeneric(stateDiagram);
    reInit();
}

function startSim2() {
    let h = returnNext(stateDiagram.currentRow % 4);

    stateDiagram.userInput[0] = h[0];
    stateDiagram.userInput[1] = h[1];
    //alert(h);
    if (stateDiagram.currentRow >= 4) {
        stateDiagram.Q = 1;
        stateDiagram.nQ = 0;
    } else {
        stateDiagram.Q = 0;
        stateDiagram.nQ = 1;
    }
    setTimeout(function () {
        setCurrentStateGeneric(stateDiagram);
        reInit();
    }, 500);

    setTimeout(function () {
        setNextStateGeneric(stateDiagram);
        reInit();
    }, 500);
    if (stateDiagram.currentRow >= 8) {
        stateDiagram.currentRow = 1;
        stateDiagram.Q = 0;
    }
    ++stateDiagram.currentRow;

}

function reInit() {
    drawDiagram(canvasD1, contextD1);
    drawTable(contextD1);
}

function startDiagramSimulation() {
    setInterval(startSim2, 2000);
}

// Init twice because of wrong draws
init();
init();
startDiagramSimulation();

