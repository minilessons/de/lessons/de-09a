scopeFunction4B();

function scopeFunction4B() {

    let canvas = document.getElementById("NANDB");
    let context = canvas.getContext("2d");
    let mouseHots2 = {
        hots: [],
        min: 1.4,
        max: 2
    };

    let scaleFactor = 2;
    saveClickHots(mouseHots2, 0, 0, canvas.width, canvas.height, 0);

    let stateNANDSB = {
        simName: "Bistabil ostvaren NI sklopovima\n" +
        "(jedan ulaz fiksiran na jedinicu)",
        Q: 1,
        nQ: 0,
        constOne: 1,
        userInput: [0, 0],
        hots: [],
        animationOffset: 0,
        animationPattern: [20, 12]
    };

    /**
     * This function checks for any mousedown action and activates circuit flow accordingly.
     * This is callback function for event <code>mousedown</code> listener of clickable box.
     * @param event event for this action
     */
    function clickLineEvent(event) {
        let rect = canvas.getBoundingClientRect();
        let pos = {
            x: event.clientX - rect.left,
            y: event.clientY - rect.top
        };
        // Check if mouse is in area which we consider..
        for (let i = 0, l = stateNANDSB.hots.length; i < l; i++) {
            let h = stateNANDSB.hots[i];
            if (pos.x >= h.x && pos.x < h.x + h.w &&
                pos.y >= h.y && pos.y < h.y + h.h) {
                stateNANDSB.Q = 1 - stateNANDSB.Q;
                stateNANDSB.nQ = 1 - stateNANDSB.nQ;
                drawSketch(canvas, context);
                break;
            }
        }
    }

    function mouseUpEvent() {
        // Do nothing...
    }

    function setScaleFactor(newScaleFactor) {
        scaleFactor = newScaleFactor;
        drawSketch(canvas, context);
    }


    function MouseWheelEvent2(e) {
        // Firefox prevent default...
        e.preventDefault();
        mouseZoomer(e, canvas, mouseHots2, scaleFactor, setScaleFactor);
    }

    /**
     * Adds mouse down and mouse up listeners to simulation.
     * If mouse is pressed inverters will change state. After releasing the mouse button,
     * inverters hold their state.
     */
    function eventListeners(canvas) {
        canvas.addEventListener('mousedown', clickLineEvent);
        canvas.addEventListener('mouseup', mouseUpEvent);
        canvas.addEventListener("mousewheel", MouseWheelEvent2, false);
        canvas.addEventListener("DOMMouseScroll", MouseWheelEvent2, false);
    }

    function startSimulation() {
        stateNANDSB.animationOffset++;
        if (stateNANDSB.animationOffset > 2000) {
            stateNANDSB.animationOffset = 0;
        }
        drawSketch(canvas, context);
        setTimeout(startSimulation, 20);
    }

    /**
     * Initialises whole simulation. Draws sketch and adds event listeners.
     */
    function init() {
        drawSketch(canvas, context);
        drawSketch(canvas, context);
        eventListeners(canvas);
        startSimulation();
    }

    /**
     * Draws inverters in given context and their connecting lines.
     * @param canvas canvas to draw sketch
     * @param context context to draw sketch
     */
    function drawSketch(canvas, context) {
        context.clearRect(0, 0, canvas.width, canvas.height);
        writeSimulationName(canvas, context, stateNANDSB.simName);
        setBackgroundFrame(context, canvas.width, canvas.height);
        clearStateHots(stateNANDSB);

        let sim2Const = new Constants(scaleFactor);

        // Set some useful variables
        let connectLineLength = 60 * scaleFactor;

        /* Sketch Globals */
        let sketchLengthSim2 = (2 * (sim2Const.NANDBoxWidth) +
        2 * sim2Const.IEEEComplementDiameter +
        4 * sim2Const.inputLineLength + connectLineLength + sim2Const.constBoxWidth);

        /* Starting point for first NAND, important for fixing center on canvas */
        let canvasCenter = (canvas.width - sketchLengthSim2 -
            2 * sim2Const.outLineLength) / 2;
        let xOff = canvasCenter + sim2Const.outLineLength +
            sim2Const.inputLineLength;
        let yOff = ((canvas.height - sim2Const.NANDBoxHeight / 2) / 2 );


        // Changing colors
        let q = (stateNANDSB.Q === 1)
            ? sim2Const.redLineActiveColor : sim2Const.passiveStateColor;
        let nQ = (stateNANDSB.nQ === 1)
            ? sim2Const.redLineActiveColor : sim2Const.passiveStateColor;
        let constOne = (stateNANDSB.constOne === 1)
            ? sim2Const.redLineActiveColor : sim2Const.passiveStateColor;

        let inputLinesYOff = (sim2Const.NANDBoxHeight
            - sim2Const.inputLineDistance) / 2;
        let NAND_IEC = [
            {
                startX: xOff,
                startY: yOff,
                boxWidth: sim2Const.NANDBoxWidth,
                boxHeight: sim2Const.NANDBoxHeight,
                color: sim2Const.frameBlacked,
                fillStyle: "white",
                textFont: sim2Const.IECFontSize + "px Courier",
                complementList: [0, 0, 1],
                lineColorsIO: [constOne, nQ, q],
                state: stateNANDSB,
                clickHotsIO: [false, true, true]
            }
        ];

        let connectLine1 = {};
        connectLine1.startX = xOff + sim2Const.lineLength + sim2Const.IEEEArcRadius +
            sim2Const.IEEEComplementDiameter + sim2Const.inputLineLength;
        connectLine1.startY = yOff + sim2Const.NANDBoxHeight / 2;
        connectLine1.endX = connectLine1.startX;
        connectLine1.endY = connectLine1.startY + sim2Const.inputLineDistance / 2;
        connectLine1.color = sim2Const.passiveStateColor;

        let connectLine2 = {
            startX: connectLine1.endX,
            startY: connectLine1.endY,
            endX: connectLine1.endX + connectLineLength + sim2Const.constBoxWidth,
            endY: connectLine1.endY,
            color: sim2Const.passiveStateColor
        };

        let NAND_IEC2 =
            {
                startX: connectLine2.endX + sim2Const.inputLineLength,
                startY: yOff,
                boxWidth: sim2Const.NANDBoxWidth,
                boxHeight: sim2Const.NANDBoxHeight,
                color: sim2Const.frameBlacked,
                fillStyle: "white",
                textFont: sim2Const.IECFontSize + "px Courier",
                complementList: [0, 0, 1],
                lineColorsIO: [constOne, q, nQ],
                state: stateNANDSB,
                clickHotsIO: [false, true, true]
            };

        NAND_IEC = NAND_IEC.concat(NAND_IEC2);
        setGradient(context, NAND_IEC);
        let clickHotsOffset = 10;

        drawConnectLine(context, connectLine1, sim2Const.connectLinesWidth);
        if (q === sim2Const.redLineActiveColor) {
            activeStyle(context, stateNANDSB);
            connectLine1.color = sim2Const.currentColor;
            drawConnectLine(context, connectLine1, sim2Const.currentLineWidth);
        }
        passiveStyle(context);


        //drawConnectLine(context, connectLine1, sim2Const.connectLinesWidth);

        drawConnectLine(context, connectLine2, sim2Const.connectLinesWidth);
        if (q === sim2Const.redLineActiveColor) {
            activeStyle(context, stateNANDSB);
            connectLine2.color = sim2Const.currentColor;
            drawConnectLine(context, connectLine2, sim2Const.currentLineWidth);
        }
        passiveStyle(context);
        //drawConnectLine(context, connectLine2, sim2Const.connectLinesWidth);
        saveConnectLineClickHots(context, stateNANDSB,
            connectLine1, clickHotsOffset, 1);
        saveConnectLineClickHots(context, stateNANDSB,
            connectLine2, clickHotsOffset, 0);
        let logicGates = new LogicGates();
        drawRectangularBox(context, NAND_IEC[0], logicGates.ANDSIGN, sim2Const);
        drawRectangularBox(context, NAND_IEC[1], logicGates.ANDSIGN, sim2Const);

        // Draw returning line
        let returnLine = {
            startX: NAND_IEC[1].startX + sim2Const.lineLength + sim2Const.IEEEArcRadius +
            sim2Const.inputLineLength + sim2Const.IEEEComplementDiameter,
            startY: NAND_IEC[1].startY + sim2Const.NANDBoxHeight / 2,
            endAtY: yOff + sim2Const.NANDBoxHeight - inputLinesYOff,
            sketchLength: sketchLengthSim2,
            color: sim2Const.passiveStateColor,
            saveClickHots: true,
            state: stateNANDSB
        };


        drawReturnConnector(context, returnLine, sim2Const);
        if (nQ === sim2Const.redLineActiveColor) {
            activeStyle(context, stateNANDSB);
            returnLine.color = sim2Const.currentColor;
            drawReturnConnector(context, returnLine, sim2Const,
                sim2Const.currentLineWidth);
        }
        passiveStyle(context);

        let constOneBox = {
            leftVertexX: xOff - sim2Const.inputLineLength - sim2Const.constBoxWidth,
            leftVertexY: yOff + inputLinesYOff - sim2Const.constBoxHeight / 2,
            boxWidth: sim2Const.constBoxWidth,
            boxHeight: sim2Const.constBoxHeight,
            state: stateNANDSB,
            color: "black",
            fillStyle: "white",
            textFont: "20px Courier",
            lineWidth: sim2Const.constBoxLineWidth
        };
        // Draws text boxes, and Q and nQ text
        drawTextBox(context, constOneBox, stateNANDSB.constOne, 0, sim2Const);

        let constOneBox2 = {
            leftVertexX: NAND_IEC[1].startX - sim2Const.inputLineLength -
            sim2Const.constBoxWidth,
            leftVertexY: NAND_IEC[1].startY + inputLinesYOff -
            sim2Const.constBoxHeight / 2,
            boxWidth: sim2Const.constBoxWidth,
            boxHeight: sim2Const.constBoxHeight,
            state: stateNANDSB,
            color: "black",
            fillStyle: "white",
            textFont: "20px Courier",
            lineWidth: sim2Const.constBoxLineWidth
        };
        // Draws text boxes, and Q and nQ text
        let withOverLine = true;
        let shiftNQ = 20;
        let shiftQ = 25;
        drawTextBox(context, constOneBox2, stateNANDSB.constOne, 0, sim2Const);
        genericDrawOutText(context, NAND_IEC[0], "Q =", " " + stateNANDSB.Q, sim2Const, 0, shiftQ);
        genericDrawOutText(context, NAND_IEC[1], "Q =", " " + stateNANDSB.nQ,
            sim2Const, 0, shiftNQ, withOverLine);
    }

    init();

}
