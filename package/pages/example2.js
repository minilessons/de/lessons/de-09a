scopeFunction2();

function scopeFunction2() {

    let canvasB1 = document.getElementById("boxB1");
    let contextB1 = canvasB1.getContext("2d");
    let canvasB2 = document.getElementById("boxB2");
    let contextB2 = canvasB2.getContext("2d");
    let canvasB3 = document.getElementById("boxB3");
    let contextB3 = canvasB3.getContext("2d");


    /* Global constants for this simulation */
    let scaleFactor = 1;
    let constEx2 = new Constants(scaleFactor, 50, 50, 20, 20, 40);
    const boxLineWidth = constEx2.clickBoxLineWidth + 1;

    let stateB1 = {
        simName: "Primjer 1",
        curOut: 0,
        userInput: [0, 0],
        hots: []
    };
    let stateB2 = {
        simName: "Primjer 2",
        cp: 0,
        curOut: 0,
        userInput: [0, 0],
        hots: []
    };
    let stateB3 = {
        simName: "Primjer 3",
        cpEdge: 0,
        cp: 0,
        curOut: 0,
        userInput: [0, 0],
        hots: []
    };

    /**
     * Defines LineHandler template which uses different set of colors.
     * @Note Only for learning purposes...
     */
    class LineHandler extends BoxHandler {
        constructor(x, y, w, h, lineWidth, lineLength, inputLineDistance, color) {
            super(x, y, w, h, "black");
            this.lineWidth = lineWidth;
            this.lineLength = lineLength;
            this.inputLineDistance = inputLineDistance;
            this.colorIn1 = color[0];
            this.colorIn2 = color[1];
            this.colorOut = color[2];
            this.colorCp = color[3]
        }

        get getColorIn1() {
            return this.colorIn1;
        }

        get getColorIn2() {
            return this.colorIn2;
        }

        get getColorOut() {
            return this.colorOut;
        }

        get getColorCp() {
            return this.colorCp;
        }
    }

    /**
     * Defines ClickBoxHandler template which uses specific dimensions.
     * @Note Only for learning purposes...
     */
    class ClickBoxHandler extends LineHandler {
        constructor(x, y, w, h, lineWidth, lineLength,
                    lineDist, cWidth, cHeight, color) {
            super(x, y, w, h, lineWidth, lineLength, lineDist, [undefined, undefined, undefined, undefined]);
            this.clickBoxWidth = cWidth;
            this.clickBoxHeight = cHeight;
            this.color = color;
        }

        get getColor() {
            return this.color;
        }
    }

    init();
    /**
     * This function initialises main draw functions and theirs events.
     */
    function init() {

        drawB1(canvasB1, contextB1);
        drawB1(canvasB1, contextB1);
        drawB2(canvasB2, contextB2);
        drawB2(canvasB2, contextB2);
        drawB3(canvasB3, contextB3);
        drawB3(canvasB3, contextB3);

        eventListeners();
        setInterval(animateCP, 2000);
        setInterval(animateRaisingCP, 2000);
    }

    /**
     * This function connects events which are used for simulating boxes behaviour.
     */
    function eventListeners() {
        canvasB1.addEventListener('mousedown', clickBoxEventB1);
        canvasB2.addEventListener('mousedown', clickBoxEventB2);
        canvasB3.addEventListener('mousedown', clickBoxEventB3);
        canvasB1.addEventListener('mouseup', setDefaultB1);
        canvasB2.addEventListener('mouseup', setDefaultB2);
        canvasB3.addEventListener('mouseup', setDefaultB3);
    }

    /**
     * This function resets state of first box inputs and outputs.
     * This is callback function for event <code>mouseup</code> listener of first box.
     */
    function setDefaultB1() {
        for (let i = 0, l = stateB1.hots.length; i < l; i++) {
            stateB1.userInput[i] = 0;
        }
        drawB1(canvasB1, contextB1);
    }

    /**
     * This function checks for any mousedown action and activates circuit flow accordingly.
     * This is callback function for event <code>mousedown</code> listener of second box.
     */
    function clickBoxEventB1(event) {
        let rect = canvasB1.getBoundingClientRect();
        let pos = {
            x: event.clientX - rect.left,
            y: event.clientY - rect.top
        };
        // Check if mouse is in area which we consider..
        for (let i = 0, l = stateB1.hots.length; i < l; i++) {
            let h = stateB1.hots[i];
            if (pos.x >= h.x && pos.x < h.x + h.w &&
                pos.y >= h.y && pos.y < h.y + h.h) {
                let u = stateB1.userInput[h.row];
                if (u === 0) {
                    u = 1;
                } else {
                    u = 0;
                }
                // If set is pressed
                if (h.row === 0) {
                    stateB1.curOut = 1;
                } else if (h.row === 1) {
                    // Reset pressed
                    stateB1.curOut = 0;
                }
                stateB1.userInput[h.row] = u;
                drawB1(canvasB1, contextB1);
                break;
            }
        }
    }

    /**
     * This function resets state of second box inputs.
     * This is callback function for event <code>mouseup</code> listener of second box.
     */
    function setDefaultB2() {
        for (let i = 0, l = stateB2.hots.length; i < l; i++) {
            stateB2.userInput[i] = 0;
        }
        drawB2(canvasB2, contextB2);
    }

    /**
     * This function checks for any mousedown action and activates circuit flow accordingly.
     * This is callback function for event <code>mousedown</code> listener of second box.
     */
    function clickBoxEventB2(event) {
        let rect = canvasB2.getBoundingClientRect();
        let pos = {
            x: event.clientX - rect.left,
            y: event.clientY - rect.top
        };
        // Check if mouse is in area which we consider..
        for (let i = 0, l = stateB2.hots.length; i < l; i++) {
            let h = stateB2.hots[i];
            let cp = stateB2.cp;
            if (pos.x >= h.x && pos.x < h.x + h.w &&
                pos.y >= h.y && pos.y < h.y + h.h) {
                let u = stateB2.userInput[h.row];
                if (u === 0) {
                    u = 1;
                } else {
                    u = 0;
                }
                // If set is pressed & CP is 1
                if (h.row === 0 && cp === 1) {
                    stateB2.curOut = 1;
                } else if (h.row === 1 && cp === 1) {
                    // Reset pressed
                    stateB2.curOut = 0;
                }
                stateB2.userInput[h.row] = u;
                drawB2(canvasB2, contextB2);
                break;
            }
        }
    }

    /**
     * This function resets state of first box inputs and outputs.
     * This is callback function for event <code>mouseup</code> listener of first box.
     */
    function setDefaultB3() {
        for (let i = 0, l = stateB3.hots.length; i < l; i++) {
            stateB3.userInput[i] = 0;
        }
        drawB3(canvasB3, contextB3);
    }

    /**
     * This function checks for any mousedown action and activates circuit flow accordingly.
     * This is callback function for event <code>mousedown</code> listener of second box.
     */
    function clickBoxEventB3(event) {
        let rect = canvasB3.getBoundingClientRect();
        let pos = {
            x: event.clientX - rect.left,
            y: event.clientY - rect.top
        };
        // Check if mouse is in area which we consider..
        for (let i = 0, l = stateB3.hots.length; i < l; i++) {
            let h = stateB3.hots[i];
            if (pos.x >= h.x && pos.x < h.x + h.w &&
                pos.y >= h.y && pos.y < h.y + h.h) {
                let u = stateB3.userInput[h.row];
                if (u === 0) {
                    u = 1;
                } else {
                    u = 0;
                }
                stateB3.userInput[h.row] = u;
                drawB3(canvasB3, contextB3);
                break;
            }
        }
    }

    /**
     * Function which draws third box.
     * @param canvas canvas in which box is drawn
     * @param context context in which box is drawn
     */
    function drawB3(canvas, context) {
        context.clearRect(0, 0, canvas.width, canvas.height);
        writeSimulationName(canvas, context, stateB3.simName);
        setBackgroundFrame(context, canvas.width, canvas.height);
        clearStateHots(stateB1);
        clearStateHots(stateB2);
        clearStateHots(stateB3);

        let lineColorOut = stateB3.curOut === 0 ? "black" : "red";
        let lineColorIn1 = stateB3.userInput[0] === 0 ? "black" : "red";
        let lineColorIn2 = stateB3.userInput[1] === 0 ? "black" : "red";
        let lineColorCp = stateB3.cp === 0 ? "black" : "red";
        let colors = [lineColorIn1, lineColorIn2, lineColorOut, lineColorCp];
        let colorsBs = {boxColor: "#CB3A34", lineColor: colors, cBoxColor: "#746dc5"};

        // Description: boxXOffset, boxYOffset, boxWidth, boxHeight, lineWidth,
        // lineLength, inputLineDistance, clickBoxWidth, clickBoxHeight, colors...
        let boxWidth = 150;
        let boxHeight = 150;
        let cpLength = 30;
        let boxXOffset = (canvas.width - boxWidth) / 2;
        let boxYOffset = (canvas.height - boxHeight - cpLength) / 2;
        let lineLength = 40;
        let inputLineDistance = 70;
        let clickBoxWidth = 35;
        let clickBoxHeight = 35;
        let complementList = [0, 0, 0];
        let optsBs = setOptions(boxXOffset, boxYOffset, boxWidth, boxHeight,
            boxLineWidth, lineLength, inputLineDistance, clickBoxWidth, clickBoxHeight,
            complementList, colorsBs);

        // Init classes
        let boxBs = new BoxHandler(optsBs.boxXOff, optsBs.boxYOff,
            optsBs.boxWidth, optsBs.boxHeight, optsBs.boxColor);

        let linesIEEE = new IEEELineHandler(optsBs.boxXOff, optsBs.boxYOff,
            optsBs.boxWidth, optsBs.boxHeight, optsBs.complementList, constEx2.IEEEComplementDiameter,
            optsBs.lineWidth, optsBs.lineLength,
            optsBs.inputLineDistance, optsBs.lineColor);

        let lineHandlerBs = new LineHandler(optsBs.boxXOff, optsBs.boxYOff,
            optsBs.boxWidth, optsBs.boxHeight, optsBs.lineWidth, optsBs.lineLength,
            optsBs.inputLineDistance, optsBs.lineColor);

        let clickBoxHandlerBs = new ClickBoxHandler(optsBs.boxXOff, optsBs.boxYOff,
            optsBs.boxWidth, optsBs.boxHeight, optsBs.lineWidth, optsBs.lineLength,
            optsBs.inputLineDistance,
            optsBs.clickBoxWidth, optsBs.clickBoxHeight, optsBs.clickBoxColor);
        // Draw first Black Box
        let states = [0, 1];
        drawBox(context, boxBs);
        drawLines(context, linesIEEE, constEx2);
        drawClickBoxes(context, clickBoxHandlerBs, stateB3, states, calcOutputB3());
        drawTextForBox(context, lineHandlerBs, constEx2);
        drawCP(context, lineHandlerBs, constEx2);

        // Animate CP
        //animateCP(canvas, context);
    }

    /**
     * Function which draws second box.
     * @param canvas canvas in which box is drawn
     * @param context context in which box is drawn
     */
    function drawB2(canvas, context) {
        context.clearRect(0, 0, canvas.width, canvas.height);
        setBackgroundFrame(context, canvas.width, canvas.height);
        writeSimulationName(canvas, context, stateB2.simName);

        let lineColorOut = stateB2.curOut === 0 ? "black" : "red";
        let lineColorIn1 = stateB2.userInput[0] === 0 ? "black" : "red";
        let lineColorIn2 = stateB2.userInput[1] === 0 ? "black" : "red";
        let lineColorCp = stateB2.cp === 0 ? "black" : "red";
        let colors = [lineColorIn1, lineColorIn2, lineColorOut, lineColorCp];
        let colorsBs = {boxColor: "#CB3A34", lineColor: colors, cBoxColor: "#746dc5"};

        // Description: boxXOffset, boxYOffset, boxWidth, boxHeight, lineWidth,
        // lineLength, inputLineDistance, clickBoxWidth, clickBoxHeight, colors...
        let boxWidth = 150;
        let boxHeight = 150;
        let cpLength = 30;
        let boxXOffset = (canvas.width - boxWidth) / 2;
        let boxYOffset = (canvas.height - boxHeight - cpLength) / 2;
        let lineLength = 40;
        let inputLineDistance = 70;
        let clickBoxWidth = 35;
        let clickBoxHeight = 35;
        let complementList = [0, 0, 0];
        let optsBs = setOptions(boxXOffset, boxYOffset, boxWidth, boxHeight,
            boxLineWidth, lineLength, inputLineDistance, clickBoxWidth,
            clickBoxHeight, complementList, colorsBs);

        // Init classes
        let boxBs = new BoxHandler(optsBs.boxXOff, optsBs.boxYOff,
            optsBs.boxWidth, optsBs.boxHeight, optsBs.boxColor);

        let linesIEEE = new IEEELineHandler(optsBs.boxXOff, optsBs.boxYOff,
            optsBs.boxWidth, optsBs.boxHeight, optsBs.complementList, constEx2.IEEEComplementDiameter,
            optsBs.lineWidth, optsBs.lineLength,
            optsBs.inputLineDistance, optsBs.lineColor);
        let lineHandlerBs = new LineHandler(optsBs.boxXOff, optsBs.boxYOff,
            optsBs.boxWidth, optsBs.boxHeight, optsBs.lineWidth, optsBs.lineLength,
            optsBs.inputLineDistance, optsBs.lineColor);

        let clickBoxHandlerBs = new ClickBoxHandler(optsBs.boxXOff, optsBs.boxYOff,
            optsBs.boxWidth, optsBs.boxHeight, optsBs.lineWidth, optsBs.lineLength,
            optsBs.inputLineDistance,
            optsBs.clickBoxWidth, optsBs.clickBoxHeight, optsBs.clickBoxColor);
        // Draw first Black Box
        let states = [0, 1];
        drawBox(context, boxBs);
        drawLines(context, linesIEEE, constEx2);
        drawClickBoxes(context, clickBoxHandlerBs, stateB2, states, calcOutputB2());
        drawTextForBox(context, lineHandlerBs, constEx2);
        drawCP(context, lineHandlerBs, constEx2);
    }

    /**
     * Function which draws first box.
     * @param canvas canvas in which box is drawn
     * @param context context in which box is drawn
     */
    function drawB1(canvas, context) {
        context.clearRect(0, 0, canvas.width, canvas.height);
        setBackgroundFrame(context, canvas.width, canvas.height);
        writeSimulationName(canvas, context, stateB1.simName);

        let lineColorOut = (stateB1.curOut === 0) ? "black" : "red";
        let lineColorIn1 = (stateB1.userInput[0] === 0) ? "black" : "red";
        let lineColorIn2 = (stateB1.userInput[1] === 0) ? "black" : "red";
        let colors = [lineColorIn1, lineColorIn2, lineColorOut];
        let colorsBs = {boxColor: "#CB3A34", lineColor: colors, cBoxColor: "#746dc5"};

        // Description: boxXOffset, boxYOffset, boxWidth, boxHeight, lineWidth,
        // lineLength, inputLineDistance, clickBoxWidth, clickBoxHeight, colors...
        let boxWidth = 150;
        let boxHeight = 150;
        let boxXOffset = (canvas.width - boxWidth) / 2;
        let boxYOffset = (canvas.height - boxHeight) / 2;
        let lineLength = 40;
        let inputLineDistance = 70;
        let clickBoxWidth = 35;
        let clickBoxHeight = 35;
        let complementList = [0, 0, 0];
        let optsBs = setOptions(boxXOffset, boxYOffset, boxWidth, boxHeight,
            boxLineWidth, lineLength, inputLineDistance, clickBoxWidth,
            clickBoxHeight, complementList, colorsBs);

        // Init classes
        let boxBs = new BoxHandler(optsBs.boxXOff, optsBs.boxYOff,
            optsBs.boxWidth, optsBs.boxHeight, optsBs.boxColor);

        let linesIEEE = new IEEELineHandler(optsBs.boxXOff, optsBs.boxYOff,
            optsBs.boxWidth, optsBs.boxHeight, optsBs.complementList,
            constEx2.IEEEComplementDiameter,
            optsBs.lineWidth, optsBs.lineLength,
            optsBs.inputLineDistance, optsBs.lineColor);
        let lineHandlerBs = new LineHandler(optsBs.boxXOff, optsBs.boxYOff,
            optsBs.boxWidth, optsBs.boxHeight, optsBs.lineWidth, optsBs.lineLength,
            optsBs.inputLineDistance, optsBs.lineColor);

        let clickBoxHandlerBs = new ClickBoxHandler(optsBs.boxXOff, optsBs.boxYOff,
            optsBs.boxWidth, optsBs.boxHeight, optsBs.lineWidth, optsBs.lineLength,
            optsBs.inputLineDistance,
            optsBs.clickBoxWidth, optsBs.clickBoxHeight, optsBs.clickBoxColor);

        // Draw first Black Box
        let states = [0, 1];
        drawBox(context, boxBs);
        drawLines(context, linesIEEE, constEx2);
        drawClickBoxes(context, clickBoxHandlerBs, stateB1, states, calcOutputB1());
        drawTextForBox(context, lineHandlerBs, constEx2);
    }

    /**
     * Function which calculates output for first box.
     * @returns {number} current output state of first box
     */
    function calcOutputB1() {
        return stateB1.curOut;
    }

    /**
     * Function which calculates output for second box.
     * @returns {number} current output state of second box
     */
    function calcOutputB2() {
        return stateB2.curOut;
    }

    function calcOutputB3() {
        return stateB3.curOut;
    }

    /* To animate correctly, change state and drawB2
     When user clicks something check current cp and react if it is appropriate */
    function animateCP() {
        stateB2.cp = 1 - stateB2.cp;
        if (stateB2.userInput[0] === 1 && stateB2.cp === 1) {
            stateB2.curOut = 1;
        }
        if (stateB2.userInput[1] === 1 && stateB2.cp === 1) {
            stateB2.curOut = 0;
        }
        drawB2(canvasB2, contextB2);
    }

// Only when raising edge, check for user input.
// User input is set if user clicks on something..
    /**
     * Animates flip flop which has raising edge behaviour.
     */
    function animateRaisingCP() {
        stateB3.cp = 1 - stateB3.cp;
        if (stateB3.cp === 1) {
            stateB3.cpEdge = 1;
        } else {
            stateB3.cpEdge = 0;
        }
        if (stateB3.cpEdge === 1 && stateB3.userInput[0] == 1) {
            stateB3.curOut = 1;
        }
        if (stateB3.cpEdge === 1 && stateB3.userInput[1] == 1) {
            stateB3.curOut = 0;
        }
        stateB3.cpEdge = 0;
        drawB3(canvasB3, contextB3);
    }
}
