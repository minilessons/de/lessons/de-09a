/**
 * Created by Komediruzecki on 2.12.2016..
 */
scopeFunction();

function scopeFunction() {

    let canvas = document.getElementById("NOR_B");
    let context = canvas.getContext("2d");
    let zoomHotsNORB = {
        hots: [],
        min: 1.4,
        max: 2
    };


    let scaleFactor = 2;
    saveClickHots(zoomHotsNORB, 0, 0, canvas.width, canvas.height, 0);

    let stateNORB = {
        simName: "Bistabil ostvaren NILI sklopovima\n" +
        "(jedan ulaz fiksiran na nulu)",
        Q: 1,
        nQ: 0,
        constOne: 1,
        constZero: 0,
        userInput: [0, 0],
        hots: [],
        animationOffset: 0,
        animationPattern: [20, 12]
    };

    /**
     * This function checks for any mousedown action and activates circuit flow accordingly.
     * This is callback function for event <code>mousedown</code> listener of clickable box.
     * @param event event for this action
     */
    function clickLineEvent(event) {
        let rect = canvas.getBoundingClientRect();
        let pos = {
            x: event.clientX - rect.left,
            y: event.clientY - rect.top
        };
        // Check if mouse is in area which we consider..
        for (let i = 0, l = stateNORB.hots.length; i < l; i++) {
            let h = stateNORB.hots[i];
            if (pos.x >= h.x && pos.x < h.x + h.w &&
                pos.y >= h.y && pos.y < h.y + h.h) {
                stateNORB.Q = 1 - stateNORB.Q;
                stateNORB.nQ = 1 - stateNORB.nQ;
                drawSketch(canvas, context);
                break;
            }
        }
    }

    function mouseUpEvent() {
        // Do nothing...
    }

    function setScaleFactor(newScaleFactor) {
        scaleFactor = newScaleFactor;
        drawSketch(canvas, context);
    }


    function MouseWheelEvent2(e) {
        // Firefox prevent default...
        e.preventDefault();
        mouseZoomer(e, canvas, zoomHotsNORB, scaleFactor, setScaleFactor);
    }

    /**
     * Adds mouse down and mouse up listeners to simulation.
     * If mouse is pressed inverters will change state. After releasing the mouse button,
     * inverters hold their state.
     */
    function eventListeners(canvas) {
        canvas.addEventListener('mousedown', clickLineEvent);
        canvas.addEventListener('mouseup', mouseUpEvent);
        canvas.addEventListener("mousewheel", MouseWheelEvent2, false);
        canvas.addEventListener("DOMMouseScroll", MouseWheelEvent2, false);
    }

    function startSimulation() {
        stateNORB.animationOffset++;
        if (stateNORB.animationOffset > 2000) {
            stateNORB.animationOffset = 0;
        }
        drawSketch(canvas, context);
        setTimeout(startSimulation, 20);
    }

    /**
     * Initialises whole simulation. Draws sketch and adds event listeners.
     */
    function init() {
        drawSketch(canvas, context);
        drawSketch(canvas, context);
        eventListeners(canvas);
        startSimulation();
    }

    /**
     * Draws inverters in given context and their connecting lines.
     * @param canvas canvas to draw sketch
     * @param context context to draw sketch
     */
    function drawSketch(canvas, context) {
        context.clearRect(0, 0, canvas.width, canvas.height);
        clearStateHots(stateNORB);
        writeSimulationName(canvas, context, stateNORB.simName);
        setBackgroundFrame(context, canvas.width, canvas.height);

        let sim5BConst = new Constants(scaleFactor);

        // Set some useful variables
        let connectLineLength = 60 * scaleFactor;

        /* Sketch Globals */
        let sketchLengthSim2 = (2 * (sim5BConst.NANDBoxWidth) +
        2 * sim5BConst.IEEEComplementDiameter +
        4 * sim5BConst.inputLineLength + connectLineLength + sim5BConst.constBoxWidth);

        /* Starting point for first NAND, important for fixing center on canvas */
        let canvasCenter = (canvas.width - sketchLengthSim2 -
            2 * sim5BConst.outLineLength) / 2;
        let xOff = canvasCenter + sim5BConst.outLineLength +
            sim5BConst.inputLineLength;
        let yOff = ((canvas.height - sim5BConst.NANDBoxHeight / 2) / 2 );


        // Changing colors
        let q = (stateNORB.Q === 1)
            ? sim5BConst.redLineActiveColor : sim5BConst.passiveStateColor;
        let nQ = (stateNORB.nQ === 1)
            ? sim5BConst.redLineActiveColor : sim5BConst.passiveStateColor;
        let constOne = (stateNORB.constZero === 1)
            ? sim5BConst.redLineActiveColor : sim5BConst.passiveStateColor;

        let inputLinesYOff = (sim5BConst.NANDBoxHeight
            - sim5BConst.inputLineDistance) / 2;
        let NAND_IEC = [
            {
                startX: xOff,
                startY: yOff,
                boxWidth: sim5BConst.NANDBoxWidth,
                boxHeight: sim5BConst.NANDBoxHeight,
                color: sim5BConst.frameBlacked,
                fillStyle: "white",
                textFont: sim5BConst.IECFontSize + "px Courier",
                complementList: [0, 0, 1],
                lineColorsIO: [constOne, nQ, q],
                state: stateNORB,
                clickHotsIO: [false, true, true]
            }
        ];

        let connectLine1 = {};
        connectLine1.startX = xOff + sim5BConst.lineLength +
            sim5BConst.IEEEArcRadius + sim5BConst.IEEEComplementDiameter
            + sim5BConst.inputLineLength;
        connectLine1.startY = yOff + sim5BConst.NANDBoxHeight / 2;
        connectLine1.endX = connectLine1.startX;
        connectLine1.endY = connectLine1.startY + sim5BConst.inputLineDistance / 2;
        connectLine1.color = sim5BConst.passiveStateColor;

        let connectLine2 = {
            startX: connectLine1.endX,
            startY: connectLine1.endY,
            endX: connectLine1.endX + connectLineLength + sim5BConst.constBoxWidth,
            endY: connectLine1.endY,
            color: sim5BConst.passiveStateColor
        };

        let NAND_IEC2 =
            {
                startX: connectLine2.endX + sim5BConst.inputLineLength,
                startY: yOff,
                boxWidth: sim5BConst.NANDBoxWidth,
                boxHeight: sim5BConst.NANDBoxHeight,
                color: sim5BConst.frameBlacked,
                fillStyle: "white",
                textFont: sim5BConst.IECFontSize + "px Courier",
                complementList: [0, 0, 1],
                lineColorsIO: [constOne, q, nQ],
                state: stateNORB,
                clickHotsIO: [false, true, true]
            };

        NAND_IEC = NAND_IEC.concat(NAND_IEC2);
        setGradient2(context, NAND_IEC);
        let clickHotsOffset = sim5BConst.clickHotsOffset;

        drawConnectLine(context, connectLine1, sim5BConst.connectLinesWidth);
        if (q === sim5BConst.redLineActiveColor) {
            activeStyle(context, stateNORB);
            connectLine1.color = sim5BConst.currentColor;
            drawConnectLine(context, connectLine1, sim5BConst.currentLineWidth);
        }
        passiveStyle(context);

        drawConnectLine(context, connectLine2, sim5BConst.connectLinesWidth);
        if (q === sim5BConst.redLineActiveColor) {
            activeStyle(context, stateNORB);
            connectLine2.color = sim5BConst.currentColor;
            drawConnectLine(context, connectLine2, sim5BConst.currentLineWidth);
        }
        passiveStyle(context);

        saveConnectLineClickHots(context, stateNORB,
            connectLine1, clickHotsOffset, 1);
        saveConnectLineClickHots(context, stateNORB,
            connectLine2, clickHotsOffset, 0);

        let logicGates = new LogicGates();
        drawRectangularBox(context, NAND_IEC[0],
            logicGates.ORSIGN + "1", sim5BConst);
        drawRectangularBox(context, NAND_IEC[1],
            logicGates.ORSIGN + "1", sim5BConst);

        // Draw returning line
        let returnLine = {
            startX: NAND_IEC[1].startX + sim5BConst.lineLength +
            sim5BConst.IEEEArcRadius +
            sim5BConst.inputLineLength + sim5BConst.IEEEComplementDiameter,
            startY: NAND_IEC[1].startY + sim5BConst.NANDBoxHeight / 2,
            endAtY: yOff + sim5BConst.NANDBoxHeight - inputLinesYOff,
            sketchLength: sketchLengthSim2,
            color: sim5BConst.passiveStateColor,
            saveClickHots: true,
            state: stateNORB
        };


        drawReturnConnector(context, returnLine, sim5BConst);
        if (nQ === sim5BConst.redLineActiveColor) {
            activeStyle(context, stateNORB);
            returnLine.color = sim5BConst.currentColor;
            drawReturnConnector(context, returnLine, sim5BConst, sim5BConst.currentLineWidth);
        }
        passiveStyle(context);

        let constOneBox = {
            leftVertexX: xOff - sim5BConst.inputLineLength - sim5BConst.constBoxWidth,
            leftVertexY: yOff + inputLinesYOff - sim5BConst.constBoxHeight / 2,
            boxWidth: sim5BConst.constBoxWidth,
            boxHeight: sim5BConst.constBoxHeight,
            state: stateNORB,
            color: "black",
            fillStyle: "white",
            textFont: "20px Courier",
            lineWidth: sim5BConst.constBoxLineWidth
        };
        // Draws text boxes, and Q and nQ text
        drawTextBox(context, constOneBox, stateNORB.constZero, 0, sim5BConst);

        let constOneBox2 = {
            leftVertexX: NAND_IEC[1].startX - sim5BConst.inputLineLength -
            sim5BConst.constBoxWidth,
            leftVertexY: NAND_IEC[1].startY + inputLinesYOff -
            sim5BConst.constBoxHeight / 2,
            boxWidth: sim5BConst.constBoxWidth,
            boxHeight: sim5BConst.constBoxHeight,
            state: stateNORB,
            color: "black",
            fillStyle: "white",
            textFont: "20px Courier",
            lineWidth: sim5BConst.constBoxLineWidth
        };
        // Draws text boxes, and Q and nQ text
        let withOverLine = true;
        let shiftNQ = 20;
        let shiftQ = 25;
        drawTextBox(context, constOneBox2, stateNORB.constZero, 0, sim5BConst);
        genericDrawOutText(context, NAND_IEC[0], "Q =", " " + stateNORB.Q, sim5BConst, 0, shiftQ);
        genericDrawOutText(context, NAND_IEC[1], "Q =", " " + stateNORB.nQ,
            sim5BConst, 0, shiftNQ, withOverLine);
    }

    init();

}
