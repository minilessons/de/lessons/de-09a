/**
 * Created by Komediruzecki on 2.12.2016..
 */
scopeFunction5C();

function scopeFunction5C() {

    let canvas = document.getElementById("NOR_C");
    let context = canvas.getContext("2d");

    let zoomHotsNORC = {
        hots: [],
        min: 1.5,
        max: 2
    };

    let stateNORC = {
        simName: "Bistabil ostvaren NILI sklopovima\n(moguće upravljanje ulazima)",
        currentRow: 5,
        nextState: 1,
        X: 0,
        Y: 0,
        Q: 1,
        nQ: 0,
        QnNext: 1,
        userInput: [0, 0],
        hots: [],
        animationOffset: 0,
        animationPattern: [20, 12]
    };


    let scaleFactor = 2;
    let sim5CConst = new Constants(scaleFactor);
    saveClickHots(zoomHotsNORC, 0, 0, canvas.width, canvas.height, 0);

    /**
     * This function checks for any mousedown action and activates circuit flow accordingly.
     * This is callback function for event <code>mousedown</code> listener of clickable box.
     * @param event event for this action
     */
    function clickLineEvent(event) {
        let rect = canvas.getBoundingClientRect();
        let pos = {
            x: event.clientX - rect.left,
            y: event.clientY - rect.top
        };
        // Check if mouse is in area which we consider..
        for (let i = 0, l = stateNORC.hots.length; i < l; i++) {
            let h = stateNORC.hots[i];
            if (pos.x >= h.x && pos.x < h.x + h.w &&
                pos.y >= h.y && pos.y < h.y + h.h) {

                if (stateNORC.userInput[0] === 1 && stateNORC.userInput[1] === 1) {
                    mouseUpEvent();
                }
                if (h.row === 0) {
                    // Set was pressed
                    stateNORC.Q = (stateNORC.userInput[1] === 1) ? 0 : 1;
                    stateNORC.nQ = 0;
                } else if (h.row === 1) {
                    // Reset was pressed
                    stateNORC.nQ = (stateNORC.userInput[0] === 1) ? 0 : 1;
                    stateNORC.Q = 0;
                }
                let u = stateNORC.userInput[h.row];
                stateNORC.userInput[h.row] = (u === 0) ? 1 : 0;
                reInit();
                break;
            }
        }
    }

    function mouseUpEvent() {
        for (let i = 0, l = stateNORC.hots.length; i < l; i++) {
            let h = stateNORC.hots[i];
            stateNORC.userInput[h.row] = 0;

        }
        reInit();
    }

    function setScaleFactor(newScaleFactor) {
        scaleFactor = newScaleFactor;
        sim5CConst = new Constants(scaleFactor);
        reInit();
    }

    function MouseWheelEvent(e) {
        // Firefox prevent default...
        e.preventDefault();
        mouseZoomer(e, canvas, zoomHotsNORC, scaleFactor, setScaleFactor);
    }

    /**
     * Adds mouse down and mouse up listeners to simulation.
     * If mouse is pressed inverters will change state. After releasing the mouse button,
     * inverters hold their state.
     */
    function eventListeners(canvas) {
        canvas.addEventListener('mousedown', clickLineEvent);
        canvas.addEventListener('mouseup', mouseUpEvent);
        canvas.addEventListener("mousewheel", MouseWheelEvent, false);
        // Firefox
        canvas.addEventListener("DOMMouseScroll", MouseWheelEvent, false);
    }

    /**
     * Draws NANDSC in given context and their connecting lines.
     * @param canvas canvas to draw sketch
     * @param context context to draw sketch
     */
    function drawSketch(canvas, context) {
        context.clearRect(0, 0, canvas.width, canvas.height);
        writeSimulationName(canvas, context, stateNORC.simName);
        setBackgroundFrame(context, canvas.width, canvas.height);
        clearStateHots(stateNORC);

        let sketchLengthSim3 = sim5CConst.clickBoxWidth +
            sim5CConst.inputLineLength + sim5CConst.universalBoxWidth
            + sim5CConst.IEEEComplementDiameter +
            sim5CConst.inputLineLength;

        /* Starting point for first NOR, important for fixing center on canvas */
        let canvasCenter = (canvas.width - sketchLengthSim3 -
            2 * sim5CConst.outLineLength) / 2;
        let xOff = canvasCenter + sim5CConst.outLineLength
            + sim5CConst.inputLineLength;
        let yOff = ((canvas.height - 2 * sim5CConst.universalBoxHeight
        - sim5CConst.verticalUpLength) / 2 );


        // Changing colors
        let q = (stateNORC.Q === 1) ? "red" : "black";
        let nQ = (stateNORC.nQ === 1) ? "red" : "black";
        let X = (stateNORC.userInput[0] === 1) ? "red" : "black";
        let Y = (stateNORC.userInput[1] === 1) ? "red" : "black";

        let NORIEC = [
            {
                startX: xOff,
                startY: yOff,
                boxWidth: sim5CConst.universalBoxWidth,
                boxHeight: sim5CConst.universalBoxHeight,
                color: sim5CConst.frameBlacked,
                fillStyle: "white",
                textFont: (sim5CConst.IECFontSize - 5) + "px Courier",
                complementList: [0, 0, 1],
                lineColorsIO: [X, nQ, q],
                state: stateNORC
            }
        ];

        let inputLinesYOff = (sim5CConst.universalBoxHeight -
            sim5CConst.inputLineDistance) / 2;

        // Draw return line1
        let downYOff = 60;
        let downLine = {};

        downLine.startX = NORIEC[0].startX + sim5CConst.universalBoxWidth +
            sim5CConst.IEEEComplementDiameter + sim5CConst.inputLineLength;
        downLine.startY = NORIEC[0].startY + sim5CConst.universalBoxHeight / 2;
        downLine.endX = downLine.startX;
        downLine.endY = downLine.startY + downYOff;
        downLine.color = sim5CConst.passiveStateColor;

        let NORYOff = 90;
        let retLineOff = 75;
        let retLine1 = {
            startX: downLine.endX,
            startY: downLine.endY,
            endX: NORIEC[0].startX - sim5CConst.inputLineLength,
            endY: NORIEC[0].startY + sim5CConst.universalBoxHeight + retLineOff,
            color: sim5CConst.passiveStateColor
        };

        drawConnectLine(context, downLine, sim5CConst.connectLinesWidth);
        if (q === sim5CConst.redLineActiveColor) {
            activeStyle(context, stateNORC);
            downLine.color = sim5CConst.currentColor;
            drawConnectLine(context, downLine, sim5CConst.currentLineWidth);
        }
        passiveStyle(context);

        drawConnectLine(context, retLine1, sim5CConst.connectLinesWidth);
        if (q === sim5CConst.redLineActiveColor) {
            activeStyle(context, stateNORC);
            retLine1.color = sim5CConst.currentColor;
            drawConnectLine(context, retLine1, sim5CConst.currentLineWidth);
        }
        passiveStyle(context);

        let NORIEC2 = {
            startX: NORIEC[0].startX,
            startY: NORIEC[0].startY + sim5CConst.universalBoxHeight + NORYOff,
            boxWidth: sim5CConst.universalBoxWidth,
            boxHeight: sim5CConst.universalBoxHeight,
            color: sim5CConst.frameBlacked,
            fillStyle: "white",
            textFont: (sim5CConst.IECFontSize - 5) + "px Courier",
            complementList: [0, 0, 1],
            lineColorsIO: [q, Y, nQ],
            state: stateNORC
        };

        NORIEC = NORIEC.concat(NORIEC2);
        let logicGates = new LogicGates();
        setGradient2(context, NORIEC);
        drawRectangularBox(context, NORIEC[0], logicGates.ORSIGN + "1", sim5CConst);
        drawRectangularBox(context, NORIEC[1], logicGates.ORSIGN + "1", sim5CConst);

        let retLineConnect = {
            startX: retLine1.endX,
            startY: retLine1.endY,
            endX: retLine1.endX,
            endY: NORIEC[1].startY + inputLinesYOff,
            color: sim5CConst.passiveStateColor
        };

        drawConnectLine(context, retLineConnect, sim5CConst.connectLinesWidth);

        let upLine = {};
        let upYOff = downYOff;
        upLine.startX = NORIEC[1].startX + sim5CConst.universalBoxWidth +
            sim5CConst.IEEEComplementDiameter + sim5CConst.inputLineLength;
        upLine.startY = NORIEC[1].startY + sim5CConst.universalBoxHeight / 2;
        upLine.endX = upLine.startX;
        upLine.endY = upLine.startY - upYOff;
        upLine.color = sim5CConst.passiveStateColor;

        let retLine2 = {
            startX: upLine.endX,
            startY: upLine.endY,
            endX: NORIEC[0].startX - sim5CConst.inputLineLength,
            endY: NORIEC[1].startY - retLineOff,
            color: sim5CConst.passiveStateColor
        };

        let retLineConnect2 = {
            startX: retLine2.endX,
            startY: retLine2.endY,
            endX: retLine2.endX,
            endY: NORIEC[0].startY + sim5CConst.universalBoxHeight - inputLinesYOff,
            color: sim5CConst.passiveStateColor
        };

        drawConnectLine(context, upLine, sim5CConst.connectLinesWidth);
        if (nQ === sim5CConst.redLineActiveColor) {
            activeStyle(context, stateNORC);
            upLine.color = sim5CConst.currentColor;
            drawConnectLine(context, upLine, sim5CConst.currentLineWidth);
        }
        passiveStyle(context);

        drawConnectLine(context, retLine2, sim5CConst.connectLinesWidth);
        if (nQ === sim5CConst.redLineActiveColor) {
            activeStyle(context, stateNORC);
            retLine2.color = sim5CConst.currentColor;
            drawConnectLine(context, retLine2, sim5CConst.currentLineWidth);
        }
        passiveStyle(context);

        drawConnectLine(context, retLineConnect2, sim5CConst.connectLinesWidth);
        if (nQ === sim5CConst.redLineActiveColor) {
            activeStyle(context, stateNORC);
            retLineConnect2.color = sim5CConst.currentColor;
            drawConnectLine(context, retLineConnect2, sim5CConst.currentLineWidth);
        }
        passiveStyle(context);

        let clickBoxX = {
            leftVertexX: xOff - sim5CConst.inputLineLength - sim5CConst.clickBoxWidth,
            leftVertexY: yOff + inputLinesYOff - sim5CConst.clickBoxHeight / 2,
            boxWidth: sim5CConst.clickBoxWidth,
            boxHeight: sim5CConst.clickBoxHeight,
            state: stateNORC,
            clickId: 0,
            color: "black",
            fillStyle: sim5CConst.commonFillStyle,
            textFont: "20px Courier",
            lineWidth: sim5CConst.constBoxLineWidth
        };

        // Draws text boxes, and Q and nQ text
        drawTextBox(context, clickBoxX,
            stateNORC.userInput[clickBoxX.clickId], 1, sim5CConst);

        let clickBoxY = {
            leftVertexX: NORIEC[1].startX - sim5CConst.inputLineLength -
            sim5CConst.clickBoxWidth,
            leftVertexY: NORIEC[1].startY + inputLinesYOff -
            sim5CConst.clickBoxHeight / 2 + sim5CConst.inputLineDistance,
            boxWidth: sim5CConst.clickBoxWidth,
            boxHeight: sim5CConst.clickBoxHeight,
            state: stateNORC,
            clickId: 1,
            color: "black",
            fillStyle: sim5CConst.commonFillStyle,
            textFont: "20px Courier",
            lineWidth: sim5CConst.constBoxLineWidth
        };

        drawTextBox(context, clickBoxY,
            stateNORC.userInput[clickBoxY.clickId], 1, sim5CConst);
        let underLinePosition = 1;
        let overLine = true;
        let shift = -sim5CConst.inputLineLength + (10 * sim5CConst.scaleFactor);
        let shiftY = (NORIEC[0].boxHeight / 2) - (sim5CConst.textBoxHeight / 2)
            + (4 * sim5CConst.scaleFactor);
        let shiftOverLineY = -(4 * sim5CConst.scaleFactor) -
            (sim5CConst.textBoxHeight / 2);
        let uni = new usedUnicode();

        genericDrawOutText(context, NORIEC[0],
            "Q" + uni.subN + " =", " " + stateNORC.Q,
            sim5CConst, 0, shift, false, shiftY);
        genericDrawOutText(context, NORIEC[1],
            "Q" + uni.subN + " =", " " + stateNORC.nQ,
            sim5CConst, underLinePosition, shift, overLine, shiftOverLineY);

        let inputLineLengths = 50;
        let inputS =
            {
                startX: NORIEC[0].startX - sim5CConst.inputLineLength -
                sim5CConst.clickBoxWidth,
                startY: NORIEC[0].startY + inputLinesYOff,
                color: sim5CConst.passiveStateColor
            };
        inputS.endX = inputS.startX - inputLineLengths;
        inputS.endY = inputS.startY;

        let inputR =
            {
                startX: NORIEC[1].startX - sim5CConst.inputLineLength -
                sim5CConst.clickBoxWidth,
                startY: NORIEC[1].startY + inputLinesYOff +
                sim5CConst.inputLineDistance,
                color: sim5CConst.passiveStateColor
            };
        inputR.endX = inputR.startX - inputLineLengths;
        inputR.endY = inputR.startY;


        drawConnectLine(context, inputS, sim5CConst.connectLinesWidth);
        if (X === sim5CConst.redLineActiveColor) {
            activeStyle(context, stateNORC);
            inputS.color = sim5CConst.currentColor;
            drawConnectLine(context, inputS, sim5CConst.currentLineWidth);
        }
        passiveStyle(context);


        drawConnectLine(context, inputR, sim5CConst.connectLinesWidth);
        if (Y === sim5CConst.redLineActiveColor) {
            activeStyle(context, stateNORC);
            inputR.color = sim5CConst.currentColor;
            drawConnectLine(context, inputR, sim5CConst.currentLineWidth);
        }
        passiveStyle(context);

        let centerWithClickBox = 5;
        let textX = {
            leftVertexX: inputS.endX - sim5CConst.textBoxWidth,
            leftVertexY: clickBoxX.leftVertexY - centerWithClickBox,
            boxWidth: sim5CConst.textBoxWidth,
            boxHeight: sim5CConst.textBoxHeight,
            color: "black",
            fillStyle: "white",
            textFont: sim5CConst.varTextSize + "px Courier",
            lineWidth: sim5CConst.connectLinesWidth
        };

        let textY = {
            leftVertexX: inputR.endX - sim5CConst.textBoxWidth,
            leftVertexY: clickBoxY.leftVertexY - centerWithClickBox,
            boxWidth: sim5CConst.textBoxWidth,
            boxHeight: sim5CConst.textBoxHeight,
            color: "black",
            fillStyle: "white",
            textFont: sim5CConst.varTextSize + "px Courier",
            lineWidth: sim5CConst.connectLinesWidth
        };
        drawText(context, textX, "S");
        drawText(context, textY, "R");

        let connectDotQ =
            {
                centerX: downLine.startX,
                centerY: downLine.startY,
                radius: sim5CConst.IEEEConnectDotRadius,
                startAngle: toRadians(sim5CConst.zeroAngle),
                endAngle: toRadians(sim5CConst.fullCircle),
                rotation: false,
                color: sim5CConst.passiveStateColor,
                fillStyle: sim5CConst.passiveStateColor,
                lineWidth: sim5CConst.connectDotLineWidth
            };

        let connectDotNQ =
            {
                centerX: upLine.startX,
                centerY: upLine.startY,
                radius: sim5CConst.IEEEConnectDotRadius,
                startAngle: toRadians(sim5CConst.zeroAngle),
                endAngle: toRadians(sim5CConst.fullCircle),
                rotation: false,
                color: sim5CConst.passiveStateColor,
                fillStyle: sim5CConst.passiveStateColor,
                lineWidth: sim5CConst.connectDotLineWidth
            };
        drawCircle(context, connectDotQ);
        drawCircle(context, connectDotNQ);


        let outQLine = {
            startX: connectDotQ.centerX + connectDotQ.radius,
            startY: connectDotQ.centerY,
            color: sim5CConst.passiveStateColor
        };
        outQLine.endX = outQLine.startX + sim5CConst.inputLineLength;
        outQLine.endY = outQLine.startY;

        let outNQLine = {
            startX: connectDotNQ.centerX + connectDotNQ.radius,
            startY: connectDotNQ.centerY,
            color: sim5CConst.passiveStateColor
        };
        outNQLine.endX = outNQLine.startX + sim5CConst.inputLineLength;
        outNQLine.endY = outNQLine.startY;

        drawConnectLine(context, outQLine, sim5CConst.connectLinesWidth);
        if (q === sim5CConst.redLineActiveColor) {
            activeStyle(context, stateNORC);
            outQLine.color = sim5CConst.currentColor;
            drawConnectLine(context, outQLine, sim5CConst.currentLineWidth);
        }
        passiveStyle(context);

        drawConnectLine(context, outNQLine, sim5CConst.connectLinesWidth);
        if (nQ === sim5CConst.redLineActiveColor) {
            activeStyle(context, stateNORC);
            outNQLine.color = sim5CConst.currentColor;
            drawConnectLine(context, outNQLine, sim5CConst.currentLineWidth);
        }
        passiveStyle(context);

    }

    function reInit() {
        drawSketch(canvas, context);
    }

    function startSimulation() {
        stateNORC.animationOffset++;
        if (stateNORC.animationOffset > 2000) {
            stateNORC.animationOffset = 0;
        }
        drawSketch(canvas, context);
        setTimeout(startSimulation, 20);
    }

    /**
     * Initialises whole simulation. Draws sketch and adds event listeners.
     */
    function init() {
        drawSketch(canvas, context);
        drawSketch(canvas, context);
        eventListeners(canvas);
        startSimulation();
    }

    init();
}
