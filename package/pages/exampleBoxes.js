/**
 * Created by Komediruzecki on 27.11.2016..
 */


// stateB2 Don-t work properly when user clicks and waits...

let canvasActiveHigh = document.getElementById("ActiveHigh");
let contextActiveHigh = canvasActiveHigh.getContext("2d");

let canvasActiveLow = document.getElementById("ActiveLow");
let contextActiveLow = canvasActiveLow.getContext("2d");

let canvasRaisingEdge = document.getElementById("RaisingEdge");
let contextRaisingEdge = canvasRaisingEdge.getContext("2d");

let canvasFallingEdge = document.getElementById("FallingEdge");
let contextFallingEdge = canvasFallingEdge.getContext("2d");

let canvasAsyncSR = document.getElementById("AsyncSR");
let contextAsyncSR = canvasAsyncSR.getContext("2d");

/* Global constants for this simulation */
let scaleFactor = 1;
let constSR = new Constants(scaleFactor, 50, 50, 20, 20, 40);
let stateAsyncSR = {
    simName: "Primjer 5.",
    curOut: 0,
    userInput: [0, 0],
    hots: []
};

let stateActiveHigh = {
    simName: "Primjer 1.",
    typeSpec: 1,
    cp: 0,
    curOut: 0,
    userInput: [0, 0],
    hots: []
};
let stateActiveLow = {
    simName: "Primjer 2.",
    typeSpec: 0,
    cp: 0,
    curOut: 0,
    userInput: [0, 0],
    hots: []
};
let stateRaisingEdge = {
    simName: "Primjer 3.",
    typeSpec: 3,
    cpEdge: 0,
    cp: 0,
    curOut: 0,
    userInput: [0, 0],
    hots: []
};

let stateFallingEdge = {
    simName: "Primjer 4.",
    typeSpec: 2,
    cpEdge: 0,
    cp: 0,
    curOut: 0,
    userInput: [0, 0],
    hots: []
};


/**
 * Defines LineHandler template which uses different set of colors.
 * @Note Only for learning purposes...
 */
class LineHandler extends BoxHandler {
    constructor(x, y, w, h, lineWidth, lineLength, inputLineDistance, color) {
        super(x, y, w, h, "black");
        this.lineWidth = lineWidth;
        this.lineLength = lineLength;
        this.inputLineDistance = inputLineDistance;
        this.colorIn1 = color[0];
        this.colorIn2 = color[1];
        this.colorOut = color[2];
        this.colorCp = color[3]
    }

    get getColorIn1() {
        return this.colorIn1;
    }

    get getColorIn2() {
        return this.colorIn2;
    }

    get getColorOut() {
        return this.colorOut;
    }

    get getColorCp() {
        return this.colorCp;
    }
}
/*

 // jQuery Method for show hide answers
 $(document).ready(function () {
 $("#showAnswers").click(function () {
 $(".answers").show();
 });
 $("#hideAnswers").click(function () {
 $(".answers").hide();
 });
 $(".indentedRadios").css("text-indent", "20px");
 });
 */


init();
init();
animateSim();

function animateSim() {
    setInterval(animateHighCP, 2000);
    setInterval(animateLowCP, 2000);
    setInterval(animateRaisingCP, 2000);
    setInterval(animateFallingCP, 2000);
}
/**
 * This function initialises main draw functions and theirs events.
 */
function init() {
    drawSR(canvasActiveHigh, contextActiveHigh, stateActiveHigh, constSR);
    drawSR(canvasActiveLow, contextActiveLow, stateActiveLow, constSR);
    drawSR(canvasRaisingEdge, contextRaisingEdge, stateRaisingEdge, constSR);
    drawSR(canvasFallingEdge, contextFallingEdge, stateFallingEdge, constSR);
    drawSR(canvasAsyncSR, contextAsyncSR, stateAsyncSR, constSR);
    eventListeners();

}

/**
 * This function connects events which are used for simulating boxes behaviour.
 */
function eventListeners() {
    canvasActiveHigh.addEventListener('mousedown', clickBoxEventActiveHigh);
    canvasActiveLow.addEventListener('mousedown', clickBoxEventActiveLow);
    canvasRaisingEdge.addEventListener('mousedown', clickBoxEventRaisingEdge);
    canvasFallingEdge.addEventListener('mousedown', clickBoxEventFallingEdge);

    canvasActiveHigh.addEventListener('mouseup', setDefaultActiveHigh);
    canvasActiveLow.addEventListener('mouseup', setDefaultActiveLow);
    canvasRaisingEdge.addEventListener('mouseup', setDefaultRaisingEdge);
    canvasFallingEdge.addEventListener('mouseup', setDefaultFallingEdge);

    canvasAsyncSR.addEventListener('mousedown', clickBoxEventAsyncSR);
    canvasAsyncSR.addEventListener('mouseup', setDefaultAsyncSR);
}


/**
 * This function resets state of first box inputs and outputs.
 * This is callback function for event <code>mouseup</code> listener of first box.
 */
function setDefaultAsyncSR() {
    for (let i = 0, l = stateAsyncSR.hots.length; i < l; i++) {
        stateAsyncSR.userInput[i] = 0;
    }
    drawSR(canvasAsyncSR, contextAsyncSR, stateAsyncSR, constSR);
}


/**
 * This function resets state of first box inputs and outputs.
 * This is callback function for event <code>mouseup</code> listener of first box.
 */
function setDefaultActiveHigh() {
    for (let i = 0, l = stateActiveHigh.hots.length; i < l; i++) {
        stateActiveHigh.userInput[i] = 0;
    }
    drawSR(canvasActiveHigh, contextActiveHigh, stateActiveHigh, constSR);
}


/**
 * This function resets state of first box inputs and outputs.
 * This is callback function for event <code>mouseup</code> listener of first box.
 */
function setDefaultActiveLow() {
    for (let i = 0, l = stateActiveLow.hots.length; i < l; i++) {
        stateActiveLow.userInput[i] = 0;
    }
    drawSR(canvasActiveLow, contextActiveLow, stateActiveLow, constSR);
}


/**
 * This function resets state of first box inputs and outputs.
 * This is callback function for event <code>mouseup</code> listener of first box.
 */
function setDefaultRaisingEdge() {
    for (let i = 0, l = stateRaisingEdge.hots.length; i < l; i++) {
        stateRaisingEdge.userInput[i] = 0;
    }
    drawSR(canvasRaisingEdge, contextRaisingEdge, stateRaisingEdge, constSR);
}


/**
 * This function resets state of first box inputs and outputs.
 * This is callback function for event <code>mouseup</code> listener of first box.
 */
function setDefaultFallingEdge() {
    for (let i = 0, l = stateFallingEdge.hots.length; i < l; i++) {
        stateFallingEdge.userInput[i] = 0;
    }
    drawSR(canvasFallingEdge, contextFallingEdge, stateFallingEdge, constSR);
}

// Sets click box events for all simulations.


/**
 * This function checks for any mousedown action and activates circuit flow accordingly.
 * This is callback function for event <code>mousedown</code> listener of second box.
 */
function clickBoxEventAsyncSR(event) {
    let rect = canvasAsyncSR.getBoundingClientRect();
    let pos = {
        x: event.clientX - rect.left,
        y: event.clientY - rect.top
    };
    // Check if mouse is in area which we consider..
    for (let i = 0, l = stateAsyncSR.hots.length; i < l; i++) {
        let h = stateAsyncSR.hots[i];
        if (pos.x >= h.x && pos.x < h.x + h.w &&
            pos.y >= h.y && pos.y < h.y + h.h) {

            // If set is pressed
            if (h.row === 0) {
                stateAsyncSR.curOut = 1;
            } else if (h.row === 1) {
                // Reset pressed
                stateAsyncSR.curOut = 0;
            }
            setInputState(stateAsyncSR.userInput, h.row);
            drawSR(canvasAsyncSR, contextAsyncSR, stateAsyncSR, constSR);
            break;
        }
    }
}


/**
 * This function checks for any mousedown action and activates circuit flow accordingly.
 * This is callback function for event <code>mousedown</code> listener of second box.
 */
function clickBoxEventActiveHigh(event) {
    let rect = canvasActiveHigh.getBoundingClientRect();
    let pos = {
        x: event.clientX - rect.left,
        y: event.clientY - rect.top
    };
    // Check if mouse is in area which we consider..
    for (let i = 0, l = stateActiveHigh.hots.length; i < l; i++) {
        let h = stateActiveHigh.hots[i];
        if (pos.x >= h.x && pos.x < h.x + h.w &&
            pos.y >= h.y && pos.y < h.y + h.h) {
            let cp = stateActiveHigh.cp;

            // If set is pressed
            if (h.row === 0 && cp === 1) {
                stateActiveHigh.curOut = 1;
            } else if (h.row === 1 && cp === 1) {
                // Reset pressed
                stateActiveHigh.curOut = 0;
            }
            setInputState(stateActiveHigh.userInput, h.row);
            drawSR(canvasActiveHigh, contextActiveHigh, stateActiveHigh, constSR);
            break;
        }
    }
}


/**
 * This function checks for any mousedown action and activates circuit flow accordingly.
 * This is callback function for event <code>mousedown</code> listener of second box.
 */
function clickBoxEventActiveLow(event) {
    let rect = canvasActiveLow.getBoundingClientRect();
    let pos = {
        x: event.clientX - rect.left,
        y: event.clientY - rect.top
    };
    // Check if mouse is in area which we consider..
    for (let i = 0, l = stateActiveLow.hots.length; i < l; i++) {
        let h = stateActiveLow.hots[i];
        if (pos.x >= h.x && pos.x < h.x + h.w &&
            pos.y >= h.y && pos.y < h.y + h.h) {
            let cp = stateActiveLow.cp;

            // If set is pressed
            if (h.row === 0 && cp === 0) {
                stateActiveLow.curOut = 1;
            } else if (h.row === 1 && cp === 0) {
                // Reset pressed
                stateActiveLow.curOut = 0;
            }
            setInputState(stateActiveLow.userInput, h.row);
            drawSR(canvasActiveLow, contextActiveLow, stateActiveLow, constSR);
            break;
        }
    }
}

/**
 * This function checks for any mousedown action and activates circuit flow accordingly.
 * This is callback function for event <code>mousedown</code> listener of second box.
 */
function clickBoxEventRaisingEdge(event) {
    let rect = canvasRaisingEdge.getBoundingClientRect();
    let pos = {
        x: event.clientX - rect.left,
        y: event.clientY - rect.top
    };
    // Check if mouse is in area which we consider..
    for (let i = 0, l = stateRaisingEdge.hots.length; i < l; i++) {
        let h = stateRaisingEdge.hots[i];
        if (pos.x >= h.x && pos.x < h.x + h.w &&
            pos.y >= h.y && pos.y < h.y + h.h) {

            setInputState(stateRaisingEdge.userInput, h.row);
            drawSR(canvasRaisingEdge, contextRaisingEdge, stateRaisingEdge, constSR);
            break;
        }
    }
}

/**
 * This function checks for any mousedown action and activates circuit flow accordingly.
 * This is callback function for event <code>mousedown</code> listener of second box.
 */
function clickBoxEventFallingEdge(event) {
    let rect = canvasFallingEdge.getBoundingClientRect();
    let pos = {
        x: event.clientX - rect.left,
        y: event.clientY - rect.top
    };
    // Check if mouse is in area which we consider..
    for (let i = 0, l = stateFallingEdge.hots.length; i < l; i++) {
        let h = stateFallingEdge.hots[i];
        if (pos.x >= h.x && pos.x < h.x + h.w &&
            pos.y >= h.y && pos.y < h.y + h.h) {
            setInputState(stateFallingEdge.userInput, h.row);
            drawSR(canvasFallingEdge, contextFallingEdge, stateFallingEdge, constSR);
            break;
        }
    }
}

function animateLowCP() {
    stateActiveLow.cp = 1 - stateActiveLow.cp;
    if (stateActiveLow.userInput[0] === 1) {
        stateActiveLow.curOut = 1;
    }
    if (stateActiveLow.userInput[1] === 1) {
        stateActiveLow.curOut = 0;
    }
    drawSR(canvasActiveLow, contextActiveLow, stateActiveLow, constSR)
}

function animateHighCP() {
    stateActiveHigh.cp = 1 - stateActiveHigh.cp;
    if (stateActiveHigh.userInput[0] === 1) {
        stateActiveHigh.curOut = 1;
    }
    if (stateActiveHigh.userInput[1] === 1) {
        stateActiveHigh.curOut = 0;
    }
    drawSR(canvasActiveHigh, contextActiveHigh, stateActiveHigh, constSR)
}

/**
 * Animates flip flop which has raising edge behaviour.
 */
function animateRaisingCP() {
    stateRaisingEdge.cp = 1 - stateRaisingEdge.cp;
    if (stateRaisingEdge.cp === 1) {
        stateRaisingEdge.cpEdge = 1;
    } else {
        stateRaisingEdge.cpEdge = 0;
    }
    if (stateRaisingEdge.cpEdge === 1 && stateRaisingEdge.userInput[0] == 1) {
        stateRaisingEdge.curOut = 1;
    }
    if (stateRaisingEdge.cpEdge === 1 && stateRaisingEdge.userInput[1] == 1) {
        stateRaisingEdge.curOut = 0;
    }
    stateRaisingEdge.cpEdge = 0;
    drawSR(canvasRaisingEdge, contextRaisingEdge, stateRaisingEdge, constSR);
}

/**
 * Animates flip flop which has raising edge behaviour.
 */
function animateFallingCP() {
    stateFallingEdge.cp = 1 - stateFallingEdge.cp;
    if (stateFallingEdge.cp === 0) {
        stateFallingEdge.cpEdge = 1;
    } else {
        stateFallingEdge.cpEdge = 0;
    }
    if (stateFallingEdge.cpEdge === 1 && stateFallingEdge.userInput[0] == 1) {
        stateFallingEdge.curOut = 1;
    }
    if (stateFallingEdge.cpEdge === 1 && stateFallingEdge.userInput[1] == 1) {
        stateFallingEdge.curOut = 0;
    }
    stateFallingEdge.cpEdge = 0;
    drawSR(canvasFallingEdge, contextFallingEdge, stateFallingEdge, constSR);
}

