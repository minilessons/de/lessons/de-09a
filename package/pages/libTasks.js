/**
 * Created by Komediruzecki on 5.12.2016..
 */


function insertConnector(list) {
    let retString = "";
    let size = list.length;
    for (let i = 0; i < size; i++) {
        if (i === (size - 1) && i != 0) {
            retString += " i ";
            retString += list[i];
        } else {
            retString += list[i];
        }
        if (i != (size - 2) && size != 1 && i != (size - 1)) {
            retString += ", ";
        }
    }
    return retString;
}

function returnValidText(answers, answered) {
    let correctIntro = "Odgovorili ste točno ";
    let wrongIntro = "Niste dobro odgovorili na ";
    let unIntro = "Niste odgovorili na ";
    let listOfCorrect = [];
    let listOfWrong = [];
    let listOfUnanswered = [];

    // Answer is correct if is 0, otherwise is wrong!
    for (let i = 0, l = answers.length; i < l; i++) {
        if (answers[i] === 0 && answered[i] === true) {
            listOfCorrect.push((i + 1) + ".");
        } else if (answered[i] === true) {
            listOfWrong.push((i + 1) + ".");
        }
    }

    for (let i = 0, l = answered.length; i < l; i++) {
        if (answered[i] === false) {
            listOfUnanswered.push((i + 1) + ".");
        }
    }

    let cSentence = "";
    let wSentence = "";
    let unSentence = "";
    if (listOfCorrect.length > 0) {
        cSentence += correctIntro + insertConnector(listOfCorrect);
        cSentence += " pitanje.\n";
    }
    if (listOfWrong.length > 0) {
        wSentence = wrongIntro + insertConnector(listOfWrong);
        wSentence += " pitanje.\n";
    }

    if (listOfUnanswered.length > 0) {
        unSentence = unIntro + insertConnector(listOfUnanswered);
        unSentence += " pitanje.\n";
    }
    let endIntro = "Pročitajte objašnjenje i pokušajte ponovo. ";
    return cSentence + unSentence + wSentence + endIntro;
}


function initStartingValues(configuration, numOfQuestions) {
    // Set answers to "checked of before..."
    for (let i = 0; i < numOfQuestions; i++) {
        if (configuration.answers[i] != "") {
            document.getElementById(configuration.answers[i]).checked = true;
        } else {
            // Leave false...
            let elements = document.getElementsByName("question" + (i + 1));
            for (let j = 0, l = elements.length; j < l; j++) {
                elements[j].checked = false;
            }
        }

    }
}

function collectData(numOfQuestions) {

    let listOfAnswered = [];
    let listOfAnswers = [];
    for (let i = 0; i < numOfQuestions; i++) {
        listOfAnswered[i] = false;
        let radios = document.getElementsByName("question" + ((i + 1)));
        for (let j = 0, l = radios.length; j < l; j++) {
            if (radios[j].checked === true) {
                listOfAnswers[i] = radios[j].id;
                listOfAnswered[i] = true;
                break;
            }
        }
    }
    return {
        answers: listOfAnswers,
        answered: listOfAnswered
    };
}

